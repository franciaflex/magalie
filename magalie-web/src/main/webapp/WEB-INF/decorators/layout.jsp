<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>
<%@ taglib prefix="sb"        uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="sj"        uri="/struts-jquery-tags" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<!DOCTYPE HTML>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <sj:head locale="fr" jqueryui="false" jquerytheme="start" />
        <sb:head />
        <title><decorator:title default="MagaLiE"/> - MagaLiE</title>
        <script src="<s:url value='/js/magalie.js' />"></script>
        <link rel="stylesheet" href="<s:url value='/css/magalie.css' />"/>
        <link rel="stylesheet" href="<s:url value='/css/magalie-ck3x.css' />"/>
        <!--link rel="icon" type="image/png" href="<s:url value='/favicon.png' />" -->
        <decorator:head />
    </head>
    <body>
        <div id="container">
            <s:if test="withMessages" >
                <script type="text/javascript">
                    $(document).ready(function () {
                    <s:iterator value="actionMessages" var="actionMessage" >
                        alert('<s:property value="#actionMessage" escapeJavaScript="true" escapeHtml="false" />');
                    </s:iterator>
                    <s:iterator value="actionErrors" var="actionError" >
                        alert('<s:property value="#actionError" escapeJavaScript="true" escapeHtml="false"/>');
                    </s:iterator>
                    <s:iterator value="messages" var="message" >
                        alert('<s:property value="#message" escapeJavaScript="true" escapeHtml="false"/>');
                    </s:iterator>
                    });
                </script>
            </s:if>
            <decorator:body />
        </div>
    </body>
</html>
