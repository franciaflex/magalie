package com.franciaflex.magalie.web;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieApplicationConfig;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.web.action.ChooseCompanyAction;
import com.franciaflex.magalie.web.action.IndexAction;
import com.franciaflex.magalie.web.action.LoginAction;
import com.franciaflex.magalie.web.action.ReportAction;
import com.franciaflex.magalie.web.action.RestoreFixturesAction;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.beans.BeanUtil;
import org.nuiton.web.filter.JpaTransactionFilter;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyDescriptor;
import java.util.Set;

/**
 * This interceptor injects services, config and session in actions.
 */
public class MagalieInterceptor implements Interceptor {

    public static final ImmutableSet<Class<? extends MagalieActionSupport>> ACCESSIBLE_ACTIONS_FOR_NOT_LOGGED_USER =
            ImmutableSet.of(
                    LoginAction.class,
                    IndexAction.class,
                    ChooseCompanyAction.class,
                    ReportAction.class,
                    RestoreFixturesAction.class);

    private static final Log log = LogFactory.getLog(MagalieInterceptor.class);

    @Override
    public void init() {

        if (log.isInfoEnabled()) {
            log.info("init " + this);
        }

    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {

        Object action = invocation.getAction();

        if (action instanceof MagalieActionSupport) {

            MagalieSession magalieSession = getMagalieSession(invocation);

            // on ajoute les notifications

            MagalieApplicationContext magalieApplicationContext =
                    getMagalieApplicationContext(invocation);

            StrutsActionMessagesUserNotificationContext userNotificationContext =
                    magalieApplicationContext.getUserNotificationContext();

            MagalieUser magalieUser = magalieSession.getMagalieUser();

            if (magalieUser != null) {

                ((MagalieActionSupport) action).setMessages(userNotificationContext.getMessages(magalieUser));

            }

            Class<?> actionClass = action.getClass();

            Set<PropertyDescriptor> descriptors =
                    BeanUtil.getDescriptors(
                            actionClass,
                            BeanUtil.IS_WRITE_DESCRIPTOR);

            MagalieServiceContext serviceContext = newServiceContext(invocation);

            for (PropertyDescriptor propertyDescriptor : descriptors) {

                Class<?> propertyType = propertyDescriptor.getPropertyType();

                Object toInject = null;

                if (MagalieService.class.isAssignableFrom(propertyType)) {

                    Class<? extends MagalieService> serviceClass =
                            (Class<? extends MagalieService>) propertyType;

                    toInject = serviceContext.newService(serviceClass);

                } else if (MagalieSession.class.isAssignableFrom(propertyType)) {

                    toInject = magalieSession;

                } else if (MagalieApplicationConfig.class.isAssignableFrom(propertyType)) {

                    toInject = magalieApplicationContext.getMagalieApplicationConfig();

                }

                if (toInject != null) {

                    if (log.isTraceEnabled()) {
                        log.trace("injecting " + toInject + " in action " + action);
                    }

                    propertyDescriptor.getWriteMethod().invoke(action, toInject);

                }
            }

            try {

                // prevent accessing a page without a session
                if (magalieUser == null) {

                    if (ACCESSIBLE_ACTIONS_FOR_NOT_LOGGED_USER.contains(actionClass)) {

                        if (log.isDebugEnabled()) {
                            log.debug("user has no session but can access to action " + action);
                        }

                    } else {

                        if (log.isDebugEnabled()) {
                            log.debug("user has no session: illegal access to action " + action
                                    + ", redirecting user to login page");
                        }

                        return "redirect-to-login";

                    }

                }

                return invocation.invoke();

            } finally {

                serviceContext.getPersistenceContext().getEntityTransaction().rollback();

            }

        } else {

            // not an action, just process

            return invocation.invoke();

        }

    }

    protected MagalieSession getMagalieSession(ActionInvocation invocation) {

        MagalieSession session = (MagalieSession) invocation.getInvocationContext().getSession().get(MagalieSession.SESSION_PARAMETER);

        if (session == null) {

            session = new MagalieSession();

            invocation.getInvocationContext().getSession().put(MagalieSession.SESSION_PARAMETER, session);

        }

        return session;

    }

    protected MagalieApplicationContext getMagalieApplicationContext(ActionInvocation invocation) {

        MagalieApplicationContext applicationContext =
                (MagalieApplicationContext) invocation
                        .getInvocationContext()
                        .getApplication()
                        .get(MagalieApplicationContext.APPLICATION_CONTEXT_PARAMETER);

        Preconditions.checkNotNull(
                "application context must be initialized before calling an action",
                applicationContext);

        return applicationContext;

    }

    protected MagalieServiceContext newServiceContext(ActionInvocation invocation) {

        final String HTTP_REQUEST_KEY = "com.opensymphony.xwork2.dispatcher.HttpServletRequest"; // StrutsStatics.HTTP_REQUEST;

        EntityManager entityManager = (EntityManager)
                ((HttpServletRequest) invocation
                        .getInvocationContext()
                        .get(HTTP_REQUEST_KEY))
                        .getAttribute(JpaTransactionFilter.JPA_TRANSACTION_REQUEST_ATTRIBUTE);

        MagalieApplicationContext magalieApplicationContext = getMagalieApplicationContext(invocation);

        MagalieServiceContext serviceContext =
                magalieApplicationContext.newServiceContext(entityManager);

        return serviceContext;

    }

    @Override
    public void destroy() {

        if (log.isInfoEnabled()) {
            log.info("destroy " + this);
        }

    }
}
