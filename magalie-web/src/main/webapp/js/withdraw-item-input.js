/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

model.storageMovements = [];
model.storageMovementsIndex = -1;

model.withdrawn = function() {
    var withdrawn = 0;
    for (var i = 0; i < this.storageMovements.length; i++) {
        withdrawn += this.storageMovements[i].withdrawn || 0;
    }
    return withdrawn;
};

model.remaining = function() {
    var remaining = null;
    if (this.expectedQuantity) {
        remaining = this.expectedQuantity - this.withdrawn();
    }
    if (remaining < 0.) {
        remaining = 0.;
    }
    return remaining;
}

model.newEmptyStorageMovementForNextLocation = function() {
    var quantityIsRemaining = ! model.expectedQuantity // pas de quantité demandée précisée au départ donc on ne sait pas s'il faut s'arrêter donc on propose au cas où
                              || model.withdrawn() < model.expectedQuantity // on a une quantité demandée mais on ne l'a pas encore atteinte
    var allIsUsed = true;
    for (var i = 0; i < this.storageMovements.length; i++) {
        if ( ! this.storageMovements[i].used) {
            allIsUsed = false;
        }
    }
    var proposeAnotherLocation = quantityIsRemaining && allIsUsed;
    if (proposeAnotherLocation) {
        var storageMovement;
        $.ajax({
            dataType: "json",
            url: getRealTimeStorageMovementTaskJsonUrl,
            data: { articleId: this.articleId },
            async: false,
            success: function(data) {
                        storageMovement = data.realTimeStorageMovementTask;
                     },
            fail: function(jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR);
                        console.error(textStatus);
                        console.error(errorThrown);
                        alert("Erreur de communication avec le serveur : \n" + errorThrown);
                    }
        });
        if (storageMovement.articleUnavailable) {
            alert("Pas d'autre emplacement disponible");
        } else {
            if (this.expectedQuantity) {
                storageMovement.expectedQuantity = Math.min(model.remaining(), storageMovement.storedArticle.quantity);
            } else {
                storageMovement.expectedQuantity = storageMovement.storedArticle.quantity;
            }
            this.storageMovements.push(storageMovement);
            this.storageMovementsIndex += 1;
        }
    }
}

model.getStorageMovement = function(barcode) {
    if (barcode == '') {
        throw new ValidationError("Il faut indiquer le code barre d'un emplacement");
    }
    var storageMovement;
    for (var i = 0; i < this.storageMovements.length; i++) {
        if (barcode == this.storageMovements[i].storedArticle.location.barcode) {
            storageMovement = this.storageMovements[i];
        }
    }
    if (storageMovement == null) {
        // TODO try to load it !
        throw new ValidationError(barcode + " n'est pas le code barre d'un emplacement valide");
    }
    return storageMovement;
};

model.withdraw = function(barcode, quantity) {
    if ( ! isFinite(quantity) || quantity < 0) {
        throw new ValidationError('Il faut entrer une quantité strictement supérieure à 0');
    }
    var storageMovement = this.getStorageMovement(barcode);
    storageMovement.used = true;
    storageMovement.defect = quantity < storageMovement.expectedQuantity && confirm('Signaler anomalie ?');
    storageMovement.withdrawn = quantity;
    var model = this;
    $.ajax({
        dataType: "json",
        url: saveRealTimeStorageMovementJsonUrl,
        data: {
            articleId: model.articleId,
            originLocationId: storageMovement.storedArticle.location.id,
            destinationLocationId: model.destinationLocationId,
            quantity: storageMovement.withdrawn
        },
        async: false,
        success: function(data) {
            if (!data.storageMovementId) {
                alert("Erreur de communication avec le serveur au prélèvement : \n" + data);
            } else {
                storageMovement.id = data.storageMovementId;
            }
        },
        fail: function(jqXHR, textStatus, errorThrown) {
            console.l(jqXHR);
            console.error(textStatus);
            console.error(errorThrown);
            alert("Erreur de communication avec le serveur au prélèvement : \n" + errorThrown);
        }
    });
};

model.reportError = function(barcode) {
    var storageMovement = this.getStorageMovement(barcode);
    storageMovement.used = true;
    storageMovement.defect = true;
};

model.getConfirmation = function() {
    var storageMovementIds = [];
    var locationInErrorIds = [];
    $.each(this.storageMovements, function(key, storageMovement) {
        if (storageMovement.used) {
            storageMovementIds.push(storageMovement.id);
            if (storageMovement.defect) {
                locationInErrorIds.push(storageMovement.storedArticle.location.id);
            }
        }
    });
    var model = this;
    var confirmation = {
        requestedArticleId: model.requestedArticleId,
        articleId: model.articleId,
        storageMovementIds: storageMovementIds,
        locationInErrorIds: locationInErrorIds
    };
    return confirmation;
};

var view = {

    refreshSummary : function() {
        $('#withdrawn').text(model.withdrawn());
        if (model.withdrawn() === model.quantity) {
            $('#withdrawn').addClass('success');
        } else if (model.withdrawn() > model.quantity) {
            $('#withdrawn').addClass('warning');
        }
    },

    refreshLocations : function() {

        var storageMovementsElement = $('#storageMovements');

        // reset the view
        storageMovementsElement.children().remove();

        // add each storage movement in the model
        $.each(model.storageMovements, function(key, storageMovement) {
            var storageMovementElement = processTemplate('storageMovementTemplate', storageMovement);
            $(storageMovementElement).attr('id', 'storageMovement-' + key);
            storageMovementsElement.append(storageMovementElement);
            if (storageMovement.used) {
                if (storageMovement.defect) {
                    $(storageMovementElement).addClass('defect');
                } else {
                    $(storageMovementElement).addClass('success');
                }
            } else if (storageMovement.storedArticle.location.id === model.storageMovements[model.storageMovementsIndex].storedArticle.location.id) {
                $(storageMovementElement).addClass('pending');
            } else {
                $(storageMovementElement).hide();
            }
        });
        $("body").scrollTop($("#container").height());

    },

    refreshForm : function() {
        // reset field to prevent reuse of the value by mistake
        $('#withdraw-item_locationBarcode').val('');
        // set default value of quantity to what should be withdrawn on the current location
        if (model.storageMovementsIndex != null && model.storageMovementsIndex >= 0) {
            $('#withdraw-item_quantity').val(model.storageMovements[model.storageMovementsIndex].expectedQuantity);
        }
    },

//    refreshButtons : function() {
//        if (model.allowSubmit) {
//            $('#withdraw-item_next').val('Terminer').removeClass('btn-primary').addClass('btn-success');
//        } else {
//            $('#withdraw-item_next').val('Suivant').removeClass('btn-success').addClass('btn-primary');
//        }
//    },

    refresh : function() {
        this.refreshSummary();
        this.refreshLocations();
        this.refreshForm();
//        this.refreshButtons();
        $('#withdraw-item_locationBarcode').focus();
    }

};

var controller = {

    onShowFirstLocation : function () {

        model.newEmptyStorageMovementForNextLocation();

    },

    next : function(reportError) {

        var barcode = $('#withdraw-item_locationBarcode').val();
        var quantity = parseFloat($('#withdraw-item_quantity').val());

        model.withdraw(barcode, quantity);

        if (reportError === true) {
            model.reportError(barcode);
        }

        view.refresh();

    },

    onNext : function() {

        try {

            controller.next(false);

            model.newEmptyStorageMovementForNextLocation();

            view.refresh();

        } catch (ex) {

            if (ex instanceof ValidationError) {
                handleValidationError(ex);
            } else {
                throw ex;
            }
        }

    },

    onReportError : function() {

        try {

            controller.next(true);

            model.newEmptyStorageMovementForNextLocation();

            view.refresh();

        } catch (ex) {

            if (ex instanceof ValidationError) {
                handleValidationError(ex);
            } else {
                throw ex;
            }
        }

    },

    onConfirm : function() {

        try {

            var barcode = $('#withdraw-item_locationBarcode').val();

            if (barcode.length > 0) {

                controller.next(false);

            }

            var confirmation = model.getConfirmation();

            var storageMovementCount = confirmation.storageMovementIds.length;

            if (storageMovementCount > 0) {

                var confirmMessage = 'Confirmer ' + storageMovementCount + ' mouvements ?';

                if (confirm(confirmMessage)) {

                    // push model as Json in a hidden field
                    $('#withdraw-item_confirmation').val(JSON.stringify(confirmation));

                    model.forceConfirm = true;

                    $('#withdraw-item').submit();

                } else {

                    model.newEmptyStorageMovementForNextLocation();

                    view.refresh();

                }

            }

        } catch (ex) {

            if (ex instanceof ValidationError) {
                handleValidationError(ex);
            } else {
                throw ex;
            }

        }

    },

    onCancel : function(e) {

        var confirmation = model.getConfirmation();

        var confirmMessage = 'Annuler ' + confirmation.storageMovementIds.length + ' mouvements ?';

        var redirect = confirmation.storageMovementIds.length == 0
                    || confirm(confirmMessage);

        if (redirect) {

            redirectTo($('#cancelLink'));

        } else {

            if (e) {
                e.preventDefault();
            }

        }

    },

    onSubmit : function(e) {

        if (model.forceConfirm) {

        } else {

            e.preventDefault();

        }

    }

};

// bindings
$(document).ready(function() {
    $('#withdraw-item').submit(controller.onSubmit);
    $('#reportError').click(controller.onReportError);
    $('#reportCancel').click(controller.onCancel);
    $('#withdraw-item_next').click(controller.onNext);
    $('#confirm').click(controller.onConfirm);
    $('#cancelLink').click(controller.onCancel);

    bindKey('F1', controller.onConfirm);
    bindKey('F4', controller.onConfirm);
    bindKey('F5', controller.onReportError);
    bindKey('Esc', controller.onCancel);
    controller.onShowFirstLocation();
    view.refresh();
});
