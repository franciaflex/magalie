package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.StoredArticles;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.AbstractMagalieServiceTest;
import com.franciaflex.magalie.services.SimpleWithdrawItemTask;
import com.franciaflex.magalie.services.exception.IllegalWithdrawException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class SimpleWithdrawItemServiceTest extends AbstractMagalieServiceTest {

    private static final Log log = LogFactory.getLog(SimpleWithdrawItemServiceTest.class);

    protected SimpleWithdrawItemService service;

    protected MagalieUser magalieUser;

    protected Building building;

    @Before
    public void setUp() throws ParseException {

        loadFixtures("fixtures");

        service = newService(SimpleWithdrawItemService.class);

        serviceContext.setDate(new Date(1363948427576l));

        magalieUser = fixture("bruno");

        building = fixture("B1");
    }

    @Test
    public void testSimpleWithdraw() throws InvalidMagalieBarcodeException, IllegalWithdrawException {

        Article article = fixture("article2");

        SimpleWithdrawItemTask simpleWithdrawItemTask =
                service.getSimpleWithdrawItemTask(building, magalieUser, article.getCode());

        Assert.assertFalse(simpleWithdrawItemTask.isDriverLicenseRequired());

        Assert.assertFalse(simpleWithdrawItemTask.isArticleUnavailable());

        List<StoredArticle> storedArticles = simpleWithdrawItemTask.getStoredArticles();

        Predicate<StoredArticle> articleStoredInAccessibleLocationPredicate =
                StoredArticles.articleStoredInAccessibleLocationPredicate(magalieUser);

        Assert.assertTrue(Iterables.all(storedArticles, articleStoredInAccessibleLocationPredicate));

        Assert.assertEquals(3, storedArticles.size());

    }

    @Test
    public void testSimpleWithdrawOnUnavailableArticle() throws InvalidMagalieBarcodeException, IllegalWithdrawException {

        Article article = fixture("article4");

        SimpleWithdrawItemTask simpleWithdrawItemTask =
                service.getSimpleWithdrawItemTask(building, magalieUser, article.getCode());

        Assert.assertTrue(simpleWithdrawItemTask.isArticleUnavailable());

        Assert.assertFalse(simpleWithdrawItemTask.isDriverLicenseRequired());

    }

    @Test
    public void testSimpleWithdrawWithoutDriverLicense() throws InvalidMagalieBarcodeException, IllegalWithdrawException {

        magalieUser = fixture("corinne");

        Article article = fixture("article1");

        SimpleWithdrawItemTask simpleWithdrawItemTask =
                service.getSimpleWithdrawItemTask(building, magalieUser, article.getCode());

        Assert.assertFalse(simpleWithdrawItemTask.isDriverLicenseRequired());

        Assert.assertFalse(simpleWithdrawItemTask.isArticleUnavailable());

        List<StoredArticle> storedArticles = simpleWithdrawItemTask.getStoredArticles();

        Predicate<StoredArticle> articleStoredInAccessibleLocationPredicate =
                StoredArticles.articleStoredInAccessibleLocationPredicate(magalieUser);

        Assert.assertTrue(
                "all given locations must be accessible for user",
                Iterables.all(storedArticles, articleStoredInAccessibleLocationPredicate));

        Assert.assertEquals(1, storedArticles.size());

    }

}
