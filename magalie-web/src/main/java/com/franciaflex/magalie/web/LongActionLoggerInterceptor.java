package com.franciaflex.magalie.web;

/*-
 * #%L
 * MagaLiE :: UI
 * %%
 * Copyright (C) 2013 - 2019 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class LongActionLoggerInterceptor extends AbstractInterceptor {

    private static final Log log = LogFactory.getLog(LongActionLoggerInterceptor.class);

    private final ListMultimap<String, Long> stats =
            Multimaps.synchronizedListMultimap(LinkedListMultimap.<String, Long>create());

    @Override
    public void destroy() {
        if(log.isInfoEnabled()) {
            log.info("Statistiques d'utilisation : ");
            for (String actionName : stats.keySet()) {
                List<Long> actionStats = ImmutableList.copyOf(stats.get(actionName));
                int hitCount = actionStats.size();
                long sum = 0;
                for (Long aDuration : actionStats) {
                    sum += aDuration;
                }
                log.info("Action "
                        + " actionName = " + actionName
                        + " min = " + DurationFormatUtils.formatDurationHMS(Collections.min(actionStats))
                        + " max = " + DurationFormatUtils.formatDurationHMS(Collections.max(actionStats))
                        + " sum = " + DurationFormatUtils.formatDurationHMS(sum)
                        + " hit count = " + hitCount
                        + " average = " + DurationFormatUtils.formatDurationHMS(sum / hitCount)
                );
            }
        }
    }

    public String intercept(ActionInvocation invocation) throws Exception {

        Object action = invocation.getAction();

        if (action instanceof MagalieActionSupport) {
            Date before = new Date();
            String result = invocation.invoke();
            try {
                Date after = new Date();
                long duration = after.getTime() - before.getTime();
                String actionName = invocation.getProxy().getActionName();
                stats.put(actionName, duration);
                if (duration > 1000 && log.isWarnEnabled()) {
                    List<Long> actionStats = ImmutableList.copyOf(stats.get(actionName));
                    int hitCount = actionStats.size();
                    long sum = 0;
                    for (Long aDuration : actionStats) {
                        sum += aDuration;
                    }
                    log.warn("LONG ACTION - Action "
                            + " actionName = " + actionName
                            + " parameters = " + invocation.getInvocationContext().getParameters()
                            + " duration = " + DurationFormatUtils.formatDurationHMS(duration)
                            + " min = " + DurationFormatUtils.formatDurationHMS(Collections.min(actionStats))
                            + " max = " + DurationFormatUtils.formatDurationHMS(Collections.max(actionStats))
                            + " sum = " + DurationFormatUtils.formatDurationHMS(sum)
                            + " hit count = " + hitCount
                            + " average = " + DurationFormatUtils.formatDurationHMS(sum / hitCount)
                    );
                }
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("erreur pendant la journalisation des actions longues", e);
                }
            }
            return result;
        } else {
            // on ignore, c'est pas une action
            return invocation.invoke();
        }
    }

}
