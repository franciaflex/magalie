package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Supplier;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class SuppliersTest {

    @Test
    public void testOrderByNamesComparator() {

        ImmutableList<String> supplierNames = ImmutableList.of("d", "a", "c", "b");

        Iterable<Supplier> suppliers =
                Iterables.transform(
                        supplierNames,
                        new Function<String, Supplier>() {
                            @Override
                            public Supplier apply(String name) {
                                Supplier supplier = new Supplier();
                                supplier.setName(name);
                                return supplier;
                            }
                        });

        Set<Supplier> sortedSuppliers = Sets.newTreeSet(Suppliers.orderByNamesComparator());

        Iterables.addAll(sortedSuppliers, suppliers);

        Assert.assertEquals("a", Iterables.get(sortedSuppliers, 0).getName());
        Assert.assertEquals("b", Iterables.get(sortedSuppliers, 1).getName());
        Assert.assertEquals("c", Iterables.get(sortedSuppliers, 2).getName());
        Assert.assertEquals("d", Iterables.get(sortedSuppliers, 3).getName());

    }
}
