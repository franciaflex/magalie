package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

public class FakeMagalieServiceContext extends DefaultMagalieServiceContext {

    private static final Log log = LogFactory.getLog(FakeMagalieServiceContext.class);

    protected Date date;

    @Override
    public Date getNow() {
        Preconditions.checkState(date != null, "you must provide a date before running service test");
        if (log.isTraceEnabled()) {
            log.trace("injecting fake date in service: " + date);
        }
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
