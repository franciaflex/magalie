package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.dao.BuildingJpaDao;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;

import java.util.List;

public class BuildingsService implements MagalieService {

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public Building getBuilding(String buildingId) {

        BuildingJpaDao dao = serviceContext.getPersistenceContext().getBuildingDao();

        return dao.findById(buildingId);

    }

    public List<Building> getAllBuildingsByCompany(Company company) {

        BuildingJpaDao dao = serviceContext.getPersistenceContext().getBuildingDao();

        return dao.findAllByCompany(company);
    }
}
