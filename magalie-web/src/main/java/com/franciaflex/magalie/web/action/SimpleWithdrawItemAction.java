package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.StoredArticles;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.SimpleWithdrawItemTask;
import com.franciaflex.magalie.services.exception.IllegalWithdrawException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.service.LocationErrorsService;
import com.franciaflex.magalie.services.service.SimpleWithdrawItemService;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results({
    @Result(name="success", type="redirectAction", params = { "actionName", "prepare-withdraw-item!input" }),
    @Result(name="error", type="redirectAction", params = { "actionName", "prepare-withdraw-item!input" })
})
public class SimpleWithdrawItemAction extends MagalieActionSupport {

    private static final Log log = LogFactory.getLog(SimpleWithdrawItemAction.class);

    protected SimpleWithdrawItemService service;

    protected LocationErrorsService locationErrorsService;

    protected String storedArticleId;

    protected String articleBarcode;

    protected MagalieSession session;

    protected SimpleWithdrawItemTask simpleWithdrawItemTask;

    protected String destinationWarehouseId;

    protected String originLocationBarcode;

    public void setService(SimpleWithdrawItemService service) {
        this.service = service;
    }

    public void setLocationErrorsService(LocationErrorsService locationErrorsService) {
        this.locationErrorsService = locationErrorsService;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setArticleBarcode(String articleBarcode) {
        this.articleBarcode = articleBarcode;
    }

    public void setDestinationWarehouseId(String destinationWarehouseId) {
        this.destinationWarehouseId = destinationWarehouseId;
    }

    @Override
    public String input() {

        Building building = session.getBuilding();

        MagalieUser magalieUser = session.getMagalieUser();

        if (articleBarcode == null) {
            log.error("should never occur, barcode is validated before arriving in this action");
            addMessage("Le code barre " + articleBarcode + " n'est pas un code valide");
            return ERROR;
        }

        try {

            simpleWithdrawItemTask = service.getSimpleWithdrawItemTask(building, magalieUser, articleBarcode);

        } catch (InvalidMagalieBarcodeException e) {

            log.error("should never occur, barcode is validated before arriving in this action", e);

            addMessage("Le code barre " + articleBarcode + " n'est pas un code valide");

            return ERROR;

        } catch (IllegalWithdrawException e) {

            addMessage("Cet article ne peut pas être prélevé avec cette méthode");

            return ERROR;

        }

        if (simpleWithdrawItemTask.isDriverLicenseRequired()) {

            addMessage("Un permis est requis pour accéder à cet article");

            return ERROR;

        }

        if (simpleWithdrawItemTask.isArticleUnavailable()) {

            addMessage("Cet article n'est pas disponible");

            return ERROR;

        }

        return INPUT;

    }

    public String getDestinationWarehouseId() {
        return destinationWarehouseId;
    }

    public String getArticleBarcode() {
        return articleBarcode;
    }

    public SimpleWithdrawItemTask getSimpleWithdrawItemTask() {
        return simpleWithdrawItemTask;
    }

    public void setStoredArticleId(String storedArticleId) {
        this.storedArticleId = storedArticleId;
    }

    public void setOriginLocationBarcode(String originLocationBarcode) {
        this.originLocationBarcode = originLocationBarcode;
    }

    @Override
    public String execute() throws InvalidMagalieBarcodeException, IllegalWithdrawException {

        MagalieUser magalieUser = session.getMagalieUser();

        Building building = session.getBuilding();

        if (articleBarcode == null) {
            addFieldError("articleBarcode", "Ce n'est pas le code-barre d'un emplacement valide");
            return INPUT;
        }

        simpleWithdrawItemTask = service.getSimpleWithdrawItemTask(building, magalieUser, articleBarcode);

        Optional<StoredArticle> storedArticleOptional =
                Iterables.tryFind(
                        simpleWithdrawItemTask.getStoredArticles(),
                        StoredArticles.locationBarcodeEquals(originLocationBarcode));

        if (storedArticleOptional.isPresent()) {

            service.confirmStorageMovement(
                    magalieUser, destinationWarehouseId,
                    storedArticleOptional.get());

        } else {

            addFieldError("originLocationBarcode", "Ce n'est pas le code-barre d'un emplacement valide");

            return INPUT;

        }

        return SUCCESS;

    }

    public String reportError() throws InvalidMagalieBarcodeException, IllegalWithdrawException {

        MagalieUser magalieUser = session.getMagalieUser();

        Building building = session.getBuilding();

        if (articleBarcode == null) {
                addFieldError("originLocationBarcode", "Ce n'est pas le code-barre d'un emplacement valide : " + articleBarcode);
        } else {

            simpleWithdrawItemTask = service.getSimpleWithdrawItemTask(building, magalieUser, articleBarcode);

            Optional<StoredArticle> storedArticleOptional =
                    Iterables.tryFind(
                            simpleWithdrawItemTask.getStoredArticles(),
                            StoredArticles.locationBarcodeEquals(originLocationBarcode));

            if (storedArticleOptional.isPresent()) {

                StoredArticle storedArticle = storedArticleOptional.get();

                locationErrorsService.reportError(storedArticle.getLocation(), storedArticle.getArticle(), magalieUser);

                addActionMessage("Anomalie signalée");

                simpleWithdrawItemTask = service.getSimpleWithdrawItemTask(building, magalieUser, articleBarcode);

            } else {

                addFieldError("originLocationBarcode", "Ce n'est pas le code-barre d'un emplacement valide");

            }
        }

        return INPUT;

    }

}
