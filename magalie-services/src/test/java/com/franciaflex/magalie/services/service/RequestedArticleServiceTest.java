package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.dao.DeliveredRequestedListJpaDao;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.services.AbstractMagalieServiceTest;
import com.franciaflex.magalie.services.MagalieUserNotificationContext;
import com.franciaflex.magalie.services.StorageMovementConfirmation;
import com.franciaflex.magalie.services.exception.MagalieException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class RequestedArticleServiceTest extends AbstractMagalieServiceTest {

    private static final Log log = LogFactory.getLog(RequestedArticleServiceTest.class);

    protected RequestedArticleService service;

    protected MagalieUser magalieUser;

    protected Building building;

    protected static final String LIST_TYPE = "LOT DE FAB";

    private MagalieUserNotificationContext magalieUserNotificationContextMock;

    @Before
    public void setUp() throws ParseException {

        loadFixtures("fixtures");

        service = newService(RequestedArticleService.class);

        serviceContext.setDate(new Date(1363948427576l));

        magalieUser = fixture("bruno");

        building = fixture("B1");

        magalieUserNotificationContextMock = Mockito.mock(MagalieUserNotificationContext.class);

        serviceContext.setUserNotificationContext(magalieUserNotificationContextMock);

    }

    @Test
    public void testListTypes() {

        List<String> listTypes = service.getListTypes(building);

        Assert.assertEquals(1, listTypes.size());

    }

    @Test
    public void testFindOrderWhenNoRequestedArticles() {

        Set<RequestedArticle> emptySet = Sets.newHashSet();

        FindOrderToExecuteResult orderToExecute = service.findOrderToExecute(emptySet, magalieUser, building);

        Assert.assertTrue(orderToExecute.isNothingToDo());
        Assert.assertFalse(orderToExecute.isEverythingUnavailable());
        Assert.assertFalse(orderToExecute.isSuccess());
        Assert.assertFalse(orderToExecute.isDriverLicenseRequired());

    }

    @Test
    public void testFindOrderRequestedArticles() {

        FindOrderToExecuteResult orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);

        Assert.assertFalse(orderToExecute.isNothingToDo());
        Assert.assertFalse(orderToExecute.isEverythingUnavailable());
        Assert.assertFalse(orderToExecute.isDriverLicenseRequired());
        Assert.assertTrue(orderToExecute.isSuccess());

        Assert.assertNull(orderToExecute.getOldAffectation());
        Assert.assertNotNull(orderToExecute.getNewAffectation());

        Mockito.verify(magalieUserNotificationContextMock).notifyAffectedRequestedList(magalieUser, orderToExecute.getNewAffectation());

    }

    @Test
    public void testFindOrderRequestedArticlesForUnavailableArticle() {

        RequestedArticle requestedArticle4 = fixture("requestedArticle4");

        Set<RequestedArticle> fakePrioritySet = Sets.newHashSet();

        fakePrioritySet.add(requestedArticle4);

        FindOrderToExecuteResult orderToExecute =
                service.findOrderToExecute(fakePrioritySet, magalieUser, building);

        Assert.assertFalse(orderToExecute.isNothingToDo());
        Assert.assertTrue(orderToExecute.isEverythingUnavailable());
        Assert.assertFalse(orderToExecute.isDriverLicenseRequired());
        Assert.assertFalse(orderToExecute.isSuccess());

        Assert.assertNull(orderToExecute.getOldAffectation());
        Assert.assertNull(orderToExecute.getNewAffectation());

    }

    @Test
    public void testFindOrderRequestedArticlesNotAvailable() {

        // it is not available in expected quantity because spreading in different buildings

        RequestedArticle requestedArticle1 = fixture("requestedArticle1");

        Set<RequestedArticle> fakePrioritySet = Sets.newHashSet();

        fakePrioritySet.add(requestedArticle1);

        FindOrderToExecuteResult orderToExecute =
                service.findOrderToExecute(fakePrioritySet, magalieUser, building);

        Assert.assertFalse(orderToExecute.isNothingToDo());
        Assert.assertFalse(orderToExecute.isEverythingUnavailable());
        Assert.assertFalse(orderToExecute.isDriverLicenseRequired());
        Assert.assertTrue(orderToExecute.isSuccess());

    }

    @Test
    public void testFindOrderRequestedArticlesInventory() {
        // it is not available because in inventory
        RequestedArticle requestedArticle2 = fixture("requestedArticle2");
        Set<RequestedArticle> fakePrioritySet = Sets.newHashSet();
        fakePrioritySet.add(requestedArticle2);

        FindOrderToExecuteResult orderToExecute =
                service.findOrderToExecute(fakePrioritySet, magalieUser, building);

        Assert.assertFalse(orderToExecute.isNothingToDo());
        Assert.assertTrue(orderToExecute.isEverythingUnavailable());
        Assert.assertFalse(orderToExecute.isDriverLicenseRequired());
        Assert.assertFalse(orderToExecute.isSuccess());

    }

    @Test
    public void testFindOrderAffectations() throws MagalieException {

        ImmutableSet<StorageMovement> emptySet = ImmutableSet.of();

        FindOrderToExecuteResult orderToExecute;

        // user is affected to second list because article2 (list1) is in inventory, must know new affectation
        {
            orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);
            Preconditions.checkState(orderToExecute.isSuccess());

            Assert.assertNull(orderToExecute.getOldAffectation());
            Assert.assertNotNull(orderToExecute.getNewAffectation());
            Assert.assertEquals("requestedArticle6", orderToExecute.getRequestedArticle().getId());

            StorageMovementConfirmation confirmation = new StorageMovementConfirmation();
            confirmation.setRequestedArticleId(orderToExecute.getRequestedArticle().getId());
            service.onStorageMovementConfirmation(magalieUser, confirmation, emptySet);

        }

        // continuing second list, user affectation not changed
        {
            orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);
            Preconditions.checkState(orderToExecute.isSuccess());

            Assert.assertNull(orderToExecute.getOldAffectation());
            Assert.assertNull(orderToExecute.getNewAffectation());
            Assert.assertEquals("requestedArticle5", orderToExecute.getRequestedArticle().getId());

            StorageMovementConfirmation confirmation = new StorageMovementConfirmation();
            confirmation.setRequestedArticleId(orderToExecute.getRequestedArticle().getId());
            service.onStorageMovementConfirmation(magalieUser, confirmation, emptySet);

        }

        // second list is finished because article4 is not available, must know old affectation and new affectation
        {

            orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);
            Assert.assertFalse(orderToExecute.isSuccess());

            Assert.assertNull(orderToExecute.getNewAffectation());
            Assert.assertNotNull(orderToExecute.getOldAffectation());

        }

        // start a new list, we should have been affected to another list (third one)
        {
            orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);
            Preconditions.checkState(orderToExecute.isSuccess());
            Assert.assertEquals("requestedArticle8", orderToExecute.getRequestedArticle().getId());

            Assert.assertNotNull(orderToExecute.getNewAffectation());
            Assert.assertNull(orderToExecute.getOldAffectation());

            StorageMovementConfirmation confirmation = new StorageMovementConfirmation();
            confirmation.setRequestedArticleId(orderToExecute.getRequestedArticle().getId());
            service.onStorageMovementConfirmation(magalieUser, confirmation, emptySet);

        }

        // continuing third list, user affectation not changed
        {
            orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);
            Preconditions.checkState(orderToExecute.isSuccess());
            Assert.assertEquals("requestedArticle7", orderToExecute.getRequestedArticle().getId());

            Assert.assertNull(orderToExecute.getOldAffectation());
            Assert.assertNull(orderToExecute.getNewAffectation());

            StorageMovementConfirmation confirmation = new StorageMovementConfirmation();
            confirmation.setRequestedArticleId(orderToExecute.getRequestedArticle().getId());
            service.onStorageMovementConfirmation(magalieUser, confirmation, emptySet);

        }

        //article4 still unavailable
        {
            orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);

            Assert.assertTrue(orderToExecute.isEverythingUnavailable());
            // Assert.assertNotNull(orderToExecute.getOldAffectation());

        }

    }

    @Test
    public void testAffectationsLockLists() {

        FindOrderToExecuteResult orderToExecute = service.findOrderToExecute(magalieUser, building, LIST_TYPE);

        Preconditions.checkState(orderToExecute.isSuccess());

        DeliveredRequestedList firstAffectation = orderToExecute.getNewAffectation();

        Preconditions.checkNotNull(firstAffectation);

        // on essaie de travailler avec un autre utilisateur pour vérifier qu'il n'est pas affecté à la même liste

        MagalieUser otherMagalieUser = fixture("pascal");

        orderToExecute = service.findOrderToExecute(otherMagalieUser, building, LIST_TYPE);

        Preconditions.checkState(orderToExecute.isSuccess());

        DeliveredRequestedList otherAffectation = orderToExecute.getNewAffectation();

        Preconditions.checkNotNull(otherAffectation);

        EntityManager entityManager = getJpaEntityManagerRule().newEntityManager();
        DeliveredRequestedListJpaDao deliveredRequestedListJpaDao = new DeliveredRequestedListJpaDao(entityManager);
        List<DeliveredRequestedList> deliveredRequestedLists = deliveredRequestedListJpaDao.findAll();

        if (log.isDebugEnabled()) {
            log.debug("delivered requested list = " + deliveredRequestedLists);
        }

        Assert.assertEquals(2, deliveredRequestedLists.size());

        Assert.assertNotEquals(firstAffectation.getRequestedList(), otherAffectation.getRequestedList());

    }

}
