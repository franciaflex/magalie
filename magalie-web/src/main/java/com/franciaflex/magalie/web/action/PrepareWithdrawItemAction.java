package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.service.FulfilKanbanService;
import com.franciaflex.magalie.services.service.SimpleWithdrawItemService;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.opensymphony.xwork2.Preparable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;

@Results({
    @Result(name="success", type="redirectAction", params = { "actionName", "simple-withdraw-item!input", "articleBarcode", "${articleBarcode}", "destinationWarehouseId", "${destinationWarehouseId}"})
})
public class PrepareWithdrawItemAction extends MagalieActionSupport implements Preparable {

    private static final Log log = LogFactory.getLog(PrepareWithdrawItemAction.class);

    protected MagalieSession session;

    protected String articleBarcode;

    protected List<Warehouse> destinationWarehouses;

    protected String destinationWarehouseId;

    protected Double quantity;

    protected boolean askQuantity;

    protected Article article;

    protected Warehouse destinationWarehouse;

    protected FulfilKanbanService fulfilKanbanService;

    protected SimpleWithdrawItemService simpleWithdrawItemService;

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setFulfilKanbanService(FulfilKanbanService fulfilKanbanService) {
        this.fulfilKanbanService = fulfilKanbanService;
    }

    public void setSimpleWithdrawItemService(SimpleWithdrawItemService simpleWithdrawItemService) {
        this.simpleWithdrawItemService = simpleWithdrawItemService;
    }

    @Override
    public void prepare() {

        Building building = session.getBuilding();

        destinationWarehouses = fulfilKanbanService.getDestinationWarehouses(building);

        // let's help user by selecting by default the previously used destination warehouse

        Warehouse lastUsedDestinationWarehouseForKanbans = session.getLastUsedDestinationWarehouseForKanbans();

        if (lastUsedDestinationWarehouseForKanbans != null) {

            destinationWarehouseId = lastUsedDestinationWarehouseForKanbans.getId();

        }

    }

    public boolean isAskQuantity() {
        return askQuantity;
    }

    public String getDestinationWarehouseId() {
        return destinationWarehouseId;
    }

    public List<Warehouse> getDestinationWarehouses() {
        return destinationWarehouses;
    }

    public void setArticleBarcode(String articleBarcode) {
        this.articleBarcode = articleBarcode;
    }

    public void setDestinationWarehouseId(String destinationWarehouseId) {
        this.destinationWarehouseId = destinationWarehouseId;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @Override
    public void validate() {

        if (log.isDebugEnabled()) {
            log.debug("article barcode is " + articleBarcode);
        }

        destinationWarehouse = fulfilKanbanService.getStore(destinationWarehouseId);

        Company company = session.getCompany();

        // save used destination warehouse to propose it by default on next kanban
        session.setLastUsedDestinationWarehouseForKanbans(destinationWarehouse);

        try {

            article = fulfilKanbanService.getArticle(articleBarcode, company);

        } catch (InvalidMagalieBarcodeException e) {

            addFieldError("articleBarcode", "Le code barre n'est pas un code valide");

        }

        if (quantity != null) {

            addFieldError("quantity", "Il ne faut pas préciser la quantité");

        }

    }

    @Override
    public String execute() {

        session.setActivity(Activity.SIMPLE_WITHDRAW);

        return SUCCESS;

    }

    public Article getArticle() {
        return article;
    }

    public String getArticleBarcode() {
        return articleBarcode;
    }

}
