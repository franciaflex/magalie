package com.franciaflex.magalie.web;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.Collection;

/**
 * The single object every user will have in its session.
 *
 * @author bleny
 */
public class MagalieSession implements Serializable {

    /**
     * Key to store the {@link MagalieSession} instance in the session's map.
     */
    public static final String SESSION_PARAMETER = "magalieSession";

    protected MagalieUser magalieUser;

    protected Building building;

    protected Warehouse lastUsedDestinationWarehouseForKanbans;

    protected Activity activity;

    protected Collection<String> messages = Lists.newLinkedList();

    public MagalieUser getMagalieUser() {
        return magalieUser;
    }

    public void setMagalieUser(MagalieUser magalieUser) {
        this.magalieUser = magalieUser;
    }

    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Company getCompany() {

        Preconditions.checkNotNull(magalieUser);

        return magalieUser.getCompany();
    }

    public Warehouse getLastUsedDestinationWarehouseForKanbans() {
        return lastUsedDestinationWarehouseForKanbans;
    }

    public void setLastUsedDestinationWarehouseForKanbans(Warehouse lastUsedDestinationWarehouseForKanbans) {
        this.lastUsedDestinationWarehouseForKanbans = lastUsedDestinationWarehouseForKanbans;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("user", magalieUser)
                .add("building", building)
                .add("activity", activity)
                .toString();
    }

}
