package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.persistence.entity.Supplier;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Ordering;
import org.apache.commons.collections.comparators.BooleanComparator;

import java.util.Comparator;

public class StoredArticles {

    public static Ordering<StoredArticle> receptionPriorityComparator() {
        return Ordering.natural().onResultOf(getReceptionPriority());
    }

    public static Function<StoredArticle, String> getReceptionPriority() {
        return new GetReceptionPriority();
    }

    /**
     * Put fixed sites for given article first, or ignore it according to the value of
     * {@link com.franciaflex.magalie.persistence.entity.Article#isFixedLocationsFirst()}
     */
    protected static class FixedLocationForArticle implements Comparator<StoredArticle> {

        @Override
        public int compare(StoredArticle x, StoredArticle y) {
            Article article = x.getArticle();
            Preconditions.checkArgument(article.equals(y.getArticle()));
            boolean xIsInFixedLocation = article.isFixedLocation(x.getLocation());
            boolean yIsInFixedLocation = article.isFixedLocation(y.getLocation());
            int compare = BooleanComparator.getBooleanComparator(article.isFixedLocationsFirst()).compare(xIsInFixedLocation, yIsInFixedLocation);
            return compare;
        }

    }

    protected static class LocationWithHighestQuantityFirst implements Comparator<StoredArticle> {

        @Override
        public int compare(StoredArticle o1, StoredArticle o2) {
            int compare = Double.compare(o2.getQuantity(), o1.getQuantity());
            return compare;
        }

    }

    protected static class AccessibleLocationFirst implements Comparator<StoredArticle> {

        protected Predicate<StoredArticle> articleStoredInAccessibleLocationPredicate;

        public AccessibleLocationFirst(MagalieUser magalieUser) {
            articleStoredInAccessibleLocationPredicate =
                    articleStoredInAccessibleLocationPredicate(magalieUser);
        }

        @Override
        public int compare(StoredArticle o1, StoredArticle o2) {
            boolean isLocation1Accessible = articleStoredInAccessibleLocationPredicate.apply(o1);
            boolean isLocation2Accessible = articleStoredInAccessibleLocationPredicate.apply(o2);
            if (isLocation1Accessible && ! isLocation2Accessible) {
                return -1;
            } else if ( ! isLocation1Accessible && isLocation2Accessible) {
                return 1;
            } else {
                return 0;
            }
        }

    }

    protected static class GetLocationFunction implements Function<StoredArticle, Location> {

        @Override
        public Location apply(StoredArticle storedArticle) {
            return storedArticle.getLocation();
        }
    }

    protected static class ArticleStoredInLocationsRequiringDriverLicenseFirstComparator implements Comparator<StoredArticle> {

        @Override
        public int compare(StoredArticle o1, StoredArticle o2) {
            return Locations.locationRequiringDriverLicenseFirstComparator().compare(o1.getLocation(), o2.getLocation());
        }
    }

    public static Comparator<StoredArticle> locationWithHighestQuantityFirst() {
        return new LocationWithHighestQuantityFirst();
    }

    public static Comparator<StoredArticle> locationWithLowestQuantityFirstComparator() {
        return Ordering.from(locationWithHighestQuantityFirst()).reverse();
    }

    public static Comparator<StoredArticle> accessibleLocationFirstComparator(MagalieUser magalieUser) {
        return new AccessibleLocationFirst(magalieUser);
    }

    public static Comparator<StoredArticle> fixedLocationForArticleComparator() {
        return new FixedLocationForArticle();
    }

    public static Function<StoredArticle, Location> getLocationFunction() {
        return new GetLocationFunction();
    }

    protected static class NotEmptyPredicate implements Predicate<StoredArticle> {

        @Override
        public boolean apply(StoredArticle input) {
            return input.getQuantity() > 0.;
        }
    }

    public static Predicate<StoredArticle> articleStoredInAccessibleLocationPredicate(MagalieUser magalieUser) {
        Predicate<StoredArticle> predicate = Predicates.compose(
                Locations.accessibleLocationPredicate(magalieUser),
                getLocationFunction());
        return predicate;
    }

    protected static class GetArticleFunction implements Function<StoredArticle, Article> {

        @Override
        public Article apply(StoredArticle storedArticle) {
            return storedArticle.getArticle();
        }
    }

    public static Comparator<StoredArticle> articleStoredInLocationsRequiringDriverLicenseFirstComparator() {
        return new ArticleStoredInLocationsRequiringDriverLicenseFirstComparator();
    }

    public static Predicate<StoredArticle> notEmpty() {
        return new NotEmptyPredicate();
    }

    public static Function<StoredArticle, Article> getArticleFunction() {
        return new GetArticleFunction();
    }

    public static Function<StoredArticle, Supplier> getArticleSupplierFunction() {
        return Functions.compose(Articles.getSupplierFunction(), getArticleFunction());
    }

    public static Predicate<StoredArticle> locationBarcodeEquals(String originLocationBarcode) {
        return Predicates.compose(Locations.barcodeEquals(originLocationBarcode), getLocationFunction());
    }

    protected static class GetReceptionPriority implements Function<StoredArticle, String> {

        @Override
        public String apply(StoredArticle input) {
            return input.getReceptionPriority();
        }
    }

}
