<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <title>Connexion</title>
</head>

<s:form>

    <s:select label="Utilisateur"
           key="magalieUserId"
           list="allMagalieUsers"
           listKey="id"
           listValue="name"
           required="true"
    />

    <s:select label="Bâtiment"
           key="buildingId"
           list="allBuildings"
           listKey="id"
           listValue="code"
           required="true"
    />

    <div class="btn-group">
        <s:submit value="S'identifier" cssClass="btn btn-primary" />
    </div>
</s:form>
