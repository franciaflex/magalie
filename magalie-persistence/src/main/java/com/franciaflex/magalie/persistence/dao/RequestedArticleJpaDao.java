package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedListStatus;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.persistence.entity.RequestedList;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class RequestedArticleJpaDao extends AbstractRequestedArticleJpaDao {

    public RequestedArticleJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<RequestedArticle> findAllUndelivered(RequestedList requestedList) {
        TypedQuery<RequestedArticle> query = createQuery("from RequestedArticle ra where ra.requestedList = :requestedList and ra not in (select dra.requestedArticle from DeliveredRequestedArticle dra where dra.requestedArticle.requestedList = :requestedList)");
        query.setParameter("requestedList", requestedList);
        query.setHint("org.hibernate.fetchSize", 500);
        return findAll(query);
    }

    @Override
    public List<RequestedArticle> findAllUndelivered(Building building, String listType) {
        TypedQuery<RequestedArticle> query = createQuery(
                " from RequestedArticle ra" +
                " where ra.requestedList.building = :building" +
                  " and ra.requestedList.listType = :listType" +
                  " and ra.requestedList not in (select drl.requestedList from DeliveredRequestedList drl where drl.status = :affectedDeliveredRequestedListStatus OR drl.status = :completeDeliveredRequestedListStatus)" +
                  " and ra not in (select dra.requestedArticle from DeliveredRequestedArticle dra)");
        query.setParameter("building", building);
        query.setParameter("listType", listType);
        query.setParameter("affectedDeliveredRequestedListStatus", DeliveredRequestedListStatus.AFFECTED);
        query.setParameter("completeDeliveredRequestedListStatus", DeliveredRequestedListStatus.COMPLETE);
        query.setHint("org.hibernate.fetchSize", 500);
        return findAll(query);
    }

}
