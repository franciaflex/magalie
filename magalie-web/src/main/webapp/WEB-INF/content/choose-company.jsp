<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>


<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <title>Entreprise</title>
</head>


<s:iterator value="companies" status="stat">
    <li>
        <s:set var="elementCompanyId">companyId<s:property value="#stat.index" /></s:set>
        <s:url action="login!input" id="loginUrl">
            <s:param name="companyId" value="%{id}" />
        </s:url>
        <s:if test="#stat.index < shortcuts.size()">
            <script type="text/javascript">
                $(document).ready(function () {
                    var id = '<s:property value="elementCompanyId" />';
                    bindKey('<s:property value="shortcuts.get(#stat.index)" />', function(){ redirectTo($('#' + id));});
                });
            </script>
           <s:a href="%{loginUrl}" id="%{elementCompanyId}">
                <s:property value="name" />
                (<s:property value="shortcuts.get(#stat.index)" />)
            </s:a>
        </s:if>
        <s:else>
            <s:a href="%{loginUrl}" ><s:property value="name" />
            </s:a>
        </s:else>
    </li>
</s:iterator>

