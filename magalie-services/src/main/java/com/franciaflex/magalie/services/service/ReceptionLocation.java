package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.Warehouse;

public class ReceptionLocation {

    protected Location location;

    protected double alreadyStoredQuantity;

    public ReceptionLocation(Location location) {
        this.location = location;
    }

    public String getBarcode() {
        return location.getBarcode();
    }

    public String getCode() {
        return location.getCode();
    }

    public boolean isRequiredCraneMan() {
        return location.isRequiredCraneMan();
    }

    public Warehouse getWarehouse() {
        return location.getWarehouse();
    }

    public boolean isFullLocation() {
        return location.isFullLocation();
    }

    public int getRequiredAccreditationLevel() {
        return location.getRequiredAccreditationLevel();
    }

    public String getId() {
        return location.getId();
    }

    public double getAlreadyStoredQuantity() {
        return alreadyStoredQuantity;
    }

    public void setAlreadyStoredQuantity(double alreadyStoredQuantity) {
        this.alreadyStoredQuantity = alreadyStoredQuantity;
    }

}
