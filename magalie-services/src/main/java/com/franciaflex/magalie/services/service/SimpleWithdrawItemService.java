package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.StoredArticles;
import com.franciaflex.magalie.persistence.dao.LocationDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementJpaDao;
import com.franciaflex.magalie.persistence.dao.WarehouseJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.SimpleWithdrawItemTask;
import com.franciaflex.magalie.services.exception.IllegalWithdrawException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

public class SimpleWithdrawItemService implements MagalieService {

    private static final Log log = LogFactory.getLog(SimpleWithdrawItemService.class);

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public SimpleWithdrawItemTask getSimpleWithdrawItemTask(Building building, MagalieUser magalieUser, String articleBarcode)
            throws InvalidMagalieBarcodeException, IllegalWithdrawException {

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        MagalieBarcodeService magalieBarcodeService =
                serviceContext.newService(MagalieBarcodeService.class);

        Company company = magalieUser.getCompany();

        Article article = magalieBarcodeService.getArticle(articleBarcode, company);

        IllegalWithdrawException.checkWithdrawIsLegal(article);

        Iterable<StoredArticle> storedArticles =
                articleStorageService.getStoredArticles(building, article);

        Predicate<StoredArticle> articleStoredInAccessibleLocationPredicate =
                StoredArticles.articleStoredInAccessibleLocationPredicate(magalieUser);

        boolean articleUnavailable = Iterables.isEmpty(storedArticles);

        boolean articleAvailable = ! articleUnavailable;

        boolean driverLicenseRequired;

        if (articleAvailable) {

            boolean articleIsAccessible =
                    Iterables.any(
                            storedArticles,
                            articleStoredInAccessibleLocationPredicate);

            driverLicenseRequired = ! articleIsAccessible;

        } else {

            driverLicenseRequired = false;

        }

        storedArticles = Iterables.filter(storedArticles, articleStoredInAccessibleLocationPredicate);

        SimpleWithdrawItemTask simpleWithdrawItemTask =
                new SimpleWithdrawItemTask(article, storedArticles, driverLicenseRequired, articleUnavailable);

        return simpleWithdrawItemTask;

    }

    public void confirmStorageMovement(MagalieUser magalieUser, String destinationWarehouseId, StoredArticle storedArticle) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        WarehouseJpaDao warehouseDao = persistenceContext.getWarehouseDao();

        Date now = serviceContext.getNow();

        // principle is taking a stored article and get everything (all quantity)

        Location originLocation = storedArticle.getLocation();

        Article article = storedArticle.getArticle();

        double quantity = storedArticle.getQuantity();

        if (log.isInfoEnabled()) {
            log.info("user " + magalieUser.getLogin() + " took " +
                    quantity + " " + article.getUnit() + " from " +
                    (originLocation != null ? originLocation.getBarcode() : "null"));
        }

        // find actual destination location

        Warehouse destinationWarehouse = warehouseDao.findById(destinationWarehouseId);

        Location destinationLocation = getDestinationLocation(destinationWarehouse);

        // create new storage movement

        StorageMovement storageMovement = new StorageMovement();

        storageMovement.setOriginLocation(originLocation);

        storageMovement.setDestinationLocation(destinationLocation);

        storageMovement.setMagalieUser(magalieUser);

        storageMovement.setArticle(article);

        storageMovement.setActualQuantity(quantity);

        storageMovement.setConfirmDate(now);

        // save storage movement

        StorageMovementJpaDao storageMovementDao = persistenceContext.getStorageMovementDao();

        storageMovementDao.persist(storageMovement);

        persistenceContext.commit();

    }

    protected Location getDestinationLocation(Warehouse destinationWarehouse) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        LocationDao locationDao = persistenceContext.getLocationDao();

        Location location = locationDao.find(Locations.codeForWarehouseWithoutLocations(), destinationWarehouse);

        return location;

    }

}
