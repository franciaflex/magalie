<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>



<head>
   <script type="text/javascript">
        $(document).ready(function () {
            bindKey('Esc', function(){ redirectTo($('#chooseActivityLink'));});
        });
   </script>
</head>

<s:if test="suppliersToReceive.somethingToReceive">
    <ul class="oneItemPerLine">
        <li>
            <script type="text/javascript">
                $(document).ready(function () {
                    bindKey('<s:property value="shortcuts.get(0)" />', function(){ redirectTo($('#prepareArticleReceptionLink'));});
                });
            </script>
            <s:url action="prepare-article-reception" id="prepareArticleReceptionUrl" />
            <s:a href="%{prepareArticleReceptionUrl}" id="prepareArticleReceptionLink">
                (<s:property value="shortcuts.get(0)" />) Tous les fournisseurs
            </s:a>
        </li>

        <s:iterator value="suppliersToReceive.suppliersByGroup.entries()" status="stat">

            <%-- Afficher le nom du groupe --%>
            <s:set var="currentGroup"><s:property value="key" /></s:set>
            <s:if test="#lastGroup != #currentGroup">
                <s:set var="lastGroup"><s:property value="key" /></s:set>
                Groupe <s:property value="key" />
            </s:if>

            <li>
                <s:set var="prepareArticleReceptionId">prepareArticleReceptionId<s:property value="#stat.index" /></s:set>
                <s:url action="prepare-article-reception" id="prepareArticleReceptionUrl">
                    <s:param name="supplierId" value="%{value.id}" />
                </s:url>
                <s:if test="#stat.index + 1 < shortcuts.size()">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var id = '<s:property value="prepareArticleReceptionId" />';
                            bindKey('<s:property value="shortcuts.get(#stat.index + 1)" />', function(){ redirectTo($('#' + id));});
                        });
                    </script>
                    <s:a href="%{prepareArticleReceptionUrl}" id="%{prepareArticleReceptionId}">
                        (<s:property value="shortcuts.get(#stat.index + 1)" />) <s:property value="value.name" />
                    </s:a>
                </s:if>
                <s:else>
                    <s:a href="%{prepareArticleReceptionUrl}" >
                        <s:property value="value.name" />
                    </s:a>
                </s:else>
            </li>
        </s:iterator>

    </ul>
</s:if>

<s:url action="choose-activity" id="chooseActivityUrl"/>
<s:a href="%{chooseActivityUrl}" cssClass="btn" id="chooseActivityLink">Changer d'activité (Esc)</s:a>
