package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.dao.ArticleDao;
import com.franciaflex.magalie.persistence.dao.LocationDao;
import com.franciaflex.magalie.persistence.dao.PreparedArticleReceptionJpaDao;
import com.franciaflex.magalie.persistence.dao.StoredArticleJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.PreparedArticleReception;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.exception.PreparedArticleReceptionAlreadyStoredException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MagalieBarcodeService implements MagalieService {

    private static final Log log = LogFactory.getLog(MagalieBarcodeService.class);

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public Article getArticle(String articleBarcode, Company company) throws InvalidMagalieBarcodeException {

        ArticleDao articleDao = serviceContext.getPersistenceContext().getArticleDao();

        Article article = articleDao.findByCode(articleBarcode, company);

        if (article == null) {

            if (log.isWarnEnabled()) {
                log.warn("no article with code " + articleBarcode + " for company " + company);
            }

            throw new InvalidMagalieBarcodeException(
                    "no article with code " + articleBarcode + " for company " + company);

        }

        return article;

    }

    public Location getLocation(String locationBarcode, Building building) throws InvalidMagalieBarcodeException {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        LocationDao locationDao = persistenceContext.getLocationDao();

        Location location = locationDao.findByBarcode(locationBarcode, building);

        if (location == null) {

            if (log.isWarnEnabled()) {
                log.warn("no location with code " + locationBarcode + " in building " + building);
            }

            throw new InvalidMagalieBarcodeException(
                    "no location with code " + locationBarcode + " in building " + building);

        }

        return location;

    }

    public PreparedArticleReception getPreparedArticleReception(Building building, String barcode)
            throws InvalidMagalieBarcodeException, PreparedArticleReceptionAlreadyStoredException {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        PreparedArticleReceptionJpaDao dao =
                persistenceContext.getPreparedArticleReceptionDao();

        PreparedArticleReception preparedArticleReception =
                dao.findByBarcode(barcode);

        if (preparedArticleReception == null) {
            throw new InvalidMagalieBarcodeException("'" + barcode +"' is not a existing prepared article reception");
        }

        StoredArticleJpaDao storedArticleDao =
                persistenceContext.getStoredArticleDao();

        Article article = preparedArticleReception.getArticle();

        StoredArticle storedArticle =
                storedArticleDao.findInReception(building, article);

        if (storedArticle == null) {
            throw new PreparedArticleReceptionAlreadyStoredException(preparedArticleReception);
        }

        return preparedArticleReception;
    }

}
