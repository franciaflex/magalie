package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedListStatus;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.RequestedList;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class DeliveredRequestedListJpaDao extends AbstractDeliveredRequestedListJpaDao {

    public DeliveredRequestedListJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public DeliveredRequestedList findByAffectedTo(MagalieUser affectedTo) {

        TypedQuery<DeliveredRequestedList> query = createQuery(
                "from DeliveredRequestedList drl where drl.status = :affected and drl.affectedTo = :affectedTo");

        query.setParameter("affectedTo", affectedTo);

        query.setParameter("affected", DeliveredRequestedListStatus.AFFECTED);

        return findUniqueOrNull(query);

    }

    @Override
    public DeliveredRequestedList findByRequestedList(RequestedList requestedList) {

        TypedQuery<DeliveredRequestedList> query = createQuery(
                "from DeliveredRequestedList drl where drl.requestedList = :requestedList");

        query.setParameter("requestedList", requestedList);

        return findUniqueOrNull(query);

    }
}
