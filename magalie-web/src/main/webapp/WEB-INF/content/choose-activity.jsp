<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>
<%@ taglib prefix="sb"        uri="/struts-bootstrap-tags" %>
<%@ taglib prefix="sj"        uri="/struts-jquery-tags" %>

<head>
    <script>
        $(document).ready(function() {
            bindKey('1', function(){ redirectTo($('#fulfilKanbanLink'));});
            bindKey('2', function(){ redirectTo($('#deliverRequestedArticleLink'));});
            bindKey('3', function(){ redirectTo($('#prepareArticleReceptionLink'));});
            bindKey('4', function(){ redirectTo($('#preparePreparedArticleReceptionLink'));});
            bindKey('5', function(){ redirectTo($('#storageTransferLink'));});
            bindKey('6', function(){ redirectTo($('#prepareWithdrawItemLink'));});
            bindKey('Esc', function(){ redirectTo($('#logoutLink'));});
        });
    </script>
</head>

<header>
    <p>
        <s:property value="magalieUser.name" /> (<s:property value="building.code" />)
    </p>
</header>

<s:url action="fulfil-kanban!input" id="fulfilKanbanUrl"/>
<s:a href="%{fulfilKanbanUrl}" cssClass="btn btn-block" id="fulfilKanbanLink">Kanbans (1)</s:a>

<s:url action="deliver-requested-article!input" id="deliverRequestedArticleUrl"/>
<s:a href="%{deliverRequestedArticleUrl}" cssClass="btn btn-block"  id="deliverRequestedArticleLink">Listes à servir (2)</s:a>

<s:url action="prepare-article-reception!input" id="prepareArticleReceptionUrl"/>
<s:a href="%{prepareArticleReceptionUrl}" cssClass="btn btn-block" id="prepareArticleReceptionLink" >Réceptions fournisseurs (3)</s:a>

<s:url action="prepare-prepared-article-reception!input" id="preparePreparedArticleReceptionUrl"/>
<s:a href="%{preparePreparedArticleReceptionUrl}" cssClass="btn btn-block" id="preparePreparedArticleReceptionLink" >Rangement toiles (4)</s:a>

<s:url action="storage-transfer-location!input" id="storageTransferUrl"/>
<s:a href="%{storageTransferUrl}" cssClass="btn btn-block" id="storageTransferLink" >Transfert de stock (5)</s:a>

<s:url action="prepare-withdraw-item!input" id="prepareWithdrawItemUrl"/>
<s:a href="%{prepareWithdrawItemUrl}" cssClass="btn btn-block" id="prepareWithdrawItemLink" >Sorties toiles (6)</s:a>

<s:url action="logout" id="logoutUrl"/>
<s:a href="%{logoutUrl}" cssClass="btn btn-block" id="logoutLink" >Déconnexion (Esc)</s:a>
