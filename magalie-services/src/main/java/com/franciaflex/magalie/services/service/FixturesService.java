package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.dao.*;
import com.franciaflex.magalie.persistence.entity.*;
import com.franciaflex.magalie.services.MagalieFixtures;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Map;

public class FixturesService implements MagalieService {

    private static final Log log = LogFactory.getLog(FixturesService.class);

    protected MagalieServiceContext serviceContext;

    protected Map<String, MagalieFixtures> fixtureSets = Maps.newHashMap();

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public MagalieFixtures cleanDatabaseAndLoadFixtures(String fixturesSetName) {

        return loadFixtures(fixturesSetName, true);

    }

    public MagalieFixtures loadFixtures(String fixturesSetName) {

        return loadFixtures(fixturesSetName, false);

    }

    protected MagalieFixtures loadFixtures(String fixturesSetName, boolean cleanDatabase) {

        boolean devMode = serviceContext.getMagalieApplicationConfig().isDevMode();

        Preconditions.checkState(devMode);

        MagalieFixtures fixtures = fixtureSets.get(fixturesSetName);

        if (fixtures == null) {

            fixtures = new MagalieFixtures(fixturesSetName);

            fixtureSets.put(fixturesSetName, fixtures);

            if (log.isInfoEnabled()) {
                log.info("will restore database with fixture set");
            }

            JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

            if (cleanDatabase) {

                persistenceContext.clearDatabase();

            }

            CompanyJpaDao companyDao = persistenceContext.getCompanyDao();

            Collection<Company> companies = fixtures.fixture("companies");

            for (Company company : companies) {

                companyDao.persist(company);

            }

            MagalieUserJpaDao magalieUserDao = persistenceContext.getMagalieUserDao();

            Collection<MagalieUser> users = fixtures.fixture("users");

            for (MagalieUser user : users) {

                magalieUserDao.persist(user);

            }

            BuildingJpaDao buildingDao = persistenceContext.getBuildingDao();

            Collection<Building> buildings = fixtures.fixture("buildings");

            for (Building building : buildings) {

                buildingDao.persist(building);

            }

            WarehouseJpaDao warehouseDao = persistenceContext.getWarehouseDao();

            Collection<Warehouse> warehouses = fixtures.fixture("warehouses");

            for (Warehouse warehouse : warehouses) {

                warehouseDao.persist(warehouse);

            }

            LocationJpaDao locationDao = persistenceContext.getLocationDao();

            Collection<Location> locations = fixtures.fixture("locations");

            for (Location location : locations) {

                locationDao.persist(location);

            }

            SupplierJpaDao supplierDao = persistenceContext.getSupplierDao();

            Collection<Supplier> suppliers = fixtures.fixture("suppliers");

            for (Supplier supplier : suppliers) {

                supplierDao.persist(supplier);

            }

            ArticleJpaDao articleDao = persistenceContext.getArticleDao();

            Collection<Article> articles = fixtures.fixture("articles");

            for (Article article : articles) {

                articleDao.persist(article);

            }

            KanbanJpaDao kanbanDao = persistenceContext.getKanbanDao();

            Collection<Kanban> kanbans = fixtures.fixture("kanbans");

            for (Kanban kanban : kanbans) {

                kanbanDao.persist(kanban);

            }

            StoredArticleJpaDao storedArticleDao = persistenceContext.getStoredArticleDao();

            Collection<StoredArticle> storedArticles = fixtures.fixture("storedArticles");

            for (StoredArticle storedArticle : storedArticles) {

                storedArticleDao.persist(storedArticle);

            }

            RequestedListJpaDao requestedListDao = persistenceContext.getRequestedListDao();

            Collection<RequestedList> requestedLists = fixtures.fixture("requestedLists");

            for (RequestedList requestedList : requestedLists) {

                requestedListDao.persist(requestedList);

            }

            RequestedArticleJpaDao requestedArticleDao = persistenceContext.getRequestedArticleDao();

            Collection<RequestedArticle> requestedArticles = fixtures.fixture("requestedArticles");

            for (RequestedArticle requestedArticle : requestedArticles) {

                requestedArticleDao.persist(requestedArticle);

            }

            PreparedArticleReceptionJpaDao preparedArticleReceptionDao = persistenceContext.getPreparedArticleReceptionDao();

            Collection<PreparedArticleReception> preparedArticleReceptions = fixtures.fixture("preparedArticleReceptions");

            for (PreparedArticleReception preparedArticleReception : preparedArticleReceptions) {

                preparedArticleReceptionDao.persist(preparedArticleReception);

            }

            InventoryArticleJpaDao inventoryArticleJpaDao = persistenceContext.getInventoryArticleDao();

            Collection<InventoryArticle> inventoryArticles = fixtures.fixture("inventoryArticles");

            for (InventoryArticle inventoryArticle : inventoryArticles) {

                inventoryArticleJpaDao.persist(inventoryArticle);

            }

            persistenceContext.commit();

        }

        return fixtures;

    }

}
