package com.franciaflex.magalie.web;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieApplicationConfig;
import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.DefaultMagalieServiceContext;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.service.FixturesService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import java.util.Map;

public class MagalieApplicationContext {

    private static final Log log = LogFactory.getLog(MagalieApplicationContext.class);

    protected static final String APPLICATION_CONTEXT_PARAMETER = "magalieApplicationContext";

    protected MagalieApplicationConfig applicationConfig;

    protected EntityManagerFactory entityManagerFactory;

    protected String contextPath;

    protected StrutsActionMessagesUserNotificationContext userNotificationContext;

    public MagalieApplicationContext(String contextPath) {
        this.contextPath = contextPath;
        userNotificationContext = new StrutsActionMessagesUserNotificationContext();
    }

    public MagalieApplicationConfig getMagalieApplicationConfig() {
        if (applicationConfig == null) {
            applicationConfig = new MagalieApplicationConfig(contextPath);
        }
        return applicationConfig;
    }

    public EntityManager newEntityManager() {

        if (entityManagerFactory == null) {

            MagalieApplicationConfig applicationConfig = getMagalieApplicationConfig();

            Map<String, String> jpaParameters = applicationConfig.getJpaParameters();

            if (log.isInfoEnabled()) {
                log.info("creating entity manager factory");
            }

            try {

                entityManagerFactory = Persistence.createEntityManagerFactory("magaliePersistenceUnit", jpaParameters);

            } catch (PersistenceException e) {

                if (log.isErrorEnabled()) {
                    log.error("unable to create entity manager factory", e);
                }

                throw e;

            }

        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager;

    }

    public MagalieServiceContext newServiceContext(EntityManager entityManager) {

        // FIXME brendan 04/06/13 very strange behavior, raise exception when loading a page
//        Preconditions.checkArgument(
//                entityManager != null && entityManager.isOpen(),
//                "unable to find transaction for service context");

        DefaultMagalieServiceContext serviceContext = new DefaultMagalieServiceContext();

        JpaMagaliePersistenceContext jpaMagaliePersistenceContext =
                new JpaMagaliePersistenceContext(entityManager);

        serviceContext.setPersistenceContext(jpaMagaliePersistenceContext);

        serviceContext.setMagalieApplicationConfig(applicationConfig);

        serviceContext.setUserNotificationContext(userNotificationContext);

        return serviceContext;

    }

    public void init() {

        if (getMagalieApplicationConfig().isDevMode()) {

            if (log.isInfoEnabled()) {
                log.info("devMode is enabled, loading fixtures set");
            }

            EntityManager entityManager = newEntityManager();

            MagalieServiceContext serviceContext =
                    newServiceContext(entityManager);

            FixturesService fixturesService =
                    serviceContext.newService(FixturesService.class);

            fixturesService.cleanDatabaseAndLoadFixtures("fixtures");

            entityManager.close();

        }

    }

    public void close() {

        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {

            if (log.isInfoEnabled()) {
                log.info("stopping magalie, will close entity manager factory");
            }

            entityManagerFactory.close();

        }

        for (Map.Entry<MagalieUser, String> magalieUserMessage : userNotificationContext.getUsersNotifications().entries()) {

            MagalieUser magalieUser = magalieUserMessage.getKey();

            String message = magalieUserMessage.getValue();

            if (log.isWarnEnabled()) {
                log.warn("notification message was not delivered magalieUser=" + magalieUser + " message=" + message);
            }

        }

    }

    public StrutsActionMessagesUserNotificationContext getUserNotificationContext() {
        return userNotificationContext;
    }

}
