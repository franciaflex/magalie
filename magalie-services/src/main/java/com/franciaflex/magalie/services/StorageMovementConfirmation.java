package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Set;

public class StorageMovementConfirmation {

    protected String articleId;

    protected Set<String> storageMovementIds;

    protected Set<String> locationInErrorIds;

    protected String requestedArticleId;

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public Set<String> getStorageMovementIds() {
        return storageMovementIds;
    }

    public void setStorageMovementIds(Set<String> storageMovementIds) {
        this.storageMovementIds = storageMovementIds;
    }

    public Set<String> getLocationInErrorIds() {
        return locationInErrorIds;
    }

    public void setLocationInErrorIds(Set<String> locationInErrorIds) {
        this.locationInErrorIds = locationInErrorIds;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public String getRequestedArticleId() {
        return requestedArticleId;
    }

    public void setRequestedArticleId(String requestedArticleId) {
        this.requestedArticleId = requestedArticleId;
    }
}
