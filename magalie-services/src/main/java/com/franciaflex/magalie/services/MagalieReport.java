package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.StorageMovements;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.LocationError;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.UnavailableArticle;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.util.Date;
import java.util.List;

public class MagalieReport {

    protected Date reportDate;

    protected List<MagalieUser> allMagalieUsers;

    protected List<StorageMovement> allStorageMovements;

    protected List<LocationError> allLocationErrors;

    protected List<DeliveredRequestedList> allDeliveredRequestedLists;

    protected List<UnavailableArticle> allUnavailableArticles;

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setAllMagalieUsers(List<MagalieUser> allMagalieUsers) {
        this.allMagalieUsers = allMagalieUsers;
    }

    public List<MagalieUser> getAllMagalieUsers() {
        return allMagalieUsers;
    }

    public void setAllStorageMovements(List<StorageMovement> allStorageMovements) {
        this.allStorageMovements = allStorageMovements;
    }

    public List<StorageMovement> getConfirmedStorageMovements() {
        ImmutableList<StorageMovement> confirmedStorageMovements =
                ImmutableList.copyOf(
                        Iterables.filter(
                                allStorageMovements,
                                StorageMovements.storageMovementIsConfirmed()
                        )
                );
        return confirmedStorageMovements;
    }

    public List<StorageMovement> getNotConfirmedStorageMovements() {
        ImmutableList<StorageMovement> confirmedStorageMovements =
                ImmutableList.copyOf(
                        Iterables.filter(
                                allStorageMovements,
                                StorageMovements.storageMovementIsNotConfirmed()
                        )
                );
        return confirmedStorageMovements;
    }

    public List<LocationError> getAllLocationErrors() {
        return allLocationErrors;
    }

    public void setAllLocationErrors(List<LocationError> allLocationErrors) {
        this.allLocationErrors = allLocationErrors;
    }

    public void setAllDeliveredRequestedLists(List<DeliveredRequestedList> allDeliveredRequestedLists) {
        this.allDeliveredRequestedLists = allDeliveredRequestedLists;
    }

    public List<DeliveredRequestedList> getAllDeliveredRequestedLists() {
        return allDeliveredRequestedLists;
    }

    public void setAllUnavailableArticles(List<UnavailableArticle> allUnavailableArticles) {
        this.allUnavailableArticles = allUnavailableArticles;
    }

    public List<UnavailableArticle> getAllUnavailableArticles() {
        return allUnavailableArticles;
    }
}
