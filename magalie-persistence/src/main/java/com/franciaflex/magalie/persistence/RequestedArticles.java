package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.persistence.entity.RequestedList;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.nuiton.jpa.api.JpaEntities;

import java.util.Comparator;

public class RequestedArticles {

    public static Comparator<RequestedArticle> comparator() {
        Comparator<RequestedList> requestedListComparator = RequestedLists.comparator();
        Comparator<RequestedArticle> comparator =
                Ordering.compound(
                        Lists.newArrayList(
                                Ordering.from(requestedListComparator).onResultOf(getRequestList()),
                                priorityComparator(),
                                Ordering.natural().onResultOf(new GetId())
                        )
                );
        return comparator;
    }

    public static Comparator<RequestedArticle> priorityComparator() {
        return Ordering.natural().onResultOf(new GetPriority());
    }

    protected static class GetRequestList implements Function<RequestedArticle, RequestedList> {

        @Override
        public RequestedList apply(RequestedArticle requestedArticle) {
            return requestedArticle.getRequestedList();
        }
    }

    public static Function<RequestedArticle, RequestedList> getRequestList() {
        return new GetRequestList();
    }

    protected static class GetPriority implements Function<RequestedArticle, String> {

        @Override
        public String apply(RequestedArticle requestedArticle) {
            return requestedArticle.getPriority();
        }
    }

    protected static class GetId implements Function<RequestedArticle, String> {

        @Override
        public String apply(RequestedArticle requestedArticle) {
            return requestedArticle.getId();
        }
    }

}
