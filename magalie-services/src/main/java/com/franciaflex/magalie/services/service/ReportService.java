package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.dao.StorageMovementJpaDao;
import com.franciaflex.magalie.persistence.dao.UnavailableArticleJpaDao;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.LocationError;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.services.MagalieReport;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;

import java.util.Date;
import java.util.List;

public class ReportService implements MagalieService {

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public MagalieReport getReport() {

        MagalieReport magalieReport = new MagalieReport();

        Date now = serviceContext.getNow();

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        magalieReport.setReportDate(now);

        MagalieUsersService magalieUsersService = serviceContext.newService(MagalieUsersService.class);

        List<MagalieUser> allMagalieUsers = magalieUsersService.getAllMagalieUsers();

        magalieReport.setAllMagalieUsers(allMagalieUsers);

        StorageMovementJpaDao storageMovementDao = persistenceContext.getStorageMovementDao();

        List<StorageMovement> allStorageMovements = storageMovementDao.findAll();

        magalieReport.setAllStorageMovements(allStorageMovements);

        LocationErrorsService locationErrorsService = serviceContext.newService(LocationErrorsService.class);

        List<LocationError> allLocationsInError = locationErrorsService.getAllLocationErrors();

        magalieReport.setAllLocationErrors(allLocationsInError);

        RequestedArticleService requestedArticleService = serviceContext.newService(RequestedArticleService.class);

        List<DeliveredRequestedList> allDeliveredRequestedLists = requestedArticleService.getAllDeliveredRequestedLists();

        magalieReport.setAllDeliveredRequestedLists(allDeliveredRequestedLists);

        UnavailableArticleJpaDao unavailableArticleDao = persistenceContext.getUnavailableArticleDao();

        magalieReport.setAllUnavailableArticles(unavailableArticleDao.findAll());

        return magalieReport;

    }

}
