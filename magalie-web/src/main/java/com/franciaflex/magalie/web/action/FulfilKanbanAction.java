package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.exception.ArticleNotAvailableForKanbanException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.service.FulfilKanbanService;
import com.franciaflex.magalie.services.service.RealTimeStorageMovementTask;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.opensymphony.xwork2.Preparable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;


@Results({
    @Result(name="input", type = "dispatcher", location = "/WEB-INF/content/prepare-withdraw-item-input.jsp"),
    @Result(name="success", type="redirectAction", params = { "actionName", "withdraw-item!input", "articleId", "${articleId}", "destinationLocationId", "${destinationLocationId}", "expectedQuantity", "${quantity}" })
})
public class FulfilKanbanAction extends MagalieActionSupport implements Preparable {

    private static final Log log = LogFactory.getLog(FulfilKanbanAction.class);

    protected MagalieSession session;

    protected FulfilKanbanService service;

    protected String articleBarcode;

    protected List<Warehouse> destinationWarehouses;

    protected String destinationWarehouseId;

    protected Double quantity;

    protected boolean askQuantity;

    protected Article article;

    protected Warehouse destinationWarehouse;

    protected String destinationLocationId;

    public void setService(FulfilKanbanService service) {
        this.service = service;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    @Override
    public void prepare() {

        session.setActivity(Activity.KANBANS);

        Building building = session.getBuilding();

        destinationWarehouses = service.getDestinationWarehouses(building);

        // let's help user by selecting by default the previously used destination warehouse

        Warehouse lastUsedDestinationWarehouseForKanbans = session.getLastUsedDestinationWarehouseForKanbans();

        if (lastUsedDestinationWarehouseForKanbans != null) {

            destinationWarehouseId = lastUsedDestinationWarehouseForKanbans.getId();

        }

    }

    public boolean isAskQuantity() {
        return askQuantity;
    }

    public String getDestinationWarehouseId() {
        return destinationWarehouseId;
    }

    public List<Warehouse> getDestinationWarehouses() {
        return destinationWarehouses;
    }

    public void setArticleBarcode(String articleBarcode) {
        this.articleBarcode = articleBarcode;
    }

    public void setDestinationWarehouseId(String destinationWarehouseId) {
        this.destinationWarehouseId = destinationWarehouseId;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String execute() {

        session.setActivity(Activity.KANBANS);

        if (log.isDebugEnabled()) {
            log.debug("article barcode is " + articleBarcode);
        }

        Company company = session.getCompany();

        if (destinationWarehouseId == null) {

            addFieldError("destinationWarehouseId", "Vous devez sélectionner un magasin de destination");

            return INPUT;

        }

        destinationWarehouse = service.getStore(destinationWarehouseId);

        // save used destination warehouse to propose it by default on next kanban
        session.setLastUsedDestinationWarehouseForKanbans(destinationWarehouse);

        try {

            article = service.getArticle(articleBarcode, company);

        } catch (InvalidMagalieBarcodeException e) {

            addFieldError("articleBarcode", "Le code barre n'est pas un code valide");

            return INPUT;

        }

        Building building = session.getBuilding();

        MagalieUser magalieUser = session.getMagalieUser();

        // just to check that user will be able to get something
        RealTimeStorageMovementTask realTimeStorageMovementTask =
                service.getRealTimeStorageMovementTask(building, magalieUser, article.getId());

        if (realTimeStorageMovementTask.isArticleUnavailable()) {

            addActionError("Article sans stock");

            return INPUT;

        }

        if (realTimeStorageMovementTask.isDriverLicenseRequired()) {

            addActionError("Article inaccessible sans permis");

            return INPUT;

        }

        try {

            quantity = service.getDefinedQuantity(article, destinationWarehouse);

        } catch (ArticleNotAvailableForKanbanException e) {

            addFieldError("articleBarcode", "Article non disponible par kanban");

            return INPUT;

        }

        Location destinationLocation = service.getDestinationLocation(destinationWarehouse);

        destinationLocationId = destinationLocation.getId();

        return SUCCESS;

    }

    public String getArticleId() {
        return article.getId();
    }

    public Double getQuantity() {
        return quantity;
    }

    public String getDestinationLocationId() {
        return destinationLocationId;
    }
}
