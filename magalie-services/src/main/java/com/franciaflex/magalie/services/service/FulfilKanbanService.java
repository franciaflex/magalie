package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.dao.KanbanDao;
import com.franciaflex.magalie.persistence.dao.WarehouseDao;
import com.franciaflex.magalie.persistence.dao.WarehouseJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Kanban;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.exception.ArticleNotAvailableForKanbanException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public class FulfilKanbanService implements MagalieService {

    private static final Log log = LogFactory.getLog(FulfilKanbanService.class);

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public Warehouse getStore(String storeId) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        WarehouseJpaDao warehouseDao = persistenceContext.getWarehouseDao();

        Warehouse warehouse = warehouseDao.findById(storeId);

        return warehouse;

    }

    public double getDefinedQuantity(Article article, Warehouse destinationStore) throws ArticleNotAvailableForKanbanException {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        KanbanDao dao = persistenceContext.getKanbanDao();

        Kanban kanban = dao.find(article, destinationStore);

        if (kanban == null) {
            throw new ArticleNotAvailableForKanbanException(article);
        }

        double quantityInKanban = kanban.getRequiredQuantity();

        return quantityInKanban;

    }

    public List<Warehouse> getDestinationWarehouses(Building building) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        WarehouseDao warehouseDao = persistenceContext.getWarehouseDao();

        List<Warehouse> destinationWarehouses = warehouseDao.findAllWithoutLocations(building);

        return destinationWarehouses;

    }

    public Article getArticle(String articleBarcode, Company company) throws InvalidMagalieBarcodeException {

        MagalieBarcodeService magalieBarcodeService =
                serviceContext.newService(MagalieBarcodeService.class);

        Article article = magalieBarcodeService.getArticle(articleBarcode, company);

        return article;

    }

    public Location getDestinationLocation(Warehouse destinationWarehouse) {

        SimpleWithdrawItemService simpleWithdrawItemService =
                serviceContext.newService(SimpleWithdrawItemService.class);

        Location destinationLocation =
                simpleWithdrawItemService.getDestinationLocation(destinationWarehouse);

        return destinationLocation;

    }

    public RealTimeStorageMovementTask getRealTimeStorageMovementTask(Building building, MagalieUser magalieUser, String articleId) {

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        RealTimeStorageMovementTask realTimeStorageMovementTask =
                articleStorageService.getRealTimeStorageMovementTask(building, magalieUser, articleId);

        return realTimeStorageMovementTask;

    }

}
