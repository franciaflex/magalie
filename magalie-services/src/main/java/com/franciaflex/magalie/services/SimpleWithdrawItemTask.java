package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.util.List;

public class SimpleWithdrawItemTask {

    protected Article article;

    protected List<StoredArticle> storedArticles;

    protected boolean driverLicenseRequired;

    protected boolean articleUnavailable;

    public SimpleWithdrawItemTask(Article article, Iterable<StoredArticle> storedArticles, boolean driverLicenseRequired, boolean articleUnavailable) {
        this.article = article;
        this.storedArticles = Lists.newArrayList(storedArticles);
        this.driverLicenseRequired = driverLicenseRequired;
        this.articleUnavailable = articleUnavailable;
    }

    public Article getArticle() {
        return article;
    }

    public List<StoredArticle> getStoredArticles() {
        return storedArticles;
    }

    public boolean isDriverLicenseRequired() {
        if (driverLicenseRequired) {
            Preconditions.checkState( ! articleUnavailable);
        }
        return driverLicenseRequired;
    }

    public boolean isArticleUnavailable() {
        if (articleUnavailable) {
            Preconditions.checkState( ! driverLicenseRequired);
        }
        return articleUnavailable;
    }
}
