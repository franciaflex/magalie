package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.StoredArticle;

import java.util.List;

public interface StoredArticleDao {

    List<StoredArticle> findAllForArticleInBuilding(Article article, Building building);

    List<StoredArticle> findAllReceivedForAllSupplier(Building building);

    List<StoredArticle> findAllReceivedForSupplier(Building building, String supplierId);

    StoredArticle findDetachedById(String storedArticleId);

    List<StoredArticle> findAllByLocation(Location location);

    StoredArticle findInReception(Building building, Article article);

}
