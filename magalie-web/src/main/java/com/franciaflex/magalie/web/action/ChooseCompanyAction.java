package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.services.service.CompanyService;
import com.franciaflex.magalie.web.MagalieActionSupport;

import java.util.List;

/**
 * @author Bavencoff
 */
public class ChooseCompanyAction extends MagalieActionSupport {

    protected CompanyService service;

    protected List<Company> companies;

    public void setService(CompanyService service) {
        this.service = service;
    }

    @Override
    public String execute() {

        companies = service.getAllCompanies();

        return SUCCESS;

    }

    public List<Company> getCompanies() {
        return companies;
    }

}
