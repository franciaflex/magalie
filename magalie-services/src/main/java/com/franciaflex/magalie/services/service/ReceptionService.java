package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.StoredArticles;
import com.franciaflex.magalie.persistence.dao.ArticleJpaDao;
import com.franciaflex.magalie.persistence.dao.LocationDao;
import com.franciaflex.magalie.persistence.dao.LocationJpaDao;
import com.franciaflex.magalie.persistence.dao.PreparedArticleReceptionJpaDao;
import com.franciaflex.magalie.persistence.dao.ReceivedPreparedArticleReceptionJpaDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementJpaDao;
import com.franciaflex.magalie.persistence.dao.StoredArticleDao;
import com.franciaflex.magalie.persistence.dao.StoredArticleJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.PreparedArticleReception;
import com.franciaflex.magalie.persistence.entity.ReceivedPreparedArticleReception;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.persistence.entity.Supplier;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.exception.PreparedArticleReceptionAlreadyStoredException;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReceptionService implements MagalieService {

    private static final Log log = LogFactory.getLog(ReceptionService.class);

    /** disabled, for a warehouse with 1000+ locations, can lead to a page of 1Mo+, now the locations are loaded on demand */
    protected static final boolean LIMIT_EXTRA_LOCATIONS_TO_50 = true;

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public SuppliersToReceive getReceivedSuppliers(Building building) {

        Set<StoredArticle> allReceivedArticles = getReceivedArticles(building, null);

        Iterable<Supplier> suppliers =
                Sets.newHashSet( // to prevent multiple occurrence of the same supplier
                        Iterables.transform(
                                allReceivedArticles,
                                StoredArticles.getArticleSupplierFunction()
                        )
                );

        SuppliersToReceive suppliersToReceive = new SuppliersToReceive(suppliers);

        return suppliersToReceive;

    }

    public Set<StoredArticle> getReceivedArticles(Building building, String supplierId) {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        StoredArticleDao storedArticleDao =
                persistenceContext.getStoredArticleDao();

        Iterable<StoredArticle> receivedArticles;

        if (supplierId == null) {

            receivedArticles = storedArticleDao.findAllReceivedForAllSupplier(building);

        } else {

            receivedArticles = storedArticleDao.findAllReceivedForSupplier(building, supplierId);

        }

        // remove all items that have already been moved from the reception location

        StorageMovementDao storageMovementDao =
                persistenceContext.getStorageMovementDao();

        List<StorageMovement> allStorageMovementsInReception =
                storageMovementDao.findAllInReception(building);

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        receivedArticles = articleStorageService.computeActualQuantities(receivedArticles, allStorageMovementsInReception);

        Set<StoredArticle> storedArticlesByPriority =
                Sets.newTreeSet(StoredArticles.receptionPriorityComparator());

        Iterables.addAll(storedArticlesByPriority, receivedArticles);

        if (log.isTraceEnabled()) {
            log.trace(storedArticlesByPriority.size() + " stored articles found in reception for building "
                    + building + " and supplierId " + supplierId + ": "
                    + storedArticlesByPriority);
        }

        return storedArticlesByPriority;

    }

    public ReceptionTask getReceptionTask(String storedArticleId) {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        StoredArticleJpaDao storedArticleDao =
                persistenceContext.getStoredArticleDao();

        StoredArticle storedArticle =
                storedArticleDao.findDetachedById(storedArticleId);

        StorageMovementDao storageMovementDao =
                persistenceContext.getStorageMovementDao();

        List<StorageMovement> allStorageMovementsImpactingStoredArticle =
                storageMovementDao.findAllImpactingStoredArticle(storedArticle);

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        Iterable<StoredArticle> storedArticleSingleton = Lists.newArrayList(storedArticle);

        storedArticleSingleton =
                articleStorageService.computeActualQuantities(
                        storedArticleSingleton,
                        allStorageMovementsImpactingStoredArticle);

        double quantity = Iterables.getOnlyElement(storedArticleSingleton).getQuantity();

        ReceptionTask receptionTask = buildReceptionTask(storedArticle, quantity);

        if (log.isInfoEnabled()) {
            log.info("reception task is " + receptionTask);
        }

        return receptionTask;

    }

    protected ReceptionTask buildReceptionTask(StoredArticle storedArticle, double quantity) {

        Article article = storedArticle.getArticle();

        Warehouse warehouse = storedArticle.getLocation().getWarehouse();

        if (log.isTraceEnabled()) {
            log.trace("looking for locations to receive for " + article + " in " + warehouse);
        }

        LocationErrorsService locationErrorsService =
                serviceContext.newService(LocationErrorsService.class);

        // a predicate to remove reception locations because it's stupid to
        // move an article from reception location to another reception location
        // also, remove full locations and locations reported in error

        Predicate<Location> isAcceptableForReception =
                Predicates.and(
                        ImmutableSet.of(
                                Locations.isNotReceptionLocation(),
                                Locations.isNotFullLocation(),
                                locationErrorsService.getLocationNotReportedInErrorPredicate(article)
                        )
                );

        ReceptionTask receptionTask = new ReceptionTask(storedArticle, quantity);

        // first step, add fixed locations for this article first

        Set<Location> fixedLocations = article.getFixedLocationsInBuilding(warehouse.getBuilding());

        Iterable<Location> acceptableFixedLocations = Iterables.filter(fixedLocations, isAcceptableForReception);

        receptionTask.setFixedLocations(acceptableFixedLocations);

        // second step, add locations with already stored quantities

        Set<StoredArticle> alreadyUsedLocations = getAlreadyUsedLocations(warehouse, article);

        Iterable<StoredArticle> acceptableAlreadyUsedLocations =
                Iterables.filter(
                        alreadyUsedLocations,
                        Predicates.compose(isAcceptableForReception, StoredArticles.getLocationFunction()));

        receptionTask.setAlreadyUsedLocations(acceptableAlreadyUsedLocations);

        // third step, add extra locations

        Set<Location> extraLocations = getExtraLocations(warehouse);

        Iterable<Location> acceptableExtraLocations = Iterables.filter(extraLocations, isAcceptableForReception);

        receptionTask.setExtraLocations(acceptableExtraLocations);

        if (log.isDebugEnabled()) {
            log.debug("locations to receive articles for article "
                    + article + " in warehouse " + warehouse + " are acceptable = "
                    + StringUtils.join(acceptableAlreadyUsedLocations, ", ") +
                    " extra = " + StringUtils.join(acceptableExtraLocations, ", "));
        }

        return receptionTask;

    }

    protected Set<Location> getExtraLocations(Warehouse warehouse) {

        Set<Location> locations = Sets.newLinkedHashSet();

        // third step, add extra locations
        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        LocationDao locationDao =
                persistenceContext.getLocationDao();

        // add all locations in the same warehouse
        locations.addAll(locationDao.findAllWithoutReception(warehouse, LIMIT_EXTRA_LOCATIONS_TO_50));

        return locations;

    }

    protected Set<StoredArticle> getAlreadyUsedLocations(Warehouse warehouse, Article article) {

        // add locations where there is already quantities stored
        // we will suggest locations with highest quantities first

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        Iterable<StoredArticle> storedArticles =
                articleStorageService.getStoredArticles(warehouse.getBuilding(), article);

        Ordering<StoredArticle> comparator = Ordering.compound(
                Lists.newArrayList(
                        StoredArticles.locationWithHighestQuantityFirst()
                )
        );

        Set<StoredArticle> sortedStoredArticles = Sets.newTreeSet(comparator);

        Iterables.addAll(sortedStoredArticles, storedArticles);

        return sortedStoredArticles;

    }

    public ReceptionTask getReceptionTaskForPreparedArticleReception(Building building, String barcode) throws PreparedArticleReceptionAlreadyStoredException, InvalidMagalieBarcodeException {

        PreparedArticleReception preparedArticleReception =
                getPreparedArticleReception(building, barcode);

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        StoredArticleJpaDao storedArticleDao =
                persistenceContext.getStoredArticleDao();

        Article article = preparedArticleReception.getArticle();

        StoredArticle storedArticle =
                storedArticleDao.findInReception(building, article);

        ReceptionTask receptionTask = buildReceptionTask(
                storedArticle,
                preparedArticleReception.getQuantity());

        if (log.isInfoEnabled()) {
            log.info("reception task for prepared article reception '" + barcode + "' is " + receptionTask);
        }

        return receptionTask;

    }

    public void confirmReception(MagalieUser magalieUser, ReceptionConfirmation receptionConfirmation, String preparedArticleReceptionBarcode) {

        if (log.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("{");
            for (Map.Entry<String,Double> entry:receptionConfirmation.getLocationIdToStoredQuantities().entrySet()) {
                builder.append(entry.getKey()).append("->").append(entry.getValue()).append(" ; ");
            }
            builder.append("}");

            log.info("Confirmation de la reception : Article : " + receptionConfirmation.getStoredArticleId() + " , emplacements et quantités :" + builder.toString());
        }

        String storedArticleId = receptionConfirmation.getStoredArticleId();

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        StoredArticleJpaDao storedArticleDao =
                persistenceContext.getStoredArticleDao();

        StoredArticle storedArticle =
                storedArticleDao.findById(storedArticleId);

        LocationJpaDao locationDao = persistenceContext.getLocationDao();

        StorageMovementJpaDao storageMovementDao = persistenceContext.getStorageMovementDao();

        Date now = serviceContext.getNow();

        Set<StorageMovement> storageMovements = Sets.newHashSet();

        for (Map.Entry<String, Double> locationToStoredQuantity : receptionConfirmation.getLocationIdToStoredQuantities().entrySet()) {

            StorageMovement storageMovement = new StorageMovement();

            Location originLocation = storedArticle.getLocation();

            storageMovement.setOriginLocation(originLocation);

            Location destinationLocation = locationDao.findById(locationToStoredQuantity.getKey());

            storageMovement.setDestinationLocation(destinationLocation);

            storageMovement.setMagalieUser(magalieUser);

            storageMovement.setArticle(storedArticle.getArticle());

            storageMovement.setActualQuantity(locationToStoredQuantity.getValue());

            storageMovement.setMovementDate(now);

            storageMovement.setConfirmDate(now);

            if (log.isWarnEnabled()) {
                if (destinationLocation.isFullLocation()) {
                    log.warn("stored article in full destination location, should never occur " + storageMovement);
                }
            }

            storageMovementDao.persist(storageMovement);

            storageMovements.add(storageMovement);

        }

        if (preparedArticleReceptionBarcode != null) {

            PreparedArticleReceptionJpaDao preparedArticleReceptionDao =
                    persistenceContext.getPreparedArticleReceptionDao();

            PreparedArticleReception preparedArticleReception =
                    preparedArticleReceptionDao.findByBarcode(preparedArticleReceptionBarcode);

            ReceivedPreparedArticleReception newReceivedPreparedArticleReception =
                    new ReceivedPreparedArticleReception();

            newReceivedPreparedArticleReception.setPreparedArticleReception(preparedArticleReception);

            newReceivedPreparedArticleReception.setStorageMovements(storageMovements);

            ReceivedPreparedArticleReceptionJpaDao receivedPreparedArticleReceptionDao =
                    persistenceContext.getReceivedPreparedArticleReceptionDao();

            receivedPreparedArticleReceptionDao.persist(newReceivedPreparedArticleReception);

        }

        persistenceContext.commit();

        if (log.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("{");
            for (StorageMovement storageMovement:storageMovements) {
                builder.append(storageMovement).append("\n");
            }
            builder.append("}");

            log.info("StorageMovement enregistrés : " + builder.toString());
        }

    }

    public PreparedArticleReception getPreparedArticleReception(Building building, String barcode) throws InvalidMagalieBarcodeException, PreparedArticleReceptionAlreadyStoredException {

        MagalieBarcodeService magalieBarcodeService =
                serviceContext.newService(MagalieBarcodeService.class);

        PreparedArticleReception preparedArticleReception =
                magalieBarcodeService.getPreparedArticleReception(building, barcode);

        return preparedArticleReception;

    }

    public ReceptionLocation getReceptionLocation(Building building, MagalieUser magalieUser, String locationBarcode, String articleId) throws InvalidMagalieBarcodeException {

        ArticleJpaDao articleDao = serviceContext.getPersistenceContext().getArticleDao();

        Article article = articleDao.findById(articleId);

        MagalieBarcodeService magalieBarcodeService =
                serviceContext.newService(MagalieBarcodeService.class);

        Location location = magalieBarcodeService.getLocation(locationBarcode, building);

        if (location.isFullLocation()) {

            if (log.isInfoEnabled()) {
                log.info(magalieUser + " scanned barcode " + locationBarcode + " for reception but location " + location + " is full, reporting error");
            }

            LocationErrorsService locationErrorsService =
                    serviceContext.newService(LocationErrorsService.class);

            locationErrorsService.reportError(location, article, magalieUser);

        }

        ReceptionLocation receptionLocation = new ReceptionLocation(location);

        // on essaie de déterminer la quantité qui se trouve dans cet emplacement

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        Iterable<StoredArticle> storedArticles =
                articleStorageService.getStoredArticles(building, article);

        ImmutableMap<Location,StoredArticle> storedArticlesByLocation =
                Maps.uniqueIndex(storedArticles, StoredArticles.getLocationFunction());

        StoredArticle storedArticle = storedArticlesByLocation.get(location);

        if (storedArticle == null) {
            receptionLocation.setAlreadyStoredQuantity(0.);
        } else {
            receptionLocation.setAlreadyStoredQuantity(storedArticle.getQuantity());
        }

        return receptionLocation;

    }

    /**
     * On cherche à savoir si pour un storedArticle donné, il y a encore des articles à recevoir pour le même fournisseur
     */
    public Optional<Supplier> isSupplierHaveOtherArticlesToReceive(Building building, String storedArticleId) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        StoredArticleJpaDao storedArticleDao = persistenceContext.getStoredArticleDao();

        StoredArticle storedArticle = storedArticleDao.findById(storedArticleId);

        Supplier supplier = storedArticle.getArticle().getSupplier();

        Optional<Supplier> result;

        if (supplier == null) {

            result = Optional.absent();

        } else {

            SuppliersToReceive receivedSuppliers = getReceivedSuppliers(building);

            boolean isSupplierHaveOtherArticlesToReceive = receivedSuppliers.getSuppliersByGroup().values().contains(supplier);

            if (isSupplierHaveOtherArticlesToReceive) {

                result = Optional.of(supplier);

            } else {

                result = Optional.absent();

            }

        }

        return result;

    }

}
