package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieApplicationConfig;
import com.franciaflex.magalie.services.MagalieReport;
import com.franciaflex.magalie.services.service.ReportService;
import com.franciaflex.magalie.web.MagalieActionSupport;

public class ReportAction extends MagalieActionSupport {

    protected ReportService service;

    protected MagalieReport report;

    protected MagalieApplicationConfig config;

    public void setService(ReportService service) {
        this.service = service;
    }

    public void setConfig(MagalieApplicationConfig config) {
        this.config = config;
    }

    @Override
    public String execute() {

        report = service.getReport();

        return SUCCESS;

    }

    public MagalieReport getReport() {
        return report;
    }

    public boolean isDevMode() {
        return config.isDevMode();
    }
}
