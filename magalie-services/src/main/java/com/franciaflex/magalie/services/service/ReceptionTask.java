package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.StoredArticles;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Set;

public class ReceptionTask {

    protected StoredArticle storedArticle;

    protected double quantity;

    protected Iterable<Location> fixedLocations;

    protected Iterable<StoredArticle> alreadyUsedLocations;

    protected Iterable<Location> extraLocations;

    public ReceptionTask(StoredArticle storedArticle, double quantity) {
        this.storedArticle = storedArticle;
        this.quantity = quantity;
    }

    public StoredArticle getStoredArticle() {
        return storedArticle;
    }

    public Set<ReceptionLocation> getLocations() {
        ImmutableMap<Location, StoredArticle> locationToStoredArticle =
                Maps.uniqueIndex(alreadyUsedLocations, StoredArticles.getLocationFunction());
        Set<ReceptionLocation> receptionLocations = Sets.newLinkedHashSet();
        ImmutableSet<Location> allLocations =
                ImmutableSet.<Location> builder()
                        .addAll(fixedLocations)
                        .addAll(Iterables.transform(alreadyUsedLocations, StoredArticles.getLocationFunction()))
                        .addAll(extraLocations)
                        .build();
        for (Location location : allLocations) {
            ReceptionLocation receptionLocation = new ReceptionLocation(location);
            StoredArticle storedArticleInLocation = locationToStoredArticle.get(location);
            if (storedArticleInLocation == null) {
                receptionLocation.setAlreadyStoredQuantity(0.);
            } else {
                receptionLocation.setAlreadyStoredQuantity(storedArticleInLocation.getQuantity());
            }
            receptionLocations.add(receptionLocation);
        }
        return receptionLocations;
    }

    public double getQuantity() {
        return quantity;
    }

    public boolean isUserMustChooseExtraLocation() {
        return Iterables.isEmpty(fixedLocations) && Iterables.isEmpty(alreadyUsedLocations);
    }

    public void setFixedLocations(Iterable<Location> fixedLocations) {
        this.fixedLocations = fixedLocations;
    }

    public void setAlreadyUsedLocations(Iterable<StoredArticle> alreadyUsedLocations) {
        this.alreadyUsedLocations = alreadyUsedLocations;
    }

    public void setExtraLocations(Iterable<Location> extraLocations) {
        this.extraLocations = extraLocations;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
