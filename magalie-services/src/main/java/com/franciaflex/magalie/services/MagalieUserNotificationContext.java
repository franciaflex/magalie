package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.MagalieUser;

import java.util.Set;

/**
 * Ce contrat représente pour les services une façon de notifier un utilisateur
 * pour différent messages.
 *
 * @author bleny
 */
public interface MagalieUserNotificationContext {

    void notifyUnaffectedRequestedListDriverLicenseRequired(MagalieUser magalieUser, DeliveredRequestedList oldAffectation);

    void notifyUnaffectedRequestedListForEverythingUnavailable(MagalieUser magalieUser, DeliveredRequestedList oldAffectation);

    void notifyUnaffectedRequestedListForCompletedRequestedList(MagalieUser magalieUser, DeliveredRequestedList oldAffectation);

    void notifyAffectedRequestedList(MagalieUser magalieUser, DeliveredRequestedList newAffectation);

    void notifyInventoryBlockedArticle(MagalieUser magalieUser, Set<String> articles);

}
