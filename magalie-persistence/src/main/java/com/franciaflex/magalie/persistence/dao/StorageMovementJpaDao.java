package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.google.common.collect.Maps;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StorageMovementJpaDao extends AbstractStorageMovementJpaDao {

    public StorageMovementJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<StorageMovement> findAllByArticle(Article article) {
        Query query = entityManager.createQuery("from StorageMovement sm where sm.article = :article");
        query.setParameter("article", article);
        return query.getResultList();
    }

    @Override
    public List<StorageMovement> findAllInReception(Building building) {
        Query query = entityManager.createQuery(
                " from StorageMovement sm" +
                " where sm.originLocation.code = :codeForReceptionLocations and sm.originLocation.warehouse.building = :building" +
                " or " +
                " sm.destinationLocation.code = :codeForReceptionLocations and sm.destinationLocation.warehouse.building = :building");
        query.setParameter("building", building);
        query.setParameter("codeForReceptionLocations", Locations.codeForReceptionLocations());
        return query.getResultList();
    }

    @Override
    public List<StorageMovement> findAllImpactingStoredArticle(StoredArticle storedArticle) {
        Query query = entityManager.createQuery("from StorageMovement sm where sm.article = :article and (sm.originLocation = :location or sm.destinationLocation = :location)");
        query.setParameter("location", storedArticle.getLocation());
        query.setParameter("article", storedArticle.getArticle());
        return query.getResultList();
    }

    @Override
    public List<StorageMovement> findAll() {
        Query query = entityManager.createQuery("from StorageMovement sm order by sm.movementDate");
        return query.getResultList();
    }

    @Override
    public List<StorageMovement> findAllUnconfirmed(MagalieUser magalieUser) {
        Map<String, Object> storageMovementProperties = Maps.newHashMap();
        storageMovementProperties.put(StorageMovement.PROPERTY_MAGALIE_USER, magalieUser);
        storageMovementProperties.put(StorageMovement.PROPERTY_CONFIRM_DATE, null);
        List<StorageMovement> storageMovements = findAllByProperties(storageMovementProperties);
        return storageMovements;
    }

    @Override
    public List<StorageMovement> findAllByIds(Set<String> ids) {
        Query query = entityManager.createQuery("from StorageMovement sm where sm.id in (:ids)");
        query.setParameter("ids", ids);
        return query.getResultList();
    }
}
