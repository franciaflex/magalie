package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.AbstractMagalieServiceTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;

import java.text.ParseException;
import java.util.Date;

public class ArticleStorageServiceTest extends AbstractMagalieServiceTest {

    private static final Log log = LogFactory.getLog(ArticleStorageServiceTest.class);

    protected ArticleStorageService service;

    protected Building building;

    protected Location destinationLocation;

    protected MagalieUser magalieUser;

    @Before
    public void setUp() throws ParseException {

        loadFixtures("fixtures");

        building = fixture("B1");

        destinationLocation = fixture("location5");

        magalieUser = fixture("bruno");

        service = newService(ArticleStorageService.class);

        serviceContext.setDate(new Date(1363948427576l));

    }

}
