package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedListStatus;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class RequestedListJpaDao extends AbstractRequestedListJpaDao {

    public RequestedListJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<String> findAllDistinctListTypes(Building building) {
        TypedQuery<String> query =
                entityManager.createQuery(
                        " select distinct rl.listType"
                      + " from RequestedList rl"
                      + " where rl.building = :building and rl not in (select drl.requestedList from DeliveredRequestedList drl where drl.status = :affectedDeliveredRequestedListStatus OR drl.status = :completeDeliveredRequestedListStatus)"
                      , String.class);
        query.setParameter("affectedDeliveredRequestedListStatus", DeliveredRequestedListStatus.AFFECTED);
        query.setParameter("completeDeliveredRequestedListStatus", DeliveredRequestedListStatus.COMPLETE);
        query.setParameter("building", building);
        return query.getResultList();
    }

}
