package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieApplicationConfig;
import com.franciaflex.magalie.MagalieTechnicalException;
import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.google.common.base.Preconditions;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

public class DefaultMagalieServiceContext implements MagalieServiceContext {

    protected MagalieApplicationConfig magalieApplicationConfig;

    protected JpaMagaliePersistenceContext persistenceContext;

    protected MagalieUserNotificationContext userNotificationContext;

    @Override
    public MagalieApplicationConfig getMagalieApplicationConfig() {
        return magalieApplicationConfig;
    }

    public void setMagalieApplicationConfig(MagalieApplicationConfig magalieApplicationConfig) {
        this.magalieApplicationConfig = magalieApplicationConfig;
    }

    @Override
    public <E extends MagalieService> E newService(Class<E> serviceClass) {

        E service;

        try {

            Constructor<E> constructor = serviceClass.getConstructor();

            service = constructor.newInstance();

        } catch (NoSuchMethodException e) {

            throw new MagalieTechnicalException("all services must provide a non-argument constructor", e);

        } catch (InvocationTargetException e) {

            throw new MagalieTechnicalException("unable to instantiate magalie service", e);

        } catch (InstantiationException e) {

            throw new MagalieTechnicalException("unable to instantiate magalie service", e);

        } catch (IllegalAccessException e) {

            throw new MagalieTechnicalException("unable to instantiate magalie service", e);

        }

        service.setServiceContext(this);

        return service;

    }

    @Override
    public Date getNow() {
        Date now = new Date();
        return now;
    }

    @Override
    public JpaMagaliePersistenceContext getPersistenceContext() {
        return persistenceContext;
    }

    public void setPersistenceContext(JpaMagaliePersistenceContext persistenceContext) {
        this.persistenceContext = persistenceContext;
    }

    @Override
    public MagalieUserNotificationContext getUserNotificationContext() {
        Preconditions.checkState(
                userNotificationContext != null,
                "user notification context not provided");
        return userNotificationContext;
    }

    public void setUserNotificationContext(MagalieUserNotificationContext userNotificationContext) {
        this.userNotificationContext = userNotificationContext;
    }

}
