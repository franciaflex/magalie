package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.dao.LocationErrorJpaDao;
import com.franciaflex.magalie.persistence.dao.ReceivedPreparedArticleReceptionJpaDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.PreparedArticleReception;
import com.franciaflex.magalie.persistence.entity.ReceivedPreparedArticleReception;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.persistence.entity.Supplier;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.AbstractMagalieServiceTest;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.exception.PreparedArticleReceptionAlreadyStoredException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReceptionServiceTest extends AbstractMagalieServiceTest {

    private static final Log log = LogFactory.getLog(ReceptionServiceTest.class);

    protected ReceptionService service;

    protected MagalieUser magalieUser;

    protected Building building;

    protected Supplier supplier;

    protected Article article;

    @Before
    public void setUp() throws ParseException {

        loadFixtures("fixtures");

        service = newService(ReceptionService.class);

        serviceContext.setDate(new Date(1363948427576l));

        magalieUser = fixture("bruno");

        building = fixture("B1");

        supplier = fixture("supplier2");

        article = fixture("article6");

    }

    @Test
    public void testGetSuppliers() {

        SuppliersToReceive receivedSuppliers = service.getReceivedSuppliers(building);

        Assert.assertTrue(receivedSuppliers.isSomethingToReceive());

        ImmutableSetMultimap<String, Supplier> suppliersByGroup = receivedSuppliers.getSuppliersByGroup();

        Assert.assertEquals(1, suppliersByGroup.size());

        Assert.assertEquals(supplier, Iterables.get(suppliersByGroup.entries(), 0).getValue());

        Assert.assertTrue(receivedSuppliers.isArticlesWithoutSuppliers());

    }

    @Test
    public void testGetArticlesForSupplier() {

        Set<StoredArticle> receivedArticles =
                service.getReceivedArticles(building, supplier.getId());

        Assert.assertEquals(1, receivedArticles.size());

        Assert.assertEquals(article, Iterables.get(receivedArticles, 0).getArticle());

        receivedArticles = service.getReceivedArticles(building, null); // null = all suppliers

        Assert.assertEquals(2, receivedArticles.size());

    }

    @Test
    public void testGetReceptionTask() {

        Set<StoredArticle> receivedArticles =
                service.getReceivedArticles(building, supplier.getId());

        StoredArticle receivedArticle =
                Iterables.getOnlyElement(receivedArticles);

        ReceptionTask receptionTask =
                service.getReceptionTask(receivedArticle.getId());

        Location fixedLocation = Iterables.getOnlyElement(article.getFixedLocations());

        // fixed location
        Assert.assertEquals(fixedLocation, Iterables.get(receptionTask.getLocations(), 0).location);

        // locations of the same warehouse
        Assert.assertEquals(receivedArticle.getLocation().getWarehouse(), Iterables.get(receptionTask.getLocations(), 1).getWarehouse());
        Assert.assertEquals(receivedArticle.getLocation().getWarehouse(), Iterables.get(receptionTask.getLocations(), 2).getWarehouse());
        Assert.assertEquals(receivedArticle.getLocation().getWarehouse(), Iterables.get(receptionTask.getLocations(), 3).getWarehouse());

        for (ReceptionLocation location : receptionTask.getLocations()) {

            if (log.isDebugEnabled()) {
                log.debug("location " + location.getBarcode());
            }

            Assert.assertNotEquals(
                    "we should never suggest someone to store something in a reception location",
                    Locations.codeForReceptionLocations(),
                    location.getCode());

            Assert.assertNotEquals(
                    "we should never suggest someone to store something in a warehouse without locations",
                    Locations.codeForWarehouseWithoutLocations(),
                    location.getCode());

            Assert.assertFalse(
                    "we should never suggest someone to store something in a full location",
                    location.isFullLocation());

            Assert.assertEquals(
                    "we shoud only suggest locations in the same building",
                    building,
                    location.getWarehouse().getBuilding()
            );

        }

    }

    @Test
    public void testConfirmReception() {

        Set<StoredArticle> receivedArticles =
            service.getReceivedArticles(building, supplier.getId());

        StoredArticle storedArticle =
                Iterables.getOnlyElement(receivedArticles);

        Location fixedLocation = Iterables.getOnlyElement(article.getFixedLocations());

        ReceptionConfirmation receptionConfirmation = new ReceptionConfirmation();

        receptionConfirmation.setStoredArticleId(storedArticle.getId());

        receptionConfirmation.setLocationIdToStoredQuantities(
                ImmutableMap.of(fixedLocation.getId(), storedArticle.getQuantity()));

        service.confirmReception(magalieUser, receptionConfirmation, null);

        StorageMovementJpaDao storageMovementDao = serviceContext.getPersistenceContext().getStorageMovementDao();

        List<StorageMovement> storageMovements = storageMovementDao.findAll();

        Assert.assertEquals(1, storageMovements.size());

    }

    @Test
    public void testWeCannotReceiveTheSameItemMultipleTime() {

        testConfirmReception();

        Set<StoredArticle> receivedArticles =
                service.getReceivedArticles(building, supplier.getId());

        Assert.assertTrue(receivedArticles.isEmpty());

        SuppliersToReceive receivedSuppliers = service.getReceivedSuppliers(building);

        ImmutableSetMultimap<String, Supplier> suppliers = receivedSuppliers.getSuppliersByGroup();

        Assert.assertTrue(receivedSuppliers.isSomethingToReceive());

        Assert.assertTrue(suppliers.isEmpty());

    }

    @Test
    public void testReceivePreparedReception() {

        // we will try to receive a prepared article reception with following barcode
        String barcode = "PAR2";

        try {

            PreparedArticleReception preparedArticleReception =
                    service.getPreparedArticleReception(building, barcode);

            Assert.assertNotNull(preparedArticleReception);

        } catch (InvalidMagalieBarcodeException e) {
            if (log.isErrorEnabled()) {
                log.error("unexpected exception", e);
            }
            Assert.fail();
        } catch (PreparedArticleReceptionAlreadyStoredException e) {
            if (log.isErrorEnabled()) {
                log.error("unexpected exception", e);
            }
            Assert.fail();
        }

        ReceptionTask receptionTask = null;
        try {
            receptionTask = service.getReceptionTaskForPreparedArticleReception(building, barcode);
        } catch (PreparedArticleReceptionAlreadyStoredException e) {
            if (log.isErrorEnabled()) {
                log.error("unexpected exception", e);
            }
             Assert.fail();
        } catch (InvalidMagalieBarcodeException e) {
            if (log.isErrorEnabled()) {
                log.error("unexpected exception", e);
            }
            Assert.fail();
        }


        // checks that given reception task asks to store quantity given
        Assert.assertEquals(4., receptionTask.getQuantity(), DELTA);


        // now, try to confirm reception
        ReceptionConfirmation receptionConfirmation = new ReceptionConfirmation();

        receptionConfirmation.setStoredArticleId(receptionTask.getStoredArticle().getId());

        // just store the whole quantity in the first location given
        Map<String, Double> locationIdToStoredQuantities =
                ImmutableMap.of(
                        Iterables.get(receptionTask.getLocations(), 0).getId(),
                        receptionTask.getQuantity());

        receptionConfirmation.setLocationIdToStoredQuantities(locationIdToStoredQuantities);

        // confirm
        service.confirmReception(magalieUser, receptionConfirmation, barcode);

        // now check that confirmation has stored a relation between the prepared article reception
        // and the implied storage movements

        ReceivedPreparedArticleReceptionJpaDao receivedPreparedArticleReceptionDao =
                serviceContext.getPersistenceContext().getReceivedPreparedArticleReceptionDao();

        List<ReceivedPreparedArticleReception> allReceivedPreparedArticleReceptions =
                receivedPreparedArticleReceptionDao.findAll();

        Assert.assertEquals(1, allReceivedPreparedArticleReceptions.size());

        ReceivedPreparedArticleReception receivedPreparedArticleReception =
                Iterables.getOnlyElement(allReceivedPreparedArticleReceptions);

        Assert.assertEquals(
                "at confirmation we stored the total quantity in a single location, so one storage movement",
                locationIdToStoredQuantities.size(),
                receivedPreparedArticleReception.getStorageMovements().size());

        Assert.assertEquals(barcode, receivedPreparedArticleReception.getPreparedArticleReception().getBarcode());

    }

    /**
     * It's only a performance test to test with 5000+ locations.
     */
    @Test
    public void testPerformanceWithThousandsLocations() {

        EntityManager entityManager = getJpaEntityManagerRule().newEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        Warehouse u01 = fixture("U01");

        Warehouse u02 = fixture("U02");

        for (int i = 1 ; i <= 5000 ; i++) {

            Location location = new Location();

            String locationCode = String.valueOf(i);

            location.setCode(locationCode);

            location.setWarehouse(i % 2 == 0 ? u01 : u02);

            entityManager.persist(location);

        }

        transaction.commit();

        Set<StoredArticle> receivedArticles =
                service.getReceivedArticles(building, supplier.getId());

        StoredArticle receivedArticle =
                Iterables.getOnlyElement(receivedArticles);

        long time = System.currentTimeMillis();

        Set<ReceptionLocation> locations = service.getReceptionTask(receivedArticle.getId()).getLocations();

        if (log.isTraceEnabled()) {
            log.trace("getting a result of " + locations.size() +
                    " locations took " + (System.currentTimeMillis() - time));
        }

    }

    @Test
    public void testGetLocationToStoreSomethingReportErrorIfFullLocation() throws InvalidMagalieBarcodeException {

        Location fullLocation = fixture("locationFull1");

        service.getReceptionLocation(building, magalieUser, fullLocation.getBarcode(), article.getId());

        EntityManager entityManager = getJpaEntityManagerRule().newEntityManager();

        LocationErrorJpaDao locationErrorJpaDao = new LocationErrorJpaDao(entityManager);

        Assert.assertFalse(locationErrorJpaDao.findAll().isEmpty());

    }

    @Test
    public void testSuppliersPresentation() {

        Iterable<Supplier> suppliers = fixture("suppliers");

        SuppliersToReceive suppliersToReceive = new SuppliersToReceive(suppliers);

        ImmutableSetMultimap<String,Supplier> suppliersByGroup = suppliersToReceive.getSuppliersByGroup();

        for (Map.Entry<String, Collection<Supplier>> suppliersByGroupEntry : suppliersByGroup.asMap().entrySet()) {

            String supplierGroup = suppliersByGroupEntry.getKey();

            Collection<Supplier> suppliersInGroup = suppliersByGroupEntry.getValue();

            if (log.isDebugEnabled()) {
                log.debug(supplierGroup + " ↓\n  " + StringUtils.join(suppliersInGroup, "\n  "));
            }

        }

        // just check that there is no exception, user can check is order is OK
    }

    @Test
    public void testPriorities() {

        Set<StoredArticle> receivedArticles =
                service.getReceivedArticles(building, null);

        Assert.assertEquals("AAA111", Iterables.get(receivedArticles, 0).getReceptionPriority());
        Assert.assertEquals("BBB222", Iterables.get(receivedArticles, 1).getReceptionPriority());

    }
}
