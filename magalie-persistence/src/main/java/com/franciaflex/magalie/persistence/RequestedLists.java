package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.RequestedList;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.apache.commons.collections.comparators.BooleanComparator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Comparator;

public class RequestedLists {

    private static final Log log = LogFactory.getLog(RequestedLists.class);

    protected static class GivenRequestListFirst implements Comparator<RequestedList> {

        protected RequestedList requestedList;

        public GivenRequestListFirst(RequestedList requestedList) {
            this.requestedList = requestedList;
        }

        @Override
        public int compare(RequestedList x, RequestedList y) {
            return BooleanComparator.getTrueFirstComparator().compare(x.equals(requestedList), y.equals(requestedList));
        }
    }

    protected static class UrgentFirstComparator implements Comparator<RequestedList> {

        @Override
        public int compare(RequestedList x, RequestedList y) {
            int compare = BooleanComparator.getTrueFirstComparator().compare(x.isUrgent(), y.isUrgent());
            return compare;
        }

    }

    protected static class RequestDateFistComparator implements Comparator<RequestedList> {

        @Override
        public int compare(RequestedList x, RequestedList y) {
            int compare = x.getRequestDate().compareTo(y.getRequestDate());
            return compare;
        }

    }

    public static Comparator<RequestedList> urgentFirstComparator() {
        return new UrgentFirstComparator();
    }

    public static Comparator<RequestedList> requestDateFirstComparator() {
        return new RequestDateFistComparator();
    }

    public static Comparator<RequestedList> givenRequestListFirst(RequestedList requestedList) {
        return new GivenRequestListFirst(requestedList);
    }

    public static Comparator<RequestedList> comparator() {
        Ordering<RequestedList> comparator =
                Ordering.compound(
                        Lists.newArrayList(
                                urgentFirstComparator(),
                                requestDateFirstComparator(),
                                Ordering.natural().onResultOf(new GetId())
                        )
                );
        return comparator;
    }

    protected static class GetId implements Function<RequestedList, String> {

        @Override
        public String apply(RequestedList requestedList) {
            return requestedList.getId();
        }
    }
}
