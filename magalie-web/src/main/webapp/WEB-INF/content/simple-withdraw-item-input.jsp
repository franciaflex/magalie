<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <title>Prélèvement immédiat</title>
    <link rel="stylesheet" href="<s:url value='/css/magalie-ck3x-reduced.css' />"/>
    <script type="text/javascript">
        $(document).ready(function () {
            bindKey('Esc', function(){ redirectTo($('#cancelLink'));});
            bindKey('F5', function(){ $('#reportError').click(); });
            $('input[name="originLocationBarcode"]').focus();
        });
    </script>
</head>

<s:url action="prepare-withdraw-item!input" id="cancelUrl" />

<ul class="oneItemPerLine">
    <li>
        Réf. : <s:property value="simpleWithdrawItemTask.article.code" />
    </li>
    <li>
        Desc. : <s:property value="simpleWithdrawItemTask.article.description" />
    </li>
</ul>

<s:form>

    <ul>

        <s:iterator value="simpleWithdrawItemTask.storedArticles">

            <li>

                <s:property value="location.barcode" />
                Qté
                <s:property value="quantity" />
                <s:property value="article.unit" />

                <s:if test="batchCode == null">
                    (aucun lot)
                </s:if>
                <s:else>
                    (<s:property value="batchCode" />)
                </s:else>

            </li>

        </s:iterator>

    </ul>

    <s:textfield key="originLocationBarcode" label="Emplacement prélevé" inputAppendIcon="barcode" cssClass="input-medium" />

    <s:hidden name="destinationWarehouseId" value="%{destinationWarehouseId}" />

    <s:hidden name="articleBarcode" value="%{articleBarcode}" />

    <div class="btn-group">
        <s:a href="%{cancelUrl}" cssClass="btn btn-mini" id="cancelLink" >Quitter (Esc)</s:a>
        <s:submit action="simple-withdraw-item" value="Valider" cssClass="btn btn-mini btn-primary" />
        <s:submit action="simple-withdraw-item!reportError" value="Anomalie (F5)" cssClass="btn btn-mini btn-danger" id="reportError" />
    </div>

</s:form>
