package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.UnavailableArticle;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class UnavailableArticleJpaDao extends AbstractUnavailableArticleJpaDao {

    public UnavailableArticleJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public UnavailableArticle findByArticle(Building building, Article article) {
        TypedQuery<UnavailableArticle> query = createQuery("from UnavailableArticle ua where ua.article = :article and ua.building = :building");
        query.setParameter("article", article);
        query.setParameter("building", building);
        return findUniqueOrNull(query);
    }
}
