package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.MagalieUser;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class MagalieUserJpaDao extends AbstractMagalieUserJpaDao {

    public MagalieUserJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public MagalieUser findByLogin(String login) {
        TypedQuery<MagalieUser> query = createQuery("from MagalieUser mu where mu.login = :login");
        query.setParameter("login", login);
        return findUnique(query);
    }

    @Override
    public List<MagalieUser> findAll() {
        TypedQuery<MagalieUser> query = createQuery("from MagalieUser mu order by mu.login");
        return findAll(query);
    }

    @Override
    public List<MagalieUser> findAllByCompany(Company company) {
        TypedQuery<MagalieUser> query = createQuery("from MagalieUser mu where mu.company = :company order by mu.name");
        query.setParameter("company", company);
        return findAll(query);
    }
}
