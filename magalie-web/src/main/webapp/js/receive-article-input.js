/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

model.locationsIndex = 0;
model.allowSubmit = false;

model.stored = function() {
    var stored = 0;
    for (var i = 0; i < this.locations.length; i++) {
        stored += this.locations[i].stored || 0;
    }
    return stored;
};

model.isFulfilled = function() {
    var isFull = this.stored() === this.quantity;
    return isFull;
};

model.canTerminate = function() {
    var canTerminate = this.isFulfilled();
    return canTerminate;
};

model.getLocation = function(barcode) {
    var locations = this.locations;
    var location;
    for (var i = 0; i < locations.length; i++) {
        if (barcode == locations[i].barcode) {
            location = locations[i];
        }
    }
    if (location == null) {
        // try to load it form JSON request ?
        $.ajax({
            dataType: "json",
            url: locationJsonUrl,
            data: {
                locationBarcode: barcode,
                articleId: model.storedArticle.article.id
            },
            async: false,
            success: function(data) {
                        location = data.receptionLocation; // null si le code barre n'est pas valide
                        if (location != null) {
                             locations.push(location);
                         }
                     }
        });
    }
    if (location == null) {
        throw new ValidationError(barcode + " n'est pas le code barre d'un emplacement valide");
    }
    if (location.fullLocation) {
        throw new ValidationError("Emplt " + barcode + " en erreur - choisir un autre");
    }
    return location;
};

model.store = function(barcode, quantity) {
    if ( ! isFinite(quantity) || quantity < 0) {
        throw new ValidationError('Il faut entrer une quantité strictement supérieure à 0');
    }
    var location = this.getLocation(barcode);
    location.used = true;
    location.stored = quantity;
};

model.nextLocation = function() {
    this.locationsIndex += 1;
    if (this.locationsIndex >= this.locations.length) {
        this.locationsIndex = null;
    }
};

model.remainingQuantity = function() {
    var remainingQuantity = this.quantity - this.stored();
    return remainingQuantity;
}

model.getConfirmation = function() {
    var locationIdToStoredQuantities = {};
    $.each(model.locations, function(key, location) {
        if (location.used && location.stored > 0.) {
            locationIdToStoredQuantities[location.id] = location.stored;
        }
    });
    var confirmation = {
        storedArticleId: this.storedArticle.id,
        locationIdToStoredQuantities: locationIdToStoredQuantities
    };
    return confirmation;
}

model.newEmptyStorageMovementForNextLocation = function() {
    var quantityIsRemaining = model.remainingQuantity() > 0.;
    var allIsUsed = true;
    for (var i = 0; i < this.locations.length; i++) {
        if ( ! this.locations[i].used) {
            allIsUsed = false;
        }
    }
    var proposeAnotherLocation = quantityIsRemaining && allIsUsed;
    if (proposeAnotherLocation) {
        this.locationsIndex += 1;
    }
}

var view = {

    refreshSummary : function() {
        $('#stored').text(model.stored());
        if (model.stored() === model.quantity) {
            $('#stored').addClass('success');
        } else if (model.stored() > model.quantity) {
            $('#stored').addClass('warning');
        }
    },

    refreshLocations : function() {

        var locationsElement = $('#locations');

        // reset the view
        locationsElement.children().remove();

        // add each storage movement in the model
        $.each(model.locations, function(key, location) {
            var locationElement = processTemplate('locationTemplate', location);
            $(locationElement).attr('id', 'location-' + key);
            locationsElement.append(locationElement);
            if (location.used) {
                if (location.defect) {
                    $(locationElement).addClass('defect');
                } else {
                    $(locationElement).addClass('success');
                }
            } else if (location.id === model.locations[model.locationsIndex].id && ! model.canTerminate()) {
                $(locationElement).addClass('pending');
            } else {
                $(locationElement).hide();
            }
        });
        $("body").scrollTop($("#container").height());
    },

    refreshForm : function() {
        // reset field to prevent reuse of the value by mistake
        $('#receive-article_locationBarcode').val('');
        // set default value of quantity to what should be store on the current location
        if (model.locationsIndex != null) {
            $('#receive-article_quantity').val(model.remainingQuantity());
        }
    },

    refresh : function() {
        this.refreshSummary();
        this.refreshLocations();
        this.refreshForm();
        $('#receive-article_locationBarcode').focus();
    }

};

var controller = {

    onShowFirstLocation : function () {

        model.newEmptyStorageMovementForNextLocation();

    },

    next : function(full) {

        var barcode = $('#receive-article_locationBarcode').val();

        var quantity;
        if (full) {
            quantity = 0;
        } else {
            quantity = parseFloat($('#receive-article_quantity').val());
        }

        model.store(barcode, quantity);

        // if used location focused as current, highlight next location to go
        if (barcode == model.locations[model.locationsIndex].barcode) {
            model.nextLocation();
        }

        view.refresh();

    },

    onNext : function() {

        try {

            controller.next(false);

            model.newEmptyStorageMovementForNextLocation();

            view.refresh();

        } catch (ex) {

            if (ex instanceof ValidationError) {
                handleValidationError(ex);
            } else {
                throw ex;
            }
        }

    },

    onReportError : function() {

        try {

            controller.next(true);

            model.newEmptyStorageMovementForNextLocation();

            view.refresh();

        } catch (ex) {

            if (ex instanceof ValidationError) {
                handleValidationError(ex);
            } else {
                throw ex;
            }
        }

    },

    onConfirm : function() {

        try {

            var barcode = $('#receive-article_locationBarcode').val();

            if (barcode.length > 0) {

                controller.next(false);

            }

            var confirmation = model.getConfirmation();

            var storageMovementCount = Object.keys(confirmation.locationIdToStoredQuantities).length;

            if (storageMovementCount > 0) {

                var confirmMessage = 'Confirmer ' + storageMovementCount + ' mouvements ?';

                if (confirm(confirmMessage)) {

                    // push model as Json in a hidden field
                    $('#receive-article_confirmation').val(JSON.stringify(confirmation));

                    model.forceConfirm = true;

                    $('#receive-article').submit();

                } else {

                    model.newEmptyStorageMovementForNextLocation();

                    view.refresh();

                }

            }

        } catch (ex) {

            if (ex instanceof ValidationError) {
                handleValidationError(ex);
            } else {
                throw ex;
            }

        }

    },

    onCancel : function(e) {

        var confirmation = model.getConfirmation();

        var storageMovementCount = Object.keys(confirmation.locationIdToStoredQuantities).length;

        var confirmMessage = 'Annuler ' + storageMovementCount + ' mouvements ?';

        var redirect = storageMovementCount == 0
                    || confirm(confirmMessage);

        if (redirect) {

            redirectTo($('#cancelLink'));

        } else {

            if (e) {
                e.preventDefault();
            }

        }

    },

    onSubmit : function(e) {

        if (model.forceConfirm) {

        } else {

            e.preventDefault();

        }

    }

};

// bindings
$(document).ready(function() {

    $('#receive-article').submit(controller.onSubmit);
    $('#receive-article_full').click(controller.onReportError);
    $('#receive-article_next').click(controller.onNext);
    $('#confirm').click(controller.onConfirm);
    $('#cancelLink').click(controller.onCancel);

    bindKey('F1', controller.onConfirm);
    bindKey('F4', controller.onConfirm);
    bindKey('F5', controller.onReportError);
    bindKey('Esc', controller.onCancel);

    view.refresh();

});
