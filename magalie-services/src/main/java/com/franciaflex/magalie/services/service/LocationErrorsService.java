package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.dao.LocationErrorJpaDao;
import com.franciaflex.magalie.persistence.dao.StoredArticleJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.LocationError;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.List;

public class LocationErrorsService implements MagalieService {

    private static final Log log = LogFactory.getLog(LocationErrorsService.class);

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public void reportError(Location location, Article article, MagalieUser magalieUser) {

        Date reportDate = serviceContext.getNow();

        reportError(location, article, magalieUser, reportDate);

    }

    public void reportError(String storedArticleId, MagalieUser magalieUser) {

        Preconditions.checkArgument(storedArticleId != null);

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        StoredArticleJpaDao storedArticleDao = persistenceContext.getStoredArticleDao();

        StoredArticle storedArticle = storedArticleDao.findById(storedArticleId);

        reportError(storedArticle.getLocation(), storedArticle.getArticle(), magalieUser);

    }

    public void reportError(Location location, Article article, MagalieUser magalieUser, Date reportDate) {

        LocationErrorJpaDao locationErrorDao = serviceContext.getPersistenceContext().getLocationErrorDao();

        LocationError existingLocationError = locationErrorDao.findByLocation(location);

        if (existingLocationError == null) {

            LocationError newLocationError = new LocationError();

            newLocationError.setMagalieUser(magalieUser);

            newLocationError.setLocation(location);

            newLocationError.setArticle(article);

            newLocationError.setReportDate(reportDate);

            locationErrorDao.persist(newLocationError);

            serviceContext.getPersistenceContext().commit();

            if (log.isInfoEnabled()) {
                log.info("reported error on location: " + newLocationError);
            }

        } else {

            if (log.isInfoEnabled()) {
                log.info("error on location " + location + " already reported, nothing saved");
            }

        }
    }

    public List<LocationError> getAllLocationErrors() {

        LocationErrorJpaDao locationErrorDao = serviceContext.getPersistenceContext().getLocationErrorDao();

        List<LocationError> allLocationErrors = locationErrorDao.findAll();

        return allLocationErrors;

    }

    protected Predicate<Location> getLocationNotReportedInErrorPredicate(Article article) {

        LocationErrorJpaDao locationErrorDao = serviceContext.getPersistenceContext().getLocationErrorDao();

        List<Location> allLocationsInErrorForArticle =
                locationErrorDao.findAllLocationsInError(article);

        return Predicates.not(Predicates.in(allLocationsInErrorForArticle));

    }

    protected Iterable<StoredArticle> filterLocationInError(Iterable<StoredArticle> storedArticles) {

        List<StoredArticle> result = Lists.newLinkedList();

        LocationErrorJpaDao locationErrorDao =
                serviceContext.getPersistenceContext().getLocationErrorDao();

        for (StoredArticle storedArticle : storedArticles) {

            ImmutableMap properties =
                    ImmutableMap.of(
                            LocationError.PROPERTY_ARTICLE, storedArticle.getArticle(),
                            LocationError.PROPERTY_LOCATION, storedArticle.getLocation()
                    );

            LocationError locationError = locationErrorDao.findByProperties(properties);

            if (locationError == null) {

                if (log.isTraceEnabled()) {
                    log.trace("no location error reported for " + storedArticle);
                }

                result.add(storedArticle);

            } else {

                if (log.isDebugEnabled()) {
                    log.debug("removing stored article " + storedArticle + " due to location error " + locationError);
                }

            }

        }

        return result;

    }

}
