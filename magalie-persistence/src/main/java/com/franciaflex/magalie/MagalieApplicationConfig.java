package com.franciaflex.magalie;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MagalieApplicationConfig {

    private static final Log log = LogFactory.getLog(MagalieApplicationConfig.class);

    protected ApplicationConfig applicationConfig;

    protected ImmutableMap<String, String> oracleCompatibilityModeJpaParameters;

    public MagalieApplicationConfig() {
        this("magalie");
    }

    public MagalieApplicationConfig(String contextPath) {
        applicationConfig = new ApplicationConfig();
        applicationConfig.loadDefaultOptions(MagalieConfigOption.values());
        String configFileName = contextPath + ".properties";
        if (log.isDebugEnabled()) {
            log.debug("will read file config file name " + configFileName);
        }
        applicationConfig.setConfigFileName(configFileName);
        try {
            applicationConfig.parse();
        } catch (ArgumentsParserException e) {
            throw new MagalieTechnicalException(e);
        }
        if (log.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder();
            List<MagalieConfigOption> options = Lists.newArrayList(MagalieConfigOption.values());
            for (MagalieConfigOption option : options) {
                builder.append(String.format("\n%1$-40s = %2$s",
                        option.getKey(),
                        applicationConfig.getOption(option)));
            }
            log.info("MagaLiE configuration:" + builder.toString());
        }
    }

    public Map<String, String> getJpaParameters() {
        Map<String, String> jpaParameters = Maps.newHashMap();
        Properties hibernateProperties = applicationConfig.getOptionStartsWith("hibernate");
        jpaParameters.putAll((Map) hibernateProperties);
        Properties jpaProperties = applicationConfig.getOptionStartsWith("javax.persistence");
        jpaParameters.putAll((Map) jpaProperties);
        if (isOracleCompatibilityMode()) {
            if (log.isInfoEnabled()) {
                log.info("oracle compatibility mode enabled: overriding settings with " + oracleCompatibilityModeJpaParameters);
            }
            jpaParameters.putAll(getOracleCompatibilityModeJpaParameters());
        }


        if (log.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder();

            for (Map.Entry<String,String> entry: jpaParameters.entrySet()) {
                builder.append(String.format("\n%1$-40s = %2$s",
                        entry.getKey(),
                        entry.getValue()));
            }
            log.info("MagaLiE persistence configuration:" + builder.toString());
        }

        return jpaParameters;
    }

    protected ImmutableMap<String, String> getOracleCompatibilityModeJpaParameters() {
        if (oracleCompatibilityModeJpaParameters == null) {
            InputStream inputStream = null;
            try {
                inputStream = getClass().getResourceAsStream("/hibernate-oracle.properties");
                Properties oracleCompatibilityModeProperties = new Properties();
                oracleCompatibilityModeProperties.load(inputStream);
                oracleCompatibilityModeJpaParameters = Maps.fromProperties(oracleCompatibilityModeProperties);
                IOUtils.closeQuietly(inputStream);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("should never occur", e);
                }
                throw new MagalieTechnicalException(e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }
        return oracleCompatibilityModeJpaParameters;
    }

    public boolean isDevMode() {
        boolean isDevMode = applicationConfig.getOptionAsBoolean(MagalieConfigOption.DEV_MODE.key);
        return isDevMode;
    }

    public boolean isOracleCompatibilityMode() {
        boolean isOracleCompatibilityMode = applicationConfig.getOptionAsBoolean(MagalieConfigOption.ORACLE_COMPATIBILITY_MODE.key);
        return isOracleCompatibilityMode;
    }

}
