package com.franciaflex.magalie.persistence.entity;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Objects;

import javax.persistence.Entity;

@Entity
public class StoredArticle extends AbstractJpaStoredArticle {

    private static final long serialVersionUID = 1L;

    public StoredArticle() {}

    public StoredArticle(String id, double quantity, String receptionPriority, Article article, String batchCode, Location location) {
        this.id = id;
        this.quantity = quantity;
        this.receptionPriority = receptionPriority;
        this.article = article;
        this.location = location;
        this.batchCode = batchCode;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add(PROPERTY_QUANTITY, quantity + " " + getArticle().getUnit())
                .add(PROPERTY_ARTICLE, article)
                .add(PROPERTY_LOCATION, location)
                .toString();
    }
} //StoredArticle
