package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieTechnicalException;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.Supplier;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.exception.PreparedArticleReceptionAlreadyStoredException;
import com.franciaflex.magalie.services.service.ArticleStorageService;
import com.franciaflex.magalie.services.service.ReceptionConfirmation;
import com.franciaflex.magalie.services.service.ReceptionService;
import com.franciaflex.magalie.services.service.ReceptionTask;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;

import java.util.Map;

@Results({
    @Result(name= "REDIRECT_TO_ARTICLES", type="redirectAction", params = { "actionName", "prepare-article-reception", "supplierId", "%{supplierId}" }),
    @Result(name= "REDIRECT_TO_SUPPLIERS", type="redirectAction", params = { "actionName", "prepare-article-reception!input" }),
    @Result(name= "PREPARED_RECEPTIONS", type="redirectAction", params = { "actionName", "prepare-prepared-article-reception!input" })
})
public class ReceiveArticleAction extends MagalieActionSupport {

    private static final Log log = LogFactory.getLog(ReceiveArticleAction.class);

    protected MagalieSession session;

    protected ReceptionService service;

    protected ArticleStorageService articleStorageService;

    protected String storedArticleId;

    protected String preparedArticleReceptionBarcode;

    protected ReceptionTask receptionTask;

    protected ReceptionConfirmation confirmation;

    protected String supplierId;

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setService(ReceptionService service) {
        this.service = service;
    }

    public void setArticleStorageService(ArticleStorageService articleStorageService) {
        this.articleStorageService = articleStorageService;
    }

    public void setStoredArticleId(String storedArticleId) {
        this.storedArticleId = storedArticleId;
    }

    public void setPreparedArticleReceptionBarcode(String preparedArticleReceptionBarcode) {
        this.preparedArticleReceptionBarcode = Strings.emptyToNull(preparedArticleReceptionBarcode);
    }

    @Override
    public String input() {

        Building building = session.getBuilding();

        if (storedArticleId != null) {

            receptionTask = service.getReceptionTask(storedArticleId);

            // on affiche ce message que dans le cas des réceptions simples, dans le cas des prepared, c'est toujours true donc plus gênant comme notification qu'autre chose
            if (receptionTask.isUserMustChooseExtraLocation()) {

                addActionMessage("Pas de stock : emplacement libre proposé");

            }

        }

        if (preparedArticleReceptionBarcode != null) {

            try {

                receptionTask = service.getReceptionTaskForPreparedArticleReception(building, preparedArticleReceptionBarcode);

            } catch (PreparedArticleReceptionAlreadyStoredException e) {

                addMessage("Article déjà rangé");

                log.warn("should never occur", e);

                return "PREPARED_RECEPTIONS";

            } catch (InvalidMagalieBarcodeException e) {

                addMessage("Le code n'est pas valide");

                log.warn("should never occur", e);

                return "PREPARED_RECEPTIONS";

            }

        }

        Preconditions.checkNotNull(receptionTask);

        Supplier supplier = receptionTask.getStoredArticle().getArticle().getSupplier();

        if (supplier != null) {
            supplierId = supplier.getId();
        }

        return INPUT;

    }

    public String getSupplierId() {
        return supplierId;
    }

    public ReceptionTask getReceptionTask() {
        return receptionTask;
    }

    public String getModelAsJson() throws JSONException {

        String json = JSONUtil.serialize(getReceptionTask());

        return json;

    }

    public String getPreparedArticleReceptionBarcode() {
        return preparedArticleReceptionBarcode;
    }

    public void setConfirmation(String confirmationJson) {
        try {
            Map deserialize = (Map) JSONUtil.deserialize(confirmationJson);
            JSONPopulator jsonPopulator = new JSONPopulator();
            confirmation = new ReceptionConfirmation();
            jsonPopulator.populateObject(confirmation, deserialize);
        } catch (Throwable e) {
            if (log.isErrorEnabled()) {
                log.error("unable to parse confirmation, json =\n" + confirmationJson, e);
            }
            throw new MagalieTechnicalException(e);
        }
    }

    @Override
    public String execute() {

        MagalieUser magalieUser = session.getMagalieUser();

        Building building = session.getBuilding();

        service.confirmReception(magalieUser, confirmation, preparedArticleReceptionBarcode);

        // on redirige selon l'activité

        String result;

        if (Activity.PREPARED_RECEPTIONS.equals(session.getActivity())) {

            result = Activity.PREPARED_RECEPTIONS.name();

        } else {

            // dans ce cas on redirige vers la listes des articles, s'il reste des articles à réceptionner pour le même fournisseur
            // sinon, on renvoie vers la liste des fournisseurs

            Optional<Supplier> supplierOptional =
                    service.isSupplierHaveOtherArticlesToReceive(building, confirmation.getStoredArticleId());

            if (supplierOptional.isPresent()) {

                result = "REDIRECT_TO_ARTICLES";

                supplierId = supplierOptional.get().getId();

            } else {

                result = "REDIRECT_TO_SUPPLIERS";

            }

        }

        return result;

    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String cancel() {

        MagalieUser magalieUser = session.getMagalieUser();

        // pas besoin, on stocke pas de mouvements non confirmés
        // articleStorageService.cancelStorageMovements(magalieUser);

        String result;

        if (Activity.PREPARED_RECEPTIONS.equals(session.getActivity())) {

            result = Activity.PREPARED_RECEPTIONS.name();

        } else {

            result = "REDIRECT_TO_ARTICLES";

        }

        return result;

    }

}
