package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.*;
import com.franciaflex.magalie.services.AbstractMagalieServiceTest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class StorageTransferServiceTest extends AbstractMagalieServiceTest {

    private static final Log log = LogFactory.getLog(StorageTransferServiceTest.class);

    protected StorageTransferService service;

    protected MagalieUser magalieUser;

    protected Building building;

    @Before
    public void setUp() throws ParseException {

        loadFixtures("fixtures");

        service = newService(StorageTransferService.class);

        serviceContext.setDate(new Date(1363948427576l));

        magalieUser = fixture("bruno");

        building = fixture("B1");

    }

    @Test
    public void testGetStoredArticlesInLocationIgnoreArticlesReportedInError() {

        Location location = fixture("location3");

        List<StoredArticle> storedArticlesInLocation =
                service.getStoredArticlesInLocation(location);

        Assert.assertEquals(2, storedArticlesInLocation.size());

        LocationErrorsService locationErrorsService =
                newService(LocationErrorsService.class);

        locationErrorsService.reportError(storedArticlesInLocation.get(0).getId(), magalieUser);

        List<StoredArticle> storedArticlesInLocationAfterError =
                service.getStoredArticlesInLocation(location);

        Assert.assertEquals(
                storedArticlesInLocation.size() - 1,
                storedArticlesInLocationAfterError.size());

    }

    @Test
    public void testPerformancesWhenReturningLotOfStoredArticles() {
        EntityManager entityManager = getJpaEntityManagerRule().newEntityManager();

        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        Location location = fixture("location3");

        Article article = fixture("article1");

        for (int i = 1 ; i <= 250 ; i++) {

            StoredArticle storedArticle = new StoredArticle();

            storedArticle.setLocation(location);
            storedArticle.setArticle(article);
            storedArticle.setQuantity(i);
            storedArticle.setReceptionPriority(String.valueOf(i));
            storedArticle.setBatchCode(String.valueOf(i));


            entityManager.persist(storedArticle);

        }

        transaction.commit();

        long time = System.currentTimeMillis();

        List<StoredArticle> storedArticles = service.getStoredArticlesInLocation(location);

        if (log.isDebugEnabled()) {
            log.debug("getting a result of " + storedArticles.size() +
                    " storedArticles took " + (System.currentTimeMillis() - time) + " ms");
        }

    }

}
