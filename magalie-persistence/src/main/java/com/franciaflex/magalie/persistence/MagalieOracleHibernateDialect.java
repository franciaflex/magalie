package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.hibernate.dialect.Oracle10gDialect;

import java.sql.Types;

/**
 * In the actual database, we are unable to create some views with columns
 * typed as "double precision", so we map all double in the entities
 * to "float" in database.
 *
 * @link http://stackoverflow.com/questions/2524966/hibernate-found-float-expected-double-precision
 * @link http://docs.oracle.com/cd/B28359_01/server.111/b28285/sqlqr06.htm#CHDJJEEA show that
 *       SQL type DOUBLE PRECISION is actually implemented as FLOAT(126)
 */
public class MagalieOracleHibernateDialect extends Oracle10gDialect {

    public MagalieOracleHibernateDialect() {
        super();
        registerColumnType(Types.DOUBLE, "number");
    }

}
