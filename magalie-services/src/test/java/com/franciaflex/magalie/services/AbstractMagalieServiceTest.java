package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieApplicationConfig;
import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.services.service.FixturesService;
import org.junit.Rule;
import org.nuiton.jpa.junit.JpaEntityManagerRule;

import javax.persistence.EntityManager;
import java.util.Map;

public abstract class AbstractMagalieServiceTest {

    protected static final double DELTA = 0.0001;

    protected static MagalieApplicationConfig magalieApplicationConfig;

    protected JpaEntityManagerRule jpaEntityManagerRule;

    protected FakeMagalieServiceContext serviceContext;

    protected MagalieFixtures magalieFixtures;

    protected static MagalieApplicationConfig getMagalieApplicationConfig() {

        if (magalieApplicationConfig == null) {

            magalieApplicationConfig = new MagalieApplicationConfig();

        }

        return magalieApplicationConfig;

    }

    protected MagalieServiceContext getServiceContext() {

        if (serviceContext == null) {

            FakeMagalieServiceContext serviceContext = new FakeMagalieServiceContext();

            serviceContext.setMagalieApplicationConfig(getMagalieApplicationConfig());

            EntityManager entityManager = getJpaEntityManagerRule().newEntityManager();

            JpaMagaliePersistenceContext jpaMagaliePersistenceContext =
                    new JpaMagaliePersistenceContext(entityManager);

            serviceContext.setPersistenceContext(jpaMagaliePersistenceContext);

            this.serviceContext = serviceContext;

        }

        return serviceContext;

    }

    protected void loadFixtures(String fixturesSetName) {

        FixturesService fixturesService = getServiceContext().newService(FixturesService.class);

        magalieFixtures = fixturesService.loadFixtures(fixturesSetName);

    }

    protected <E> E fixture(String id) {

        return magalieFixtures.fixture(id);

    }

    protected <E extends MagalieService> E newService(Class<E> serviceClass) {

        return getServiceContext().newService(serviceClass);

    }

    @Rule
    public JpaEntityManagerRule getJpaEntityManagerRule() {

        if (jpaEntityManagerRule == null) {

            Map<String, String> jpaParameters = getMagalieApplicationConfig().getJpaParameters();

            jpaEntityManagerRule = new JpaEntityManagerRule("magaliePersistenceUnit", jpaParameters);

        }

        return jpaEntityManagerRule;

    }

}
