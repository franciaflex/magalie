<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<s:if test="requestedList">
    <script type="text/javascript">
         $(document).ready(function () {
             $("#requestedArticle").focus();
             bindKey('0', function(){ redirectTo($('#requestedArticle'));});
             bindKey('Esc', function(){ redirectTo($('#removeUserAffectationLink'));});
         });
    </script>

    <p>
        Vous êtes affecté à la liste <s:property value="requestedList.code" />
    </p>
    <s:url action="deliver-requested-article" id="deliverRequestedArticleUrl">
        <s:param name="listType" value="%{requestedList.listType}" />
    </s:url>
    <s:a href="%{deliverRequestedArticleUrl}" cssClass="btn btn-block" id="requestedArticle" >Demander un article à servir (0)</s:a>

    <s:url action="remove-user-affectation" id="removeUserAffectationUrl">
        <s:param name="result">REDIRECT_TO_LIST_TYPES</s:param>
    </s:url>
    <s:a href="%{removeUserAffectationUrl}" cssClass="btn btn-block" id="removeUserAffectationLink">Abandonner la liste (Esc)</s:a>
</s:if>
<s:else>
    <script type="text/javascript">
         $(document).ready(function () {
             bindKey('Esc', function(){ redirectTo($('#removeUserAffectationLink'));});
         });
    </script>

    <s:if test="listTypes.empty">
        Aucune liste à servir à traiter
    </s:if>
    <s:else>
        <s:iterator value="listTypes" var="listType" status="stat">
            <s:set var="deliverRequestedArticleId">prepareArticleReceptionId<s:property value="#stat.index" /></s:set>
            <s:url action="deliver-requested-article" id="deliverRequestedArticleUrl">
                <s:param name="listType" value="#listType" />
            </s:url>
            <s:if test="#stat.index < shortcuts.size()">
                <script type="text/javascript">
                    $(document).ready(function () {
                        var id = '<s:property value="deliverRequestedArticleId" />';
                        bindKey('<s:property value="shortcuts.get(#stat.index)" />', function(){ redirectTo($('#' + id));});
                    });
                </script>
                <s:a href="%{deliverRequestedArticleUrl}" cssClass="btn btn-block" id="%{deliverRequestedArticleId}">
                    Commencer à traiter une liste « <s:property value="#listType" /> »
                    (<s:property value="shortcuts.get(#stat.index)" />)
                </s:a>
            </s:if>
            <s:else>
                <s:a href="%{deliverRequestedArticleUrl}" cssClass="btn btn-block">
                    Commencer à traiter une liste « <s:property value="#listType" /> »
                </s:a>
            </s:else>
        </s:iterator>
    </s:else>

    <s:url action="remove-user-affectation" id="removeUserAffectationUrl">
        <s:param name="result">REDIRECT_TO_ACTIVITIES</s:param>
    </s:url>
    <s:a href="%{removeUserAffectationUrl}" cssClass="btn btn-block" id="removeUserAffectationLink">Changer d'activité (Esc)</s:a>
</s:else>
