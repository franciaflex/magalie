/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

// declaration des constantes de touche clavier

KEYS = {"F1": 112,
 "F2": 113,
 "F3" : 114,
 "F4" : 115,
 "F5" : 116,
 "tab" : 9,
 "del" : 8,
 "Esc" : 27,
 "0" :  48,
 "1" :  49,
 "2" :  50,
 "3" :  51,
 "4" :  52,
 "5" :  53,
 "6" :  54,
 "7" :  55,
 "8" :  56,
 "9" :  57,
 "." :  190,
 "a" :  65,
 "b" :  66,
 "c" :  67,
 "d" :  68,
 "e" :  69,
 "f" :  70,
 "g" :  71,
 "h" :  72,
 "i" :  73,
 "j" :  74,
 "k" :  75,
 "l" :  76,
 "m" :  77,
 "n" :  78,
 "o" :  79,
 "p" :  80,
 "q" :  81,
 "r" :  82,
 "s" :  83,
 "t" :  84,
 "u" :  85,
 "v" :  86,
 "w" :  87,
 "x" :  88,
 "y" :  89,
 "z" :  90 };

SHORTCUTS = ["1", "2", "3", "5", "6", "7", "8", "9", "O", "a", "b", "c", "d", "e", "f", "g", "h", "i",
 "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

function bindKey(key, handler) {
    $(document).keydown(function(event) {
        if (event.which == KEYS[key] && !($(document.activeElement).is(":input") && SHORTCUTS.indexOf(key) >= 0)) {
            event.preventDefault();
            handler();
        }
    })

}

function redirectTo(a)   {
    window.location = a.attr('href');
}

function processTemplate(templateId, model) {
    var processing = $('#' + templateId).clone().children();
    var elementsWithData = $(processing).find('*[data]');
    for (var i = 0; i < elementsWithData.length; i++) {
        var elementWithData = elementsWithData[i];
        var data = $(elementWithData).attr('data');
        // XXX brendan 27/03/13 eval, seriously ?
        var text = eval("model." + data);
        $(elementWithData).text(text).removeAttr('data');
    }
    return processing;
}

function ValidationError(message) {
    this.message = message;
}

ValidationError.prototype = new Error();
ValidationError.prototype.constructor = ValidationError;

function handleValidationError(validationError) {
    alert(validationError.message);
    console.debug(validationError);
}
