<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
   <script type="text/javascript">
        $(document).ready(function () {
            bindKey('Esc', function(){ redirectTo($('#prepareArticleReceptionLink'));});
        });
   </script>
</head>

<ul class="oneItemPerLine">
    <s:iterator value="receivedArticles"  status="stat" >
        <s:set var="receiveArticleId">prepareArticleReceptionId<s:property value="#stat.index" /></s:set>
        <s:url action="receive-article!input" id="receiveArticleUrl">
            <s:param name="storedArticleId" value="%{id}" />
        </s:url>
        <s:if test="#stat.index < shortcuts.size()">
            <script type="text/javascript">
                $(document).ready(function () {
                    var id = '<s:property value="receiveArticleId" />';
                    bindKey('<s:property value="shortcuts.get(#stat.index)" />', function(){ redirectTo($('#' + id));});
                });
            </script>
            <li>
                <s:a href="%{receiveArticleUrl}" id="%{receiveArticleId}">
                    (<s:property value="shortcuts.get(#stat.index)" />)
                    <s:property value="article.code" />
                    Qté : <s:property value="quantity" /> <s:property value="article.unit" />
                    <s:property value="article.description" />
                </s:a>
            </li>
        </s:if>
        <s:else>
            <li>
                <s:a href="%{receiveArticleUrl}">
                    <s:property value="article.code" />
                    Qté : <s:property value="quantity" /> <s:property value="article.unit" />
                    <s:property value="article.description" />
                </s:a>
            </li>
        </s:else>
    </s:iterator>
</ul>

<s:url action="prepare-article-reception!input" id="prepareArticleReceptionUrl"/>
<s:a href="%{prepareArticleReceptionUrl}" cssClass="btn" id="prepareArticleReceptionLink">Annuler (Esc)</s:a>
