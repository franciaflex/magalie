<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <title>Rapport MagaLiE</title>
</head>

<h1>Rapport MagaLiE</h1>

<p>
    <s:property value="report.reportDate" />
</p>

<section>
    <h2>Mouvements de stocks prévus (non confirmés)</h2>

    <table class="table">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Emplacement d'origine
                </th>
                <th>
                    Emplacement destination
                </th>
                <th>
                    Article
                </th>
                <th>
                    Quantité réelle
                </th>
                <th>
                    Utilisateur
                </th>
                <th>
                    Date
                </th>
            </tr>
        </thead>

        <tbody>
            <s:iterator value="report.notConfirmedStorageMovements">
                <tr>
                    <td>
                        <s:property value="id" />
                    </td>
                    <td>
                        <s:property value="originLocation.warehouse.code" />
                        -
                        <s:property value="originLocation.code" />
                        (<s:property value="originLocation.warehouse.building.code" />)
                    </td>
                    <td>
                        <s:if test="destinationLocation">
                            <s:property value="destinationLocation.warehouse.code" />
                            -
                            <s:property value="destinationLocation.code" />
                            (<s:property value="destinationLocation.warehouse.building.code" />)
                        </s:if>
                    </td>
                    <td>
                        <s:property value="article.code" />
                    </td>
                    <td>
                        <s:property value="actualQuantity" /> <s:property value="article.unit" />
                    </td>
                    <td>
                        <a href="#<s:property value="magalieUser.id" />"><s:property value="magalieUser.login" /></a>
                    </td>
                    <td>
                        <s:property value="movementDate" />
                    </td>
                </tr>
            </s:iterator>
        </tbody>
    </table>
</section>

<section>
    <h2>Mouvements de stocks confirmés</h2>

    <table class="table">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Emplacement d'origine
                </th>
                <th>
                    Emplacement destination
                </th>
                <th>
                    Article
                </th>
                <th>
                    Quantité réelle
                </th>
                <th>
                    Utilisateur
                </th>
                <th>
                    Date mouvement
                </th>
                <th>
                    Date confirmation
                </th>
            </tr>
        </thead>

        <tbody>
            <s:iterator value="report.confirmedStorageMovements">
                <tr>
                    <td>
                        <s:property value="id" />
                    </td>
                    <td>
                        <s:property value="originLocation.warehouse.code" />
                        -
                        <s:property value="originLocation.code" />
                        (<s:property value="originLocation.warehouse.building.code" />)
                    </td>
                    <td>
                        <s:if test="destinationLocation">
                            <s:property value="destinationLocation.warehouse.code" />
                            -
                            <s:property value="destinationLocation.code" />
                            (<s:property value="destinationLocation.warehouse.building.code" />)
                        </s:if>
                    </td>
                    <td>
                        <s:property value="article.code" />
                    </td>
                    <td>
                        <s:property value="actualQuantity" /> <s:property value="article.unit" />
                    </td>
                    <td>
                        <a href="#<s:property value="magalieUser.id" />"><s:property value="magalieUser.login" /></a>
                    </td>
                    <td>
                        <s:property value="movementDate" />
                    </td>
                    <td>
                        <s:property value="confirmDate" />
                    </td>
                </tr>
            </s:iterator>
        </tbody>
    </table>
</section>

<section>
    <h2>Emplacements en erreur</h2>

    <table class="table">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Magasin
                </th>
                <th>
                    Emplacement
                </th>
                <th>
                    Article
                </th>
                <th>
                    Rapporteur
                </th>
                <th>
                    Date
                </th>
            </tr>
        </thead>

        <tbody>
            <s:iterator value="report.allLocationErrors">
                <tr>
                    <td>
                        <s:property value="id" />
                    </td>
                    <td>
                        <s:property value="location.warehouse.code" />
                    </td>
                    <td>
                        <s:property value="location.code" />
                    </td>
                    <td>
                        <s:property value="article.description" />
                        (<s:property value="article.code" />)
                    </td>
                    <td>
                        <s:property value="magalieUser.name" />
                    </td>
                    <td>
                        <s:property value="reportDate" />
                    </td>
                </tr>
            </s:iterator>
        </tbody>

    </table>
</section>

<section>
    <h2>Traitement des listes</h2>

    <table class="table">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Liste
                </th>
                <th>
                    Statut
                </th>
                <th>
                    Affectation
                </th>
            </tr>
        </thead>

        <tbody>
            <s:iterator value="report.allDeliveredRequestedLists">
                <tr>
                    <td>
                        <s:property value="id" />
                    </td>
                    <td>
                        <s:property value="requestedList.code" />
                    </td>
                    <td>
                        <s:property value="status" />
                    </td>
                    <td>
                        <s:if test="affectedTo">
                            <a href="#<s:property value="affectedTo.id" />"><s:property value="affectedTo.login" /></a>
                        </s:if>
                    </td>
                </tr>
            </s:iterator>
        </tbody>

    </table>
</section>

<section>
    <h2>Articles sans stocks</h2>

    <table class="table">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    Bâtiment
                </th>
                <th>
                    Article
                </th>
            </tr>
        </thead>

        <tbody>
            <s:iterator value="report.allUnavailableArticles">
                <tr>
                    <td>
                        <s:property value="id" />
                    </td>
                    <td>
                        <s:property value="building.code" />
                    </td>
                    <td>
                        <s:property value="article.description" />
                        (<s:property value="article.code" />)
                    </td>
                </tr>
            </s:iterator>
        </tbody>

    </table>
</section>

<section>
    <h2>Utilisateurs connus</h2>

    <table class="table">
        <thead>
            <tr>
                <th>
                    Id
                </th>
                <th>
                    login
                </th>
                <th>
                    nom
                </th>
                <th>
                    niveau de permis
                </th>
                <th>
                    permis pontier
                </th>
            </tr>
        </thead>

        <tbody>
            <s:iterator value="report.allMagalieUsers">
                <tr>
                    <td>
                        <a name="<s:property value="id" />"><s:property value="id" /></a>
                    </td>
                    <td>
                        <s:property value="login" />
                    </td>
                    <td>
                        <s:property value="name" />
                    </td>
                    <td>
                        <s:property value="accreditationLevel" />
                    </td>
                    <td>
                        <s:property value="craneMan" />
                    </td>
                </tr>
            </s:iterator>
        </tbody>

    </table>
</section>

<s:if test="devMode">
    <p>
        <s:url action="restore-fixtures" id="restoreFixturesUrl"/>
        <s:a href="%{restoreFixturesUrl}">Restaurer le jeu de test de départ</s:a>
    </p>
</s:if>
