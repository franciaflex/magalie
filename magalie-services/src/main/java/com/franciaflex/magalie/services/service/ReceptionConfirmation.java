package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Map;

public class ReceptionConfirmation {

    protected String storedArticleId;

    protected Map<String, Double> locationIdToStoredQuantities;

    public String getStoredArticleId() {
        return storedArticleId;
    }

    public void setStoredArticleId(String storedArticleId) {
        this.storedArticleId = storedArticleId;
    }

    public Map<String, Double> getLocationIdToStoredQuantities() {
        return locationIdToStoredQuantities;
    }

    public void setLocationIdToStoredQuantities(Map<String, Double> locationIdToStoredQuantities) {
        this.locationIdToStoredQuantities = locationIdToStoredQuantities;
    }
}
