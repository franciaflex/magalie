package com.franciaflex.magalie.web;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.MagalieUserNotificationContext;
import com.google.common.base.Supplier;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * C'est une implémentation de {@link MagalieUserNotificationContext} qui stocke les messages
 * dans une Multimap indexée par les utilisateurs
 */
public class StrutsActionMessagesUserNotificationContext implements MagalieUserNotificationContext {

    protected SetMultimap<MagalieUser, String> usersNotifications;

    public StrutsActionMessagesUserNotificationContext() {
        Supplier<Set<String>> emptySetFactory = new Supplier<Set<String>>() {

            @Override
            public Set<String> get() {
                return Sets.newLinkedHashSet();
            }
        };
        Map<MagalieUser, Collection<String >> magalieUserToMessages = Maps.newHashMap();
        SetMultimap<MagalieUser, String> multimap = Multimaps.newSetMultimap(magalieUserToMessages, emptySetFactory);
        usersNotifications = Multimaps.synchronizedSetMultimap(multimap);
    }

    @Override
    public void notifyUnaffectedRequestedListDriverLicenseRequired(MagalieUser magalieUser, DeliveredRequestedList oldAffectation) {
        usersNotifications.put(magalieUser, "Vous n'êtes plus affecté à la liste " + oldAffectation.getRequestedList().getCode() + " : permis requis");
    }

    @Override
    public void notifyUnaffectedRequestedListForEverythingUnavailable(MagalieUser magalieUser, DeliveredRequestedList oldAffectation) {
        usersNotifications.put(magalieUser, "Vous n'êtes plus affecté à la liste " + oldAffectation.getRequestedList().getCode() + " : stock manquant");
    }

    @Override
    public void notifyUnaffectedRequestedListForCompletedRequestedList(MagalieUser magalieUser, DeliveredRequestedList oldAffectation) {
        usersNotifications.put(magalieUser, "Vous n'êtes plus affecté à la liste " + oldAffectation.getRequestedList().getCode() + " : liste terminée");
    }

    @Override
    public void notifyAffectedRequestedList(MagalieUser magalieUser, DeliveredRequestedList newAffectation) {
        usersNotifications.put(magalieUser, "Vous êtes désormais affecté à la liste " + newAffectation.getRequestedList().getCode());
    }

    @Override
    public void notifyInventoryBlockedArticle(MagalieUser magalieUser, Set<String> articles) {

        StringBuilder articlesString = new StringBuilder();
        for (String article:articles) {
            articlesString.append(article).append(" ,");
        }

        if( articlesString.length() > 0 ) {
            articlesString.setLength(articlesString.length() - 2);
        }

        usersNotifications.put(magalieUser, "Des articles sont bloqués par un inventaire en cours : " + articlesString.toString());
    }

    public SetMultimap<MagalieUser, String> getUsersNotifications() {
        return usersNotifications;
    }

    public Set<String> getMessages(MagalieUser magalieUser) {
        return usersNotifications.get(magalieUser);
    }

}
