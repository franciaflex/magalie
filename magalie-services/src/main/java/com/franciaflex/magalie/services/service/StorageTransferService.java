package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.dao.BuildingJpaDao;
import com.franciaflex.magalie.persistence.dao.LocationJpaDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementJpaDao;
import com.franciaflex.magalie.persistence.dao.StoredArticleDao;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.exception.InaccessibleLocationException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class StorageTransferService implements MagalieService {

    private static final Log log = LogFactory.getLog(StorageTransferService.class);

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public Location getLocation(Building building, MagalieUser magalieUser, String barcode)
            throws InvalidMagalieBarcodeException, InaccessibleLocationException {

        MagalieBarcodeService magalieBarcodeService =
                serviceContext.newService(MagalieBarcodeService.class);

        Location location = magalieBarcodeService.getLocation(barcode, building);

        InaccessibleLocationException.checkLocationIsAccessible(location, magalieUser);

        return location;

    }

    public Location getLocation(String buildingId, MagalieUser magalieUser, String barcode)
            throws InvalidMagalieBarcodeException, InaccessibleLocationException {

        Preconditions.checkNotNull(buildingId);

        BuildingJpaDao buildingDao = serviceContext.getPersistenceContext().getBuildingDao();

        Building building = buildingDao.findById(buildingId);

        return getLocation(building, magalieUser, barcode);

    }

    public Location getLocationById(String id) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        LocationJpaDao locationDao = persistenceContext.getLocationDao();

        Location location = locationDao.findById(id);

        return location;
    }

    public List<StoredArticle> getStoredArticlesInLocation(Location location) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        StoredArticleDao storedArticleDao = persistenceContext.getStoredArticleDao();

        Iterable<StoredArticle> storedArticles = storedArticleDao.findAllByLocation(location);

        ArticleStorageService articleStorageService = serviceContext.newService(ArticleStorageService.class);

        StorageMovementDao storageMovementDao = persistenceContext.getStorageMovementDao();

        List<StorageMovement> allImpactingStoredArticle = new LinkedList<StorageMovement>();

        for (StoredArticle storedArticle : storedArticles) {
            allImpactingStoredArticle.addAll(storageMovementDao.findAllImpactingStoredArticle(storedArticle));
        }

        storedArticles = articleStorageService.computeActualQuantities(storedArticles, allImpactingStoredArticle);

        LocationErrorsService locationErrorsService = serviceContext.newService(LocationErrorsService.class);

        storedArticles = locationErrorsService.filterLocationInError(storedArticles);

        return Lists.newArrayList(storedArticles);

    }

    public void confirmStorageTransfer(MagalieUser magalieUser, StoredArticle storedArticle, double quantity, Location destinationLocation) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        StorageMovementJpaDao storageMovementDao = persistenceContext.getStorageMovementDao();

        Date now = serviceContext.getNow();

        StorageMovement storageMovement = new StorageMovement();

        Location originLocation = storedArticle.getLocation();

        storageMovement.setOriginLocation(originLocation);

        storageMovement.setDestinationLocation(destinationLocation);

        storageMovement.setMagalieUser(magalieUser);

        storageMovement.setArticle(storedArticle.getArticle());

        storageMovement.setActualQuantity(quantity);

        storageMovement.setMovementDate(now);

        storageMovement.setConfirmDate(now);

        storageMovementDao.persist(storageMovement);

        persistenceContext.commit();

        if (log.isInfoEnabled()) {
            log.info("storage movement recorded " + storageMovement);
        }

    }

    public List<Building> getDestinationBuildings(Company company) {

        BuildingsService buildingsService =
                serviceContext.newService(BuildingsService.class);

        List<Building> buildings = buildingsService.getAllBuildingsByCompany(company);

        return buildings;

    }

}
