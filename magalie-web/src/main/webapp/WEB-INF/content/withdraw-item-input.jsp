<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <script>
        <s:url action="get-real-time-storage-movement-task-json" id="getRealTimeStorageMovementTaskJsonUrl" />
        var getRealTimeStorageMovementTaskJsonUrl = '<s:property value="%{getRealTimeStorageMovementTaskJsonUrl}" />';
        <s:url action="save-real-time-storage-movement-json" id="saveRealTimeStorageMovementJsonUrl" />
        var saveRealTimeStorageMovementJsonUrl = '<s:property value="%{saveRealTimeStorageMovementJsonUrl}" />';
        var model = <s:property value="modelAsJson" escapeHtml="false" />;
    </script>
    <script src="<s:url value='/js/withdraw-item-input.js' />"></script>
    <title>Prélèvement d'un article</title>
    <link rel="stylesheet" href="<s:url value='/css/magalie-ck3x-reduced.css' />"/>
</head>

<header>
    <ul class="oneItemPerLine">
        <s:if test="%{requestedListCode != null}">
            <li>
                Liste : <s:property value="requestedListCode" />
            </li>
        </s:if>
        <li>
            Réf. : <s:property value="article.code" />
        </li>
        <li>
            Desc. : <s:property value="article.description" />
        </li>
        <li>
            Prélevé :
            <span id="withdrawn">0</span>
            <s:if test="expectedQuantity">
                / <s:property value="expectedQuantity" />
            </s:if>
            <s:property value="article.unit" />
        </li>
    </ul>
</header>
<section id="storageMovements">
</section>
<s:form cssClass="form-horizontal">
    <s:textfield name="locationBarcode" label="Empl." inputAppendIcon="barcode" cssClass="input-small" />
    <s:textfield name="quantity" label="Qté" inputAppend="%{article.unit}" cssClass="input-mini" />
    <s:hidden name="confirmation" />

    <div style="width: 180px">
        <s:submit name="next" value="Suivant (⏎)" cssClass="btn btn-primary btn-mini" />
        <input type="button" id="confirm" value="Terminer (F4)" class="btn btn-success btn-mini" />
        <input type="button" id="reportError" value="Ano. (F5)" class="btn btn-danger btn-mini" />
        <s:url action="withdraw-item!cancel" id="cancelUrl" />
        <s:a href="%{cancelUrl}" cssClass="btn btn-mini" id="cancelLink" >Abandon (Esc)</s:a>
    </div>
</s:form>
<div id="storageMovementTemplate" class="template">
   <div>
       <span data="storedArticle.location.warehouse.code"></span>
       <span data="storedArticle.location.code"></span>
       <span data="withdrawn">0</span> / <span data="expectedQuantity"></span> / <span data="storedArticle.quantity"></span> <s:property value="article.unit" />
   </div>
</div>
