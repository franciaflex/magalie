package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class BookArticleRequest {

    protected MagalieUser magalieUser;

    protected Building building;

    protected Article article;

    protected double requestedQuantity;

    protected Location destinationLocation;

    protected boolean bestEffortPolicy;

    public BookArticleRequest(MagalieUser magalieUser, Building building, Article article, double requestedQuantity, Location destinationLocation, boolean bestEffortPolicy) {
        this.magalieUser = magalieUser;
        this.building = building;
        this.article = article;
        this.requestedQuantity = requestedQuantity;
        this.destinationLocation = destinationLocation;
        this.bestEffortPolicy = bestEffortPolicy;
    }

    public MagalieUser getMagalieUser() {
        return magalieUser;
    }

    public Building getBuilding() {
        return building;
    }

    public Article getArticle() {
        return article;
    }

    public double getRequestedQuantity() {
        return requestedQuantity;
    }

    public Location getDestinationLocation() {
        return destinationLocation;
    }

    public boolean isBestEffortPolicy() {
        return bestEffortPolicy;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this);
    }
}
