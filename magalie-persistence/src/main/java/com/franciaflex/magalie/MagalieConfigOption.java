package com.franciaflex.magalie;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.config.ConfigOptionDef;

/**
 * @author bleny
 */
public enum MagalieConfigOption implements ConfigOptionDef {

    ORACLE_COMPATIBILITY_MODE(
            "magalie.oracleCompatibilityMode",
            "Configure hibernate pour supporter une base Oracle",
            "false", Boolean.class),

    DEV_MODE(
            "magalie.devMode",
            "Mode développement, court-circuite l'envoi de mail",
            "true", Boolean.class),

    ;

    protected final String key;
    protected final String description;
    protected final Class<?> type;
    protected String defaultValue;

    private MagalieConfigOption(String key, String description,
                                   String defaultValue, Class<?> type) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return false;
    }

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean isTransient) {
        // Nothing to do
    }

    @Override
    public void setFinal(boolean isFinal) {
        // Nothing to do
    }

}
