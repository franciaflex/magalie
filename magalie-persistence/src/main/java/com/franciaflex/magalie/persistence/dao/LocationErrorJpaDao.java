package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.LocationError;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class LocationErrorJpaDao extends AbstractLocationErrorJpaDao {

    public LocationErrorJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Location> findAllLocationsInError(Article article) {
        Query query = entityManager.createQuery("select le.location from LocationError le where le.article = :article ");
        query.setParameter("article", article);
        List<Location> allLocationsInError = query.getResultList();
        return allLocationsInError;
    }

    @Override
    public List<LocationError> findAll() {
        Query query = entityManager.createQuery("from LocationError le order by le.reportDate");
        List<LocationError> all = query.getResultList();
        return all;
    }
}
