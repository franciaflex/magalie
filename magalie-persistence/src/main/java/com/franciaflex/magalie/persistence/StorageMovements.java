package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

public class StorageMovements {

    protected static class GetArticleFunction implements Function<StorageMovement, Article> {

        @Override
        public Article apply(StorageMovement storageMovement) {
            return storageMovement.getArticle();
        }
    }

    protected static class GetOriginFunction implements Function<StorageMovement, Location> {

        @Override
        public Location apply(StorageMovement storageMovement) {
            return storageMovement.getOriginLocation();
        }
    }

    protected static class GetDestinationFunction implements Function<StorageMovement, Location> {

        @Override
        public Location apply(StorageMovement storageMovement) {
            return storageMovement.getDestinationLocation();
        }
    }

    protected static class StorageMovementIsConfirmedPredicate implements Predicate<StorageMovement> {

        @Override
        public boolean apply(StorageMovement storageMovement) {
            return storageMovement.isConfirmed();
        }
    }

    public static Function<StorageMovement, Article> getArticleFunction() {
        return new GetArticleFunction();
    }

    public static Function<StorageMovement, Location> getOriginFunction() {
        return new GetOriginFunction();
    }

    public static Function<StorageMovement, Location> getDestinationFunction() {
        return new GetDestinationFunction();
    }

    public static Predicate<StorageMovement> storageMovementIsConfirmed() {
        return new StorageMovementIsConfirmedPredicate();
    }

    public static Predicate<StorageMovement> storageMovementIsNotConfirmed() {
        return Predicates.not(storageMovementIsConfirmed());
    }
}
