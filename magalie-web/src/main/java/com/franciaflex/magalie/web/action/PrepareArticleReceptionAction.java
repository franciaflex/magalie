package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.service.ReceptionService;
import com.franciaflex.magalie.services.service.SuppliersToReceive;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Strings;

import java.util.Set;

public class PrepareArticleReceptionAction extends MagalieActionSupport {

    protected String supplierId;

    protected ReceptionService service;

    protected MagalieSession session;

    protected Set<StoredArticle> receivedArticles;

    protected SuppliersToReceive suppliersToReceive;

    public void setSupplierId(String supplierId) {
        this.supplierId = Strings.emptyToNull(supplierId);
    }

    public void setService(ReceptionService service) {
        this.service = service;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    @Override
    public String input() {

        session.setActivity(Activity.RECEPTIONS);

        Building building = session.getBuilding();

        suppliersToReceive = service.getReceivedSuppliers(building);

        if ( ! suppliersToReceive.isSomethingToReceive()) {

            addActionMessage("Il n'y a aucun article en attente");

        }

        return INPUT;

    }

    @Override
    public String execute() {

        Building building = session.getBuilding();

        receivedArticles = service.getReceivedArticles(building, supplierId);

        if (receivedArticles.isEmpty()) {

            addActionMessage("Il n'y a aucun article en attente");

        }

        return SUCCESS;

    }

    public SuppliersToReceive getSuppliersToReceive() {
        return suppliersToReceive;
    }

    public Set<StoredArticle> getReceivedArticles() {
        return receivedArticles;
    }

}
