package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Suppliers;
import com.franciaflex.magalie.persistence.entity.Supplier;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;

import java.util.Comparator;

public class SuppliersToReceive {

    protected boolean articlesWithoutSuppliers;

    protected boolean somethingToReceive;

    protected ImmutableSetMultimap<String,Supplier> suppliersByGroup;

    public SuppliersToReceive(Iterable<Supplier> suppliers) {

        somethingToReceive = ! Iterables.isEmpty(suppliers);

        articlesWithoutSuppliers = Iterables.contains(suppliers, null);

        // on veut présenter les groupes dans l'ordre alphabétique
        Comparator<String> supplierGroupsOrder = Ordering.natural();

        // dans chaque groupe, on veut présenter les fournisseurs triés par nom
        Comparator<Supplier> suppliersOrdering = Suppliers.orderByNamesComparator();

        ImmutableListMultimap<String, Supplier> suppliersIndexedByGroups =
                Multimaps.index(
                        Iterables.filter(
                                suppliers,
                                Predicates.notNull()),
                        Suppliers.getSupplierGroup()
                );

        suppliersByGroup = ImmutableSetMultimap
                .<String, Supplier> builder()
                .orderKeysBy(supplierGroupsOrder)
                .orderValuesBy(suppliersOrdering)
                .putAll(suppliersIndexedByGroups)
                .build();

    }

    public boolean isSomethingToReceive() {
        return somethingToReceive;
    }

    public boolean isArticlesWithoutSuppliers() {
        return articlesWithoutSuppliers;
    }

    public ImmutableSetMultimap<String, Supplier> getSuppliersByGroup() {
        return suppliersByGroup;
    }

}
