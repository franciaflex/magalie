package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class WarehouseJpaDao extends AbstractWarehouseJpaDao {

    public WarehouseJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Warehouse> findAllWithoutLocations(Building building) {
        TypedQuery<Warehouse> query = entityManager.createQuery("from Warehouse w where w.building = :building and w in (select l.warehouse from Location l where l.code = :code)", getEntityClass());
        query.setParameter("building", building);
        query.setParameter("code", Locations.codeForWarehouseWithoutLocations());
        return query.getResultList();
    }
}
