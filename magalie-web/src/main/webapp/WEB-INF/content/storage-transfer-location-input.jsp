<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <title>Transfert de stock</title>
    <script type="text/javascript">
     $(document).ready(function () {
         $("#storage-transfer-location_originBarcode").focus();
         bindKey('Esc', function(){ redirectTo($('#chooseActivityLink'));});
     });
    </script>
</head>

<s:url action="choose-activity" id="chooseActivityUrl"/>

<s:form>
    <s:textfield key="originBarcode" label="Emplacement de prélévement" inputAppendIcon="barcode" cssClass="input-medium" />

    <div class="btn-group">
        <s:a href="%{chooseActivityUrl}" cssClass="btn" id="chooseActivityLink" >Changer d'activité (Esc)</s:a>
        <s:submit name="next" value="Suivant" cssClass="btn btn-primary" />
    </div>
</s:form>