package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.dao.UnavailableArticleJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.UnavailableArticle;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import com.franciaflex.magalie.services.AbstractMagalieServiceTest;
import com.franciaflex.magalie.services.exception.ArticleNotAvailableForKanbanException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class FulfilKanbanServiceTest extends AbstractMagalieServiceTest {

    private static final Log log = LogFactory.getLog(FulfilKanbanServiceTest.class);

    protected FulfilKanbanService service;

    protected Building building;

    protected MagalieUser magalieUser;

    protected Warehouse warehouse;

    @Before
    public void setUp() {

        loadFixtures("fixtures");

        warehouse = fixture("B01");

        building = fixture("B1");

        magalieUser = fixture("bruno");

        serviceContext.setDate(new Date(1374496598822l));

        service = newService(FulfilKanbanService.class);

    }

    @Test
    public void testGetDefinedQuantity() {

        Article article = fixture("article1");

        try {

            double definedQuantity = service.getDefinedQuantity(article, warehouse);

            Assert.assertEquals(50., definedQuantity, DELTA);

        } catch (ArticleNotAvailableForKanbanException e) {

            if (log.isErrorEnabled()) {
                log.error("unexpected exception", e);
            }

            Assert.fail();

        }

        article = fixture("article6");

        try {

            double definedQuantity = service.getDefinedQuantity(article, warehouse);

            if (log.isTraceEnabled()) {
                log.trace("defined quantity = " + definedQuantity);
            }

            Assert.fail("expected exception");

        } catch (ArticleNotAvailableForKanbanException e) {

            if (log.isTraceEnabled()) {
                log.trace("expected exception", e);
            }

            Assert.assertEquals(article, e.getArticle());

        }

    }

    @Test
    public void testIsArticleUnavailable() {

        Article articleWithNoStock = fixture("article4");

        RealTimeStorageMovementTask realTimeStorageMovementTask = service.getRealTimeStorageMovementTask(building, magalieUser, articleWithNoStock.getId());

        boolean articleUnavailable = realTimeStorageMovementTask.isArticleUnavailable();

        Assert.assertTrue(articleUnavailable);

        EntityManager entityManager = getJpaEntityManagerRule().newEntityManager();

        UnavailableArticleJpaDao unavailableArticleJpaDao =
                new UnavailableArticleJpaDao(entityManager);

        List<UnavailableArticle> allUnavailableArticles = unavailableArticleJpaDao.findAll();

        Assert.assertEquals(1, allUnavailableArticles.size());

        Assert.assertEquals(articleWithNoStock, allUnavailableArticles.get(0).getArticle());

    }

}
