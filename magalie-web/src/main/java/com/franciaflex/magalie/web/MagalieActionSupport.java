package com.franciaflex.magalie.web;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class MagalieActionSupport extends ActionSupport {

    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(MagalieActionSupport.class);

    public static final List<String> SHORTCUTS = Arrays.asList(new String[]{ "0", "1", "2", "3", "5", "6", "7", "8", "9",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z" });

    protected Set<String> messages;

    public List<String> getShortcuts () {
        return SHORTCUTS;
    }

    public boolean isWithMessages() {
        return super.hasActionErrors() || super.hasActionMessages() || CollectionUtils.isNotEmpty(messages);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this).toString();
    }

    public Set<String> getMessages() {
        ImmutableSet<String> messages = ImmutableSet.copyOf(this.messages);
        this.messages.clear();
        return messages;
    }

    public void setMessages(Set<String> messages) {
        this.messages = messages;
    }

    protected void addMessage(String message) {
        messages.add(message);
    }

}
