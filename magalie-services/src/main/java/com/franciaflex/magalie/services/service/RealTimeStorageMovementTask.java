package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class RealTimeStorageMovementTask {

    protected StoredArticle storedArticle;

    protected boolean articleUnavailable;

    protected boolean driverLicenseRequired;

    public StoredArticle getStoredArticle() {
        return storedArticle;
    }

    public void setStoredArticle(StoredArticle storedArticle) {
        this.storedArticle = storedArticle;
    }

    public boolean isArticleUnavailable() {
        if (articleUnavailable) {
            Preconditions.checkState( ! driverLicenseRequired);
        }
        return articleUnavailable;
    }

    public void setArticleUnavailable(boolean articleUnavailable) {
        this.articleUnavailable = articleUnavailable;
    }

    public void setDriverLicenseRequired(boolean driverLicenseRequired) {
        this.driverLicenseRequired = driverLicenseRequired;
    }

    public boolean isDriverLicenseRequired() {
        if (driverLicenseRequired) {
            Preconditions.checkState( ! articleUnavailable);
        }
        return driverLicenseRequired;
    }

    public boolean isSuccess() {
        return storedArticle != null;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
