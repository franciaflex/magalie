package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.service.BuildingsService;
import com.franciaflex.magalie.services.service.CompanyService;
import com.franciaflex.magalie.services.service.MagalieUsersService;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;

@Results({
    @Result(name="success", type="redirectAction", params = { "actionName", "choose-activity" })
})
public class LoginAction extends MagalieActionSupport {

    protected MagalieUsersService magalieUsersService;

    protected BuildingsService buildingsService;

    protected CompanyService companyService;

    protected List<MagalieUser> allMagalieUsers;

    protected List<Building> allBuildings;

    protected MagalieSession session;

    protected String companyId;

    protected String magalieUserId;

    protected String buildingId;

    public void setMagalieUsersService(MagalieUsersService magalieUsersService) {
        this.magalieUsersService = magalieUsersService;
    }

    public void setBuildingsService(BuildingsService buildingsService) {
        this.buildingsService = buildingsService;
    }

    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setMagalieUserId(String magalieUserId) {
        this.magalieUserId = magalieUserId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    @Override
    public String input() {

        Company company = companyService.getCompany(companyId);

        allMagalieUsers = magalieUsersService.getAllMagalieUsersByCompany(company);

        allBuildings = buildingsService.getAllBuildingsByCompany(company);

        return INPUT;

    }

    public String getMagalieUserId() {
        return magalieUserId;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public List<MagalieUser> getAllMagalieUsers() {
        return allMagalieUsers;
    }

    public List<Building> getAllBuildings() {
        return allBuildings;
    }

    @Override
    public String execute() {

        MagalieUser magalieUser = magalieUsersService.getMagalieUser(magalieUserId);

        Building building = buildingsService.getBuilding(buildingId);

        session.setMagalieUser(magalieUser);

        session.setBuilding(building);

        return SUCCESS;

    }
}
