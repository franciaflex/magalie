package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.exception.InaccessibleLocationException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.service.StorageTransferService;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;

/**
 * @author Bavencoff
 */

@Results({
        @Result(name="success", type="redirectAction", params = { "actionName", "storage-transfer!input", "originId", "${originId}" })
})
public class StorageTransferLocationAction extends MagalieActionSupport {

    private static final Log log = LogFactory.getLog(StorageTransferAction.class);

    protected MagalieSession session;

    protected StorageTransferService service;

    protected String originBarcode;

    protected Location origin;

    public void setService(StorageTransferService service) {
        this.service = service;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setOriginBarcode(String originBarcode) {
        this.originBarcode = originBarcode;
    }

    @Override
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("origin BarCode : " + originBarcode);
        }

        Building building = session.getBuilding();

        MagalieUser magalieUser = session.getMagalieUser();

        try {

            origin = service.getLocation(building, magalieUser, originBarcode);

        } catch (InvalidMagalieBarcodeException e) {

            addFieldError("originBarcode", "Le code barre n'est pas un code valide");

            return INPUT;

        } catch (InaccessibleLocationException e) {

            addFieldError("originBarcode", "Vous n'êtes pas autorisé à accéder à l'emplacement");

            return INPUT;

        }

        List<StoredArticle> storedArticles = service.getStoredArticlesInLocation(origin);

        if (storedArticles.isEmpty()) {

            addActionMessage("Aucun article dans cet emplacement");

            return INPUT;

        }

        return SUCCESS;

    }

    public String getOriginId() {
        return origin.getId();
    }
}
