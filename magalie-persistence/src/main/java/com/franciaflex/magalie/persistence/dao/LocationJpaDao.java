package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class LocationJpaDao extends AbstractLocationJpaDao {

    public LocationJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Location find(String code, Warehouse warehouse) {
        TypedQuery<Location> query = createQuery("from Location l where l.code = :code and l.warehouse = :warehouse");
        query.setParameter("code", code);
        query.setParameter("warehouse", warehouse);
        return findUnique(query);
    }

    @Override
    public List<Location> findAllWithoutReception(Warehouse warehouse, boolean limitExtraLocationsTo50) {
        String hql = " from Location l where";
        hql += " l.warehouse = :warehouse and "
            + " l.code != :codeForReceptionLocations and "
            + " l.code != :codeForWarehouseWithoutLocations and "
            + " l.fullLocation = false "
            + " order by l.warehouse.building.code, l.warehouse.code, l.code";
        TypedQuery <Location> query = createQuery(hql);
        query.setParameter("warehouse", warehouse);
        query.setParameter("codeForReceptionLocations", Locations.codeForReceptionLocations());
        query.setParameter("codeForWarehouseWithoutLocations", Locations.codeForWarehouseWithoutLocations());
        if (limitExtraLocationsTo50) {
            query.setMaxResults(50);
        }
        return findAll(query);
    }

    @Override
    public Location findByBarcode(String barcode, Building building) {
        TypedQuery<Location> query = createQuery("from Location l " +
                "where CONCAT(l.warehouse.code, l.code) = :barcode " +
                "and l.warehouse.building = :building");
        query.setParameter("barcode", barcode);
        query.setParameter("building", building);
        return findUniqueOrNull(query);
    }
}
