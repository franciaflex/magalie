package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.service.ArticleStorageService;
import com.franciaflex.magalie.services.service.RealTimeStorageMovementTask;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results({
        @Result(name="success", type="json")
})
public class GetRealTimeStorageMovementTaskJsonAction extends MagalieActionSupport {

    protected ArticleStorageService service;

    protected MagalieSession session;

    protected String articleId;

    protected RealTimeStorageMovementTask realTimeStorageMovementTask;

    public void setService(ArticleStorageService service) {
        this.service = service;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    @Override
    public String execute() {

        MagalieUser magalieUser = session.getMagalieUser();

        Building building = session.getBuilding();

        realTimeStorageMovementTask = service.getRealTimeStorageMovementTask(building, magalieUser, articleId);

        return SUCCESS;

    }

    public RealTimeStorageMovementTask getRealTimeStorageMovementTask() {
        return realTimeStorageMovementTask;
    }

}
