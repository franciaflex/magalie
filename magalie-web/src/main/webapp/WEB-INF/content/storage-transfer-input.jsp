<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <title>Transfert de stock</title>
    <script type="text/javascript">
        $(document).ready(function () {
            bindKey('Esc', function(){ redirectTo($('#cancelLink'));});
            bindKey('F5', function(){ redirectTo($('#reportErrorLink'));});
            <s:if test="storedArticle != null">
                $('#storage-transfer_destinationBarcode').focus();
            </s:if>
        });


    </script>
    <link rel="stylesheet" href="<s:url value='/css/magalie-ck3x-reduced.css' />"/>
</head>

<s:url action="storage-transfer-location!input" id="cancelUrl" />

<header>
    <dl class="dl-horizontal">
        <dt>Source</dt>
        <dd><s:property value="origin.warehouse.code + origin.code" /></dd>
        <s:if test="storedArticle != null">
            <dt>Article</dt>
            <dd><s:property value="storedArticle.article.description" /></dd>
            <dt>Réf</dt>
            <dd><s:property value="storedArticle.article.code" /></dd>
            <dt>Qté disponible</dt>
            <dd><s:property value="storedArticle.quantity + ' ' + storedArticle.article.unit" /></dd>
        </s:if>
    </dl>
</header>


<s:if test="storedArticle == null">
    <ul class="oneItemPerLine">
        <s:iterator value="storedArticles" status="stat">
            <li>
                <s:set var="elementStoredArticleId">storedArticleId<s:property value="#stat.index" /></s:set>
                <s:url action="storage-transfer!input" id="storageTransferNextStepUrl">
                    <s:param name="originId" value="%{origin.id}" />
                    <s:param name="storedArticleId" value="%{id}" />
                </s:url>
                <s:if test="#stat.index < shortcuts.size()">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var id = '<s:property value="elementStoredArticleId" />';
                            bindKey('<s:property value="shortcuts.get(#stat.index)" />', function(){ redirectTo($('#' + id));});
                        });
                    </script>
                   <s:a href="%{storageTransferNextStepUrl}" id="%{elementStoredArticleId}">
                        (<s:property value="shortcuts.get(#stat.index)" />)
                        <s:property value="article.code" />
                        Qté <s:property value="quantity" /> <s:property value="article.unit" />
                        <s:property value="article.description" />
                    </s:a>
                </s:if>
                <s:else>
                    <s:a href="%{storageTransferNextStepUrl}">
                        <s:property value="article.code" />
                        Qté <s:property value="quantity" /> <s:property value="article.unit" />
                        <s:property value="article.description" />
                    </s:a>
                </s:else>
            </li>
        </s:iterator>
    </ul>

    <s:a href="%{cancelUrl}" cssClass="btn" id="cancelLink">Quitter (Esc)</s:a>
</s:if>
<s:else>
    <s:form cssClass="form-horizontal">
        <s:hidden name="originId" value="%{origin.id}" />
        <s:hidden name="storedArticleId" value="%{storedArticle.id}" />

        <s:textfield name="quantity" value="%{quantity}" label="Qté" inputAppend="%{storedArticle.article.unit}" type="number" step="any" min="0" cssClass="input-mini"  />

        <s:select label="Bât. dest."
               key="destinationBuildingId"
               list="destinationBuildings"
               listKey="id"
               listValue="code"
               required="true"
               cssClass="input-small"
        />

        <s:textfield key="destinationBarcode" label="Empl. dest." inputAppendIcon="barcode" cssClass="input-small" />

        <div class="btn-group">

            <s:a href="%{cancelUrl}" cssClass="btn btn-small" id="cancelLink">Quitter (Esc)</s:a>

            <s:url action="storage-transfer!reportError" id="reportErrorUrl">
                <s:param name="storedArticleId" value="%{storedArticle.id}" />
            </s:url>
            <s:a href="%{reportErrorUrl}" cssClass="btn btn-danger btn-small" id="reportErrorLink">Anomalie (F5)</s:a>

            <s:submit value="Suivant" cssClass="btn btn-primary btn-small" />

        </div>
    </s:form>
</s:else>
