package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.services.service.ArticleStorageService;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Preconditions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results({
        @Result(name="success", type="json")
})
public class SaveRealTimeStorageMovementJsonAction extends MagalieActionSupport {

    private static final Log log = LogFactory.getLog(SaveRealTimeStorageMovementJsonAction.class);

    protected ArticleStorageService service;

    protected MagalieSession session;

    protected String articleId;

    protected String originLocationId;

    protected String destinationLocationId;

    protected double quantity;

    protected String storageMovementId;

    public void setService(ArticleStorageService service) {
        this.service = service;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public void setDestinationLocationId(String destinationLocationId) {
        this.destinationLocationId = destinationLocationId;
    }

    public void setOriginLocationId(String originLocationId) {
        this.originLocationId = originLocationId;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String execute() throws Exception {

        try {

            Preconditions.checkNotNull(articleId);

            Preconditions.checkNotNull(originLocationId);

            Preconditions.checkNotNull(destinationLocationId);

            MagalieUser magalieUser = session.getMagalieUser();

            storageMovementId = service.saveStorageMovement(magalieUser, articleId, originLocationId, destinationLocationId, quantity).getId();

            return SUCCESS;

        } catch (Exception e) {

            if (log.isErrorEnabled()) {
                log.error("", e);
            }

            throw e;

        }

    }

    public String getStorageMovementId() {
        return storageMovementId;
    }

}
