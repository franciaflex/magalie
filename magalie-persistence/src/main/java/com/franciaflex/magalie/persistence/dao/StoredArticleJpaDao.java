package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.StoredArticle;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class StoredArticleJpaDao extends AbstractStoredArticleJpaDao {

    public StoredArticleJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<StoredArticle> findAllForArticleInBuilding(Article article, Building building) {
        TypedQuery<StoredArticle> query = createQuery("from StoredArticle sa where sa.article = :article and sa.location.warehouse.withdrawAllowed = true and sa.location.warehouse.building = :building");
        query.setParameter("article", article);
        query.setParameter("building", building);
        query.setHint("org.hibernate.fetchSize", 500);
        List<StoredArticle> resultList = findAll(query);
        for (StoredArticle storedArticle : resultList) {
            entityManager.detach(storedArticle);
        }
        return resultList;
    }

    @Override
    public List<StoredArticle> findAllReceivedForAllSupplier(Building building) {
        TypedQuery<StoredArticle> query = createQuery("from StoredArticle sa where sa.location.warehouse.building = :building and sa.location.code = :code");
        query.setParameter("building", building);
        query.setParameter("code", Locations.codeForReceptionLocations());
        query.setHint("org.hibernate.fetchSize", 500);
        List<StoredArticle> resultList = findAll(query);
        for (StoredArticle storedArticle : resultList) {
            entityManager.detach(storedArticle);
        }
        return resultList;
    }

    @Override
    public List<StoredArticle> findAllReceivedForSupplier(Building building, String supplierId) {
        TypedQuery<StoredArticle> query = createQuery("from StoredArticle sa where sa.location.warehouse.building = :building and sa.location.code = :code and sa.article.supplier.id = :supplierId");
        query.setParameter("building", building);
        query.setParameter("supplierId", supplierId);
        query.setParameter("code", Locations.codeForReceptionLocations());
        query.setHint("org.hibernate.fetchSize", 500);
        List<StoredArticle> resultList = findAll(query);
        for (StoredArticle storedArticle : resultList) {
            entityManager.detach(storedArticle);
        }
        return resultList;
    }

    @Override
    public List<StoredArticle> findAllByLocation(Location location) {
        TypedQuery<StoredArticle> query = createQuery("from StoredArticle sa where sa.location = :location");
        query.setParameter("location", location);
        query.setHint("org.hibernate.fetchSize", 500);
        List<StoredArticle> resultList = findAll(query);
        for (StoredArticle storedArticle : resultList) {
            entityManager.detach(storedArticle);
        }
        return resultList;
    }

    @Override
    public StoredArticle findDetachedById(String storedArticleId) {
        StoredArticle storedArticle = findById(storedArticleId);
        entityManager.detach(storedArticle);
        return storedArticle;
    }

    @Override
    public StoredArticle findInReception(Building building, Article article) {
        TypedQuery<StoredArticle> query = createQuery("from StoredArticle sa where sa.article = :article and sa.location.code = :codeForReceptionLocations and sa.location.warehouse.building = :building");
        query.setParameter("building", building);
        query.setParameter("article", article);
        query.setParameter("codeForReceptionLocations", Locations.codeForReceptionLocations());
        query.setHint("org.hibernate.fetchSize", 500);
        return findUnique(query);
    }

}
