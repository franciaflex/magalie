package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.MagalieTechnicalException;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.services.StorageMovementConfirmation;
import com.franciaflex.magalie.services.exception.MagalieException;
import com.franciaflex.magalie.services.exception.MissingRowInViewsException;
import com.franciaflex.magalie.services.service.ArticleStorageService;
import com.franciaflex.magalie.services.service.IllegalConfirmationException;
import com.franciaflex.magalie.services.service.RequestedArticleService;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.xwork2.Preparable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;

import java.util.Collections;
import java.util.Map;

@Results({
    @Result(name="KANBANS", type="redirectAction", params = { "actionName", "fulfil-kanban!input" }),
    @Result(name="REQUESTED_LISTS", type="redirectAction", params = { "actionName", "deliver-requested-article!next", "listType", "%{listType}" }),
    @Result(name="REQUESTED_LISTS_CANCEL", type="redirectAction", params = { "actionName", "deliver-requested-article!input" })
})
public class WithdrawItemAction extends MagalieActionSupport implements Preparable {

    private static final Log log = LogFactory.getLog(WithdrawItemAction.class);

    protected ArticleStorageService articleStorageService;

    protected RequestedArticleService requestedArticleService;

    protected StorageMovementConfirmation confirmation;

    protected MagalieSession session;

    protected String articleId;

    protected String destinationLocationId;

    protected Double expectedQuantity;

    protected String requestedArticleId;

    protected Article article;

    protected String requestedListCode;

    protected String listType;

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public void setDestinationLocationId(String destinationLocationId) {
        this.destinationLocationId = destinationLocationId;
    }

    public void setExpectedQuantity(Double expectedQuantity) {
        this.expectedQuantity = expectedQuantity;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setArticleStorageService(ArticleStorageService articleStorageService) {
        this.articleStorageService = articleStorageService;
    }

    public void setRequestedArticleService(RequestedArticleService requestedArticleService) {
        this.requestedArticleService = requestedArticleService;
    }

    public void setRequestedArticleId(String requestedArticleId) {
        this.requestedArticleId = requestedArticleId;
    }

    @Override
    public void prepare() {

    }

    @Override
    public String input() {

        if (requestedArticleId != null) {

            RequestedArticle requestedArticle =
                    requestedArticleService.getRequestedArticle(requestedArticleId);

            expectedQuantity = requestedArticle.getQuantity();

            destinationLocationId = requestedArticle.getDestinationLocation().getId();

            article = requestedArticle.getArticle();

            articleId = article.getId();

            requestedListCode = requestedArticle.getRequestedList().getCode();

        }

        Preconditions.checkNotNull(articleId);

        Preconditions.checkNotNull(destinationLocationId);

        article = articleStorageService.getArticle(articleId);

        return INPUT;

    }

    public String getRequestedArticleId() {
        return requestedArticleId;
    }

    public Article getArticle() {
        return article;
    }

    public Double getExpectedQuantity() {
        return expectedQuantity;
    }

    public String getRequestedListCode() {
        return requestedListCode;
    }

    public String getModelAsJson() throws JSONException {

        Map<String,Object> model = Maps.newHashMap();

        model.put("articleId", articleId);
        model.put("destinationLocationId", destinationLocationId);
        model.put("expectedQuantity", expectedQuantity);
        model.put("requestedArticleId", requestedArticleId);

        String json = JSONUtil.serialize(model);

        return json;

    }

    public void setConfirmation(String confirmationJson) {

        if (log.isDebugEnabled()) {
            log.debug("should deserialize" + confirmationJson);
        }

        try {
            Map deserialize = (Map) JSONUtil.deserialize(confirmationJson);
            JSONPopulator jsonPopulator = new JSONPopulator();
            confirmation = new StorageMovementConfirmation();
            jsonPopulator.populateObject(confirmation, deserialize);
        } catch (Throwable e) {
            if (log.isErrorEnabled()) {
                log.error("unable to parse confirmation, json =\n" + confirmationJson, e);
            }
            throw new MagalieTechnicalException(e);
        }
    }

    @Override
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("confirmation = " + confirmation);
        }

        Preconditions.checkNotNull(confirmation, "absence de données de confirmation");

        if (confirmation.getStorageMovementIds().contains(null)) {

            if (log.isErrorEnabled()) {
                log.error("confirmation " + confirmation + " is malformed. session = " + session);
            }

            throw new MagalieTechnicalException("confirmation " + confirmation + " is malformed");

        }

        if (confirmation.getLocationInErrorIds() == null) {
            if (log.isWarnEnabled()) {
                log.warn("Pas de locationInErrors, initialisation à vide");
            }
            confirmation.setLocationInErrorIds(Collections.<String>emptySet());
        }

        MagalieUser magalieUser = session.getMagalieUser();

        try {
            articleStorageService.confirmStorageMovement(confirmation, magalieUser);
        } catch (IllegalConfirmationException eee) {
            log.warn("Confirmation déjà confirmée : " + confirmation);
        } catch (MissingRowInViewsException eee) {
            log.warn("Erreur de confirmation", eee);
        }

        if (confirmation.getRequestedArticleId() != null) {

            RequestedArticle requestedArticle =
                    requestedArticleService.getRequestedArticle(confirmation.getRequestedArticleId());

            listType = requestedArticle.getRequestedList().getListType();

        }

        String resultName = session.getActivity().name();

        return resultName;
    }

    public String getListType() {
        return listType;
    }

    public String cancel() {

        MagalieUser magalieUser = session.getMagalieUser();

        articleStorageService.cancelStorageMovements(magalieUser);

        String resultName = session.getActivity().name();

        if (Activity.REQUESTED_LISTS.equals(session.getActivity())) {

            resultName = "REQUESTED_LISTS_CANCEL";

        }

        return resultName;

    }

}
