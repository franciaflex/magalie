package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.services.service.RequestedArticleService;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Preconditions;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results({
    @Result(name="REDIRECT_TO_LIST_TYPES", type="redirectAction", params = { "actionName", "deliver-requested-article!input" }),
    @Result(name="REDIRECT_TO_ACTIVITIES", type="redirectAction", params = { "actionName", "choose-activity" })
})
public class RemoveUserAffectationAction extends MagalieActionSupport {

    protected MagalieSession session;

    protected RequestedArticleService service;

    protected String result;

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setService(RequestedArticleService service) {
        this.service = service;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String execute() {

        Preconditions.checkState(result != null);

        MagalieUser magalieUser = session.getMagalieUser();

        service.removeAffectation(magalieUser);

        return result;

    }

}
