package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.StorageMovements;
import com.franciaflex.magalie.persistence.StoredArticles;
import com.franciaflex.magalie.persistence.dao.ArticleJpaDao;
import com.franciaflex.magalie.persistence.dao.LocationJpaDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementDao;
import com.franciaflex.magalie.persistence.dao.StorageMovementJpaDao;
import com.franciaflex.magalie.persistence.dao.StoredArticleDao;
import com.franciaflex.magalie.persistence.dao.UnavailableArticleJpaDao;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.persistence.entity.UnavailableArticle;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.StorageMovementConfirmation;
import com.franciaflex.magalie.services.exception.MagalieException;
import com.franciaflex.magalie.services.exception.MissingRowInViewsException;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ArticleStorageService implements MagalieService {

    private static final Log log = LogFactory.getLog(ArticleStorageService.class);

    protected MagalieServiceContext serviceContext;

    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    /** Add or remove a line in table {@link UnavailableArticle} */
    protected void updateArticleAvailability(Building building, Article article, boolean articleIsAvailable) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        UnavailableArticleJpaDao dao = persistenceContext.getUnavailableArticleDao();

        UnavailableArticle unavailableArticle = dao.findByArticle(building, article);

        if (articleIsAvailable) {

            if (unavailableArticle != null) {

                if (log.isInfoEnabled()) {
                    log.info("article " + article + " is no longer considered as unavailable");
                }

                dao.remove(unavailableArticle);

            }

        } else {

            if (log.isInfoEnabled()) {
                log.info("reporting article " + article + " as unavailable");
            }

            if (unavailableArticle == null) {

                UnavailableArticle newUnavailableArticle = new UnavailableArticle();

                newUnavailableArticle.setArticle(article);

                newUnavailableArticle.setBuilding(building);

                newUnavailableArticle.setReportDate(serviceContext.getNow());

                dao.persist(newUnavailableArticle);

            }

        }

        persistenceContext.commit();

    }

    protected Set<StoredArticle> sortStoredArticlesByPriority(Iterable<StoredArticle> storedArticles) {

        Ordering<StoredArticle> orderingByPriority =
                Ordering.compound(
                        Lists.newArrayList(
                                StoredArticles.fixedLocationForArticleComparator(),
                                StoredArticles.locationWithLowestQuantityFirstComparator(),
                                StoredArticles.articleStoredInLocationsRequiringDriverLicenseFirstComparator()
                        )
                );

        Set<StoredArticle> storedArticlesByPriority =
                Sets.newTreeSet(orderingByPriority);

        Iterables.addAll(storedArticlesByPriority, storedArticles);

        return storedArticlesByPriority;

    }

    /** Get a snapshot of the actual current state of the stock. */
    protected Iterable<StoredArticle> getStoredArticles(Building building, Article article) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        StoredArticleDao storedArticleDao = persistenceContext.getStoredArticleDao();

        // first get all stored articles in the building at known quantities
        Iterable<StoredArticle> storedArticles = storedArticleDao.findAllForArticleInBuilding(article, building);

        StorageMovementDao storageMovementDao = persistenceContext.getStorageMovementDao();

        List<StorageMovement> storageMovementsForArticle = storageMovementDao.findAllByArticle(article);

        // consider storage movements and compute actually available quantities
        storedArticles = computeActualQuantities(storedArticles, storageMovementsForArticle);

        boolean articleAvailable = ! Iterables.isEmpty(storedArticles);

        updateArticleAvailability(building, article, articleAvailable);

        // exclude storageMovements on a location reported in error
        LocationErrorsService locationErrorsService =
                serviceContext.newService(LocationErrorsService.class);

        storedArticles = locationErrorsService.filterLocationInError(storedArticles);

        return storedArticles;

    }

    /**
     * This method take some storedArticles and update the quantities according
     * to given storage movements (adding or removing quantities depending if it's
     * a movement from the location or to the location)
     */
    protected Iterable<StoredArticle> computeActualQuantities(Iterable<StoredArticle> storedArticles, Iterable<StorageMovement> storageMovements) {

        Multimap<Article, StoredArticle> storedArticlesByArticles =
                Multimaps.index(storedArticles, StoredArticles.getArticleFunction());

        Multimap<Article, StorageMovement> allStorageMovementsInReceptionByArticle =
                Multimaps.index(storageMovements, StorageMovements.getArticleFunction());

        for (Map.Entry<Article, StoredArticle> articleStored : storedArticlesByArticles.entries()) {

            Article article = articleStored.getKey();

            StoredArticle storedArticle = articleStored.getValue();

            Location location = storedArticle.getLocation();

            for (StorageMovement storageMovement : allStorageMovementsInReceptionByArticle.get(article)) {

                if (storageMovement.getOriginLocation().equals(location)) {

                    double actualQuantity = storedArticle.getQuantity() - storageMovement.getActualQuantity();

                    storedArticle.setQuantity(actualQuantity);

                }

                if (storageMovement.getDestinationLocation().equals(location)) {

                    double actualQuantity = storedArticle.getQuantity() + storageMovement.getActualQuantity();

                    storedArticle.setQuantity(actualQuantity);

                }

            }

        }

        // Warning :
        // this function don't add a new StoredArticle
        // if exist a storage movement with new article in location

        Iterable<StoredArticle> result = Iterables.filter(storedArticles, StoredArticles.notEmpty());

        return result;

    }

    public void confirmStorageMovement(StorageMovementConfirmation confirmation, MagalieUser magalieUser) throws MissingRowInViewsException, IllegalConfirmationException {

        Preconditions.checkNotNull(confirmation);

        if (log.isDebugEnabled()) {
            log.debug(magalieUser + " will make confirmation " + confirmation);
        }

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        ArticleJpaDao articleDao = persistenceContext.getArticleDao();

        Article article = articleDao.findById(confirmation.getArticleId());

        Date confirmDate = serviceContext.getNow();

        StorageMovementJpaDao storageMovementDao = persistenceContext.getStorageMovementDao();

        Set<StorageMovement> confirmedStorageMovements = Sets.newHashSet();

        List<StorageMovement> storageMovements = storageMovementDao.findAllByIds(confirmation.getStorageMovementIds());
        for (StorageMovement storageMovement : storageMovements) {

            if (storageMovement.getConfirmDate() == null) {

                storageMovement.setConfirmDate(confirmDate);

                //force flush
                persistenceContext.getEntityManager().flush();

                confirmedStorageMovements.add(storageMovement);

                if (log.isDebugEnabled()) {
                    log.debug("confirmed storage movement " + storageMovement);
                }
            } else {
                throw new IllegalConfirmationException();
            }

        }

        LocationErrorsService locationErrorsService =
                serviceContext.newService(LocationErrorsService.class);

        LocationJpaDao locationDao = persistenceContext.getLocationDao();

        for (String locationInErrorId : confirmation.getLocationInErrorIds()) {

            Location locationInError = locationDao.findById(locationInErrorId);

            locationErrorsService.reportError(locationInError, article, magalieUser, confirmDate);

        }

        RequestedArticleService requestedArticleService =
                serviceContext.newService(RequestedArticleService.class);

        requestedArticleService.onStorageMovementConfirmation(magalieUser, confirmation, confirmedStorageMovements);

        persistenceContext.commit();

    }

    public void cancelStorageMovements(MagalieUser magalieUser) {

        if (log.isDebugEnabled()) {
            log.debug("will make cancellation for " + magalieUser);
        }

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        StorageMovementJpaDao storageMovementDao =
                persistenceContext.getStorageMovementDao();

        List<StorageMovement> unconfirmedStorageMovements =
                storageMovementDao.findAllUnconfirmed(magalieUser);

        for (StorageMovement unconfirmedStorageMovement : unconfirmedStorageMovements) {

            storageMovementDao.remove(unconfirmedStorageMovement);

        }

        if (log.isInfoEnabled()) {
            log.info("cancelled " + unconfirmedStorageMovements.size() +
                    " storage movements " + unconfirmedStorageMovements);
        }

        persistenceContext.commit();

    }

    public RealTimeStorageMovementTask getRealTimeStorageMovementTask(Building building, MagalieUser magalieUser, String articleId) {

        Preconditions.checkNotNull(magalieUser);

        Preconditions.checkNotNull(building);

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        ArticleJpaDao articleDao = persistenceContext.getArticleDao();

        Article article = articleDao.findById(articleId);

        Preconditions.checkNotNull(article);

        if (log.isInfoEnabled()) {
            log.info("will process real time article request in building " +
                    building + " by user " + magalieUser + " for article " + article);
        }

        // we will proceed in three main steps

        // fist, get a snapshot of the actual current state of the stock
        Iterable<StoredArticle> storedArticles = getStoredArticles(building, article);

        if (log.isTraceEnabled()) {
            log.trace("found " + Iterables.size(storedArticles) + " stored articles " + storedArticles);
        }

        // second step, remove all already used location, to find all used location, looking
        // for not already confirmed storage movements
        StorageMovementJpaDao storageMovementDao =
                persistenceContext.getStorageMovementDao();

        List<StorageMovement> storageMovements =
                storageMovementDao.findAllUnconfirmed(magalieUser);

        if (log.isDebugEnabled()) {
            log.debug("found " + storageMovements.size() + " movements already done " + storageMovements);
        }

        ImmutableSet<Location> alreadyUsedLocations =
                ImmutableSet.copyOf(
                        Iterables.transform(
                                storageMovements,
                                StorageMovements.getOriginFunction()
                        )
                );

        if (log.isTraceEnabled()) {
            log.trace("already used locations " + alreadyUsedLocations);
        }

        Predicate<StoredArticle> articleStoredInAlreadyUsedLocation =
                Predicates.compose(
                        Predicates.in(alreadyUsedLocations),
                        StoredArticles.getLocationFunction());

        storedArticles = Iterables.filter(storedArticles, Predicates.not(articleStoredInAlreadyUsedLocation));

        if (log.isTraceEnabled()) {
            log.trace("after removing already used locations " + Iterables.size(storedArticles) + " stored articles remaining " + storedArticles);
        }

        // third step, sort all those stored article by priority
        Set<StoredArticle> sortedStoredArticles = sortStoredArticlesByPriority(storedArticles);

        RealTimeStorageMovementTask realTimeStorageMovementTask =
                new RealTimeStorageMovementTask();

        if (sortedStoredArticles.isEmpty()) {

            realTimeStorageMovementTask.setArticleUnavailable(true);

        } else {

            Predicate<StoredArticle> articleStoredInAccessibleLocationPredicate =
                    StoredArticles.articleStoredInAccessibleLocationPredicate(magalieUser);

            Optional<StoredArticle> accessibleStoredArticleOptional =
                    Iterables.tryFind(sortedStoredArticles, articleStoredInAccessibleLocationPredicate);

            if (accessibleStoredArticleOptional.isPresent()) {

                StoredArticle accessibleStoredArticle = accessibleStoredArticleOptional.get();

                realTimeStorageMovementTask.setStoredArticle(accessibleStoredArticle);

            } else {

                realTimeStorageMovementTask.setDriverLicenseRequired(true);

            }

        }

        if (log.isInfoEnabled()) {
            log.info("will return real time storage movement task " + realTimeStorageMovementTask);
        }

        return realTimeStorageMovementTask;

    }

    public Article getArticle(String articleId) {

        ArticleJpaDao articleDao = serviceContext.getPersistenceContext().getArticleDao();

        Article article = articleDao.findById(articleId);

        return article;

    }

    public StorageMovement saveStorageMovement(MagalieUser magalieUser, String articleId, String originLocationId, String destinationLocationId, double quantity) {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        ArticleJpaDao articleDao = persistenceContext.getArticleDao();

        Article article = articleDao.findById(articleId);

        LocationJpaDao locationDao = persistenceContext.getLocationDao();

        Location originLocation = locationDao.findById(originLocationId);

        Location destinationLocation = locationDao.findById(destinationLocationId);

        StorageMovementJpaDao storageMovementDao =
                persistenceContext.getStorageMovementDao();

        Map<String, Object> storageMovementProperties = Maps.newHashMap();
        storageMovementProperties.put(StorageMovement.PROPERTY_MAGALIE_USER, magalieUser);
        storageMovementProperties.put(StorageMovement.PROPERTY_ARTICLE, article);
        storageMovementProperties.put(StorageMovement.PROPERTY_ORIGIN_LOCATION, originLocation);
        storageMovementProperties.put(StorageMovement.PROPERTY_DESTINATION_LOCATION, destinationLocation);
        storageMovementProperties.put(StorageMovement.PROPERTY_CONFIRM_DATE, null);

        StorageMovement storageMovement = storageMovementDao.findByProperties(storageMovementProperties);

        if (storageMovement == null) {

            storageMovement = new StorageMovement();

            storageMovement.setOriginLocation(originLocation);

            storageMovement.setDestinationLocation(destinationLocation);

            storageMovement.setMagalieUser(magalieUser);

            storageMovement.setArticle(article);

            storageMovementDao.persist(storageMovement);

        } else {

            if (log.isDebugEnabled()) {
                log.debug("storage movement already exists, user just want to update quantity " + storageMovement);
            }

        }

        storageMovement.setActualQuantity(quantity);

        Date now = serviceContext.getNow();

        storageMovement.setMovementDate(now);

        persistenceContext.commit();

        if (log.isInfoEnabled()) {
            log.info("saved real time storage movement " + storageMovement);
        }

        return storageMovement;

    }

}
