package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class FindOrderToExecuteResult {

    protected RequestedArticle requestedArticle;

    protected boolean nothingToDo;

    protected boolean driverLicenseRequired;

    protected boolean everythingUnavailable;

    protected DeliveredRequestedList oldAffectation;

    protected DeliveredRequestedList newAffectation;

    public boolean isSuccess() {
        return requestedArticle != null;
    }

    public RequestedArticle getRequestedArticle() {
        return requestedArticle;
    }

    public void setRequestedArticle(RequestedArticle requestedArticle) {
        this.requestedArticle = requestedArticle;
    }

    public void setNothingToDo(boolean nothingToDo) {
        this.nothingToDo = nothingToDo;
    }

    public boolean isNothingToDo() {
        return nothingToDo;
    }

    public void setDriverLicenseRequired(boolean driverLicenseRequired) {
        this.driverLicenseRequired = driverLicenseRequired;
    }

    public boolean isDriverLicenseRequired() {
        return driverLicenseRequired;
    }

    public void setEverythingUnavailable(boolean everythingUnavailable) {
        this.everythingUnavailable = everythingUnavailable;
    }

    public boolean isEverythingUnavailable() {
        return everythingUnavailable;
    }

    public void setOldAffectation(DeliveredRequestedList oldAffectation) {
        this.oldAffectation = oldAffectation;
    }

    public DeliveredRequestedList getOldAffectation() {
        return oldAffectation;
    }

    public void setNewAffectation(DeliveredRequestedList newAffectation) {
        this.newAffectation = newAffectation;
    }

    public DeliveredRequestedList getNewAffectation() {
        return newAffectation;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
