<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<head>
    <script>
        <s:url action="location-json" id="locationJsonUrl" />
        var locationJsonUrl = '<s:property value="%{locationJsonUrl}" />';
        var model = <s:property value="modelAsJson" escapeHtml="false" />;
    </script>
    <script src="<s:url value='/js/receive-article-input.js' />"></script>
    <title>Réception d'un article</title>
    <link rel="stylesheet" href="<s:url value='/css/magalie-ck3x-reduced.css' />"/>
</head>

<ul class="oneItemPerLine">
    <li>
        Réf. : <s:property value="receptionTask.storedArticle.article.code" />
    </li>
    <li>
        Desc. : <s:property value="receptionTask.storedArticle.article.description" />
    </li>
    <li>
        Stocké :
        <span id="stored">0</span> / <s:property value="receptionTask.quantity" />
        <s:property value="receptionTask.storedArticle.article.unit" />
    </li>
</ul>

<section id="locations">
</section>

<s:form cssClass="form-horizontal">
    <s:textfield name="locationBarcode" label="Empl." inputAppendIcon="barcode" cssClass="input-small" />
    <s:textfield name="quantity" label="Qté" inputAppend="%{receptionTask.storedArticle.article.unit}" cssClass="input-mini" />
    <s:hidden name="preparedArticleReceptionBarcode" value="%{preparedArticleReceptionBarcode}" />
    <s:hidden name="supplierId" value="%{supplierId}" />
    <s:hidden name="confirmation" />

    <div style="width: 180px">
        <s:url action="receive-article!cancel" id="cancelUrl">
            <s:param name="supplierId" value="%{supplierId}" />
        </s:url>
        <s:a href="%{cancelUrl}" cssClass="btn btn-mini" id="cancelLink">Abandon (Esc)</s:a>
        <input type="button" name="full" value="Plein (F5)" class="btn btn-danger btn-mini" id="receive-article_full" />
        <s:submit name="next" value="Suivant (⏎)" cssClass="btn btn-primary btn-mini" />
        <input type="button" id="confirm" value="Terminer (F4)" class="btn btn-success btn-mini" />
    </div>
</s:form>
<div id="locationTemplate" class="template">
    <div>
        <span data="warehouse.code"></span>
        <span data="code"></span>
        <span data="stored">0</span> / <span data="alreadyStoredQuantity"></span> <s:property value="receptionTask.storedArticle.article.unit" />
    </div>
</div>
