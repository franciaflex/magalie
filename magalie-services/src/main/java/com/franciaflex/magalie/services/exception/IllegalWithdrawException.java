package com.franciaflex.magalie.services.exception;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;

public class IllegalWithdrawException extends MagalieException {

    protected Article article;

    public IllegalWithdrawException(String message, Article article) {
        super(message);
        this.article = article;
    }

    public static void checkWithdrawIsLegal(Article article) throws IllegalWithdrawException {

        if ( ! article.isDirectWithdrawAllowed()) {

            throw new IllegalWithdrawException("article cannot be withdrawn", article);

        }

    }

}
