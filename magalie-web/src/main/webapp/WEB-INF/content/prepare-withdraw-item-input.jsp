<%--
  #%L
  MagaLiE :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2013 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s"         uri="/struts-tags" %>

<%-- XXX bleny cette page est utilisée comme vue pour deux actions ! --%>

<head>
    <title>Traitement des kanbans</title>
    <script type="text/javascript">
        $(document).ready(function () {
            bindKey('Esc', function(){ redirectTo($('#chooseActivityLink'));});
            $('input[name="articleBarcode"]').focus();
        });
    </script>
</head>

<s:url action="choose-activity" id="chooseActivityUrl"/>

<s:form>
    <s:textfield key="articleBarcode" label="Article" inputAppendIcon="barcode" cssClass="input-medium" />

    <s:select label="Magasin destination"
           key="destinationWarehouseId"
           list="destinationWarehouses"
           listKey="id"
           listValue="code"
           required="true"
    />

    <s:if test="askQuantity">
        <s:textfield name="quantity" label="Qté" inputAppend="%{article.unit}" cssClass="input-mini" />
    </s:if>

    <div class="btn-group">
        <s:a href="%{chooseActivityUrl}" cssClass="btn" id="chooseActivityLink" >Changer d'activité (Esc)</s:a>
        <s:submit name="next" value="Suivant" cssClass="btn btn-primary" />
    </div>
</s:form>