package com.franciaflex.magalie.services.service;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.DeliveredRequestedArticles;
import com.franciaflex.magalie.persistence.JpaMagaliePersistenceContext;
import com.franciaflex.magalie.persistence.RequestedArticles;
import com.franciaflex.magalie.persistence.dao.DeliveredRequestedArticleJpaDao;
import com.franciaflex.magalie.persistence.dao.DeliveredRequestedListJpaDao;
import com.franciaflex.magalie.persistence.dao.InventoryArticleJpaDao;
import com.franciaflex.magalie.persistence.dao.RequestedArticleJpaDao;
import com.franciaflex.magalie.persistence.dao.RequestedListJpaDao;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedArticle;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedList;
import com.franciaflex.magalie.persistence.entity.DeliveredRequestedListStatus;
import com.franciaflex.magalie.persistence.entity.InventoryArticle;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.persistence.entity.RequestedList;
import com.franciaflex.magalie.persistence.entity.StorageMovement;
import com.franciaflex.magalie.services.MagalieService;
import com.franciaflex.magalie.services.MagalieServiceContext;
import com.franciaflex.magalie.services.MagalieUserNotificationContext;
import com.franciaflex.magalie.services.StorageMovementConfirmation;
import com.franciaflex.magalie.services.exception.MagalieException;
import com.franciaflex.magalie.services.exception.MissingRowInViewsException;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.primitives.Booleans;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class RequestedArticleService implements MagalieService {

    private static final Log log = LogFactory.getLog(RequestedArticleService.class);

    protected MagalieServiceContext serviceContext;

    @Override
    public void setServiceContext(MagalieServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public FindOrderToExecuteResult findOrderToExecute(MagalieUser magalieUser, Building building, String listType) {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        MagalieUserNotificationContext userNotificationContext =
                serviceContext.getUserNotificationContext();

        // first, try find order in affected list
        FindOrderToExecuteResult findOrderToExecuteResult;

        DeliveredRequestedListJpaDao deliveredRequestedListDao =
                persistenceContext.getDeliveredRequestedListDao();

        DeliveredRequestedList affectedDeliveredRequestedList =
                deliveredRequestedListDao.findByAffectedTo(magalieUser);

        if (affectedDeliveredRequestedList == null) {
            //Pas de requestedList affectée
            RequestedArticleJpaDao requestedArticleDao =
                    persistenceContext.getRequestedArticleDao();

            List<RequestedArticle> allUndelivered =
                    requestedArticleDao.findAllUndelivered(building, listType);

            if (log.isInfoEnabled()) {
                log.info(allUndelivered.size() + " undelivered requested articles in building " + building + " for list type " + listType);
            }

            if (allUndelivered.isEmpty()) {

                findOrderToExecuteResult = new FindOrderToExecuteResult();

                findOrderToExecuteResult.setNothingToDo(true);

            } else {

                //Build inventoryCaches
                Set<String> articlesInInventory = new HashSet<>();
                Set<String> articlesInInventoryCache = new HashSet<>();
                InventoryArticleJpaDao dao = serviceContext.getPersistenceContext().getInventoryArticleDao();

                List<InventoryArticle> inventoryArticles = dao.findAll();
                for (InventoryArticle inventoryArticle:inventoryArticles) {
                    articlesInInventoryCache.add(inventoryArticle.getArticleId());
                }

                Set<RequestedList> listInInventory = new HashSet<>();

                Set<RequestedArticle> allUndeliveredByPriority =
                        Sets.newTreeSet(RequestedArticles.comparator());

                //caches requestedLists containing articles in inventory
                //caches article in inventory asked for (in case nothing can be done, warns the user)
                for (RequestedArticle undelivered:allUndelivered) {
                    if (articlesInInventoryCache.contains(undelivered.getArticle().getId())) {
                        listInInventory.add(undelivered.getRequestedList());
                        articlesInInventory.add(undelivered.getArticle().getCode());
                    }
                }

                //Remove articles in lists containing articles in inventory
                for (RequestedArticle undelivered:allUndelivered) {
                    if (!listInInventory.contains(undelivered.getRequestedList())) {
                        allUndeliveredByPriority.add(undelivered);
                    }
                }

                findOrderToExecuteResult = findOrderToExecute(allUndeliveredByPriority, magalieUser, building);

                if (findOrderToExecuteResult.isSuccess()) {

                    RequestedList requestedList = findOrderToExecuteResult.getRequestedArticle().getRequestedList();

                    DeliveredRequestedList newAffectation = deliveredRequestedListDao.findByRequestedList(requestedList);

                    if (newAffectation == null) {

                        newAffectation = new DeliveredRequestedList();

                        deliveredRequestedListDao.persist(newAffectation);

                    } else {

                        Preconditions.checkState(newAffectation.getStatus().isPending(), "liste déjà affectée");

                    }

                    newAffectation.setAffectedTo(magalieUser);

                    newAffectation.setRequestedList(requestedList);

                    newAffectation.setStatus(DeliveredRequestedListStatus.AFFECTED);

                    deliveredRequestedListDao.merge(newAffectation);

                    userNotificationContext.notifyAffectedRequestedList(magalieUser, newAffectation);

                    findOrderToExecuteResult.setNewAffectation(newAffectation);

                } else if (!articlesInInventory.isEmpty()){
                    serviceContext.getUserNotificationContext().notifyInventoryBlockedArticle(magalieUser, articlesInInventory);
                }

            }

        } else {

            RequestedList affectedRequestedList = affectedDeliveredRequestedList.getRequestedList();

            findOrderToExecuteResult =
                    findOrderToExecute(affectedRequestedList, magalieUser, building);

            if (findOrderToExecuteResult.isSuccess()) {

                if (log.isInfoEnabled()) {
                    log.info("order to execute " + findOrderToExecuteResult + " found in affected list " + affectedDeliveredRequestedList);
                }

            } else {

                if (log.isInfoEnabled()) {
                    log.info("request list " + affectedDeliveredRequestedList.getRequestedList()
                            + " is not complete, putting it back to pending");
                }

                affectedDeliveredRequestedList.setStatus(DeliveredRequestedListStatus.PENDING);

                affectedDeliveredRequestedList.setAffectedTo(null);

                DeliveredRequestedList oldAffectation = affectedDeliveredRequestedList;

                if (findOrderToExecuteResult.isDriverLicenseRequired()) {

                    userNotificationContext.notifyUnaffectedRequestedListDriverLicenseRequired(magalieUser, oldAffectation);

                } else if (findOrderToExecuteResult.isEverythingUnavailable()) {

                    userNotificationContext.notifyUnaffectedRequestedListForEverythingUnavailable(magalieUser, oldAffectation);

                }

                findOrderToExecuteResult.setOldAffectation(oldAffectation);

            }

        }

        persistenceContext.commit();

        if (log.isInfoEnabled()) {
            log.info("will return " + findOrderToExecuteResult);
        }

        Preconditions.checkState(findOrderToExecuteResult != null);

        Preconditions.checkState(
                BooleanUtils.xor(
                        Booleans.toArray(
                                ImmutableSet.of(
                                        findOrderToExecuteResult.isSuccess(),
                                        findOrderToExecuteResult.isEverythingUnavailable(),
                                        findOrderToExecuteResult.isNothingToDo(),
                                        findOrderToExecuteResult.isDriverLicenseRequired()
                                )
                        )
                )
        );

        return findOrderToExecuteResult;

    }

    protected FindOrderToExecuteResult findOrderToExecute(RequestedList requestedList, MagalieUser magalieUser, Building building) {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        RequestedArticleJpaDao requestedArticleDao =
                persistenceContext.getRequestedArticleDao();

        List<RequestedArticle> allUndelivered =
                requestedArticleDao.findAllUndelivered(requestedList);

        Comparator<RequestedArticle> priorityComparator =
                RequestedArticles.priorityComparator();

        Set<RequestedArticle> requestedArticlesByPriority =
                Sets.newTreeSet(priorityComparator);

        requestedArticlesByPriority.addAll(allUndelivered);

        return findOrderToExecute(requestedArticlesByPriority, magalieUser, building);

    }

    protected FindOrderToExecuteResult findOrderToExecute(Set<RequestedArticle> requestedArticlesByPriority, MagalieUser magalieUser, Building building) {

        ArticleStorageService articleStorageService =
                serviceContext.newService(ArticleStorageService.class);

        RequestedArticle requestedArticle = null;

        boolean success = false;

        Iterator<RequestedArticle> requestedArticleIterator = requestedArticlesByPriority.iterator();

        boolean driverLicenseRequired = false;

        boolean somethingIsAvailable = false;

        Set<String> articlesInInventory = new HashSet<>();

        Set<String> articlesInInventoryCache = new HashSet<>();

        InventoryArticleJpaDao dao = serviceContext.getPersistenceContext().getInventoryArticleDao();

        List<InventoryArticle> inventoryArticles = dao.findAll();

        for (InventoryArticle inventoryArticle:inventoryArticles) {
            articlesInInventoryCache.add(inventoryArticle.getArticleId());
        }

        while ( ! success && requestedArticleIterator.hasNext()) {

            requestedArticle = requestedArticleIterator.next();

            String articleId = requestedArticle.getArticle().getId();

            //On vérifie que l'article est pas en inventaire
            if (articlesInInventoryCache.contains(articleId)) {
                articlesInInventory.add(articleId);
            } else {
                RealTimeStorageMovementTask realTimeStorageMovementTask =
                        articleStorageService.getRealTimeStorageMovementTask(building, magalieUser, articleId);


                boolean articleIsAvailable = !realTimeStorageMovementTask.isArticleUnavailable();

                if (articleIsAvailable) {

                    somethingIsAvailable = true;

                    driverLicenseRequired |= realTimeStorageMovementTask.isDriverLicenseRequired();

                }

                success = articleIsAvailable;
            }

        }

        if (!articlesInInventory.isEmpty()) {
            serviceContext.getUserNotificationContext().notifyInventoryBlockedArticle(magalieUser, articlesInInventory);
        }

        FindOrderToExecuteResult findOrderToExecuteResult = new FindOrderToExecuteResult();

        if (success) {

            if (log.isInfoEnabled()) {
                log.info("order to execute found: " + requestedArticle);
            }

            Preconditions.checkState(requestedArticle != null);

            findOrderToExecuteResult.setRequestedArticle(requestedArticle);

        } else {

            if (requestedArticlesByPriority.isEmpty()) {

                if (log.isInfoEnabled()) {
                    log.info("no order to execute found: nothing to do");
                }

                findOrderToExecuteResult.setNothingToDo(true);

            } else {

                if (somethingIsAvailable) {

                    if (log.isInfoEnabled()) {
                        log.info("no order to execute found: there is something available to do but driver license is required");
                    }

                    Preconditions.checkState(driverLicenseRequired);

                    findOrderToExecuteResult.setDriverLicenseRequired(driverLicenseRequired);

                } else {

                    if (log.isInfoEnabled()) {
                        log.info("no order to execute found: everything is unavailable");
                    }

                    Preconditions.checkState(!driverLicenseRequired);

                    findOrderToExecuteResult.setEverythingUnavailable(true);

                }

            }

        }

        return findOrderToExecuteResult;

    }

    public List<DeliveredRequestedList> getAllDeliveredRequestedLists() {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        DeliveredRequestedListJpaDao dao = persistenceContext.getDeliveredRequestedListDao();

        List<DeliveredRequestedList> all = dao.findAll();

        return all;

    }

    public void onStorageMovementConfirmation(MagalieUser magalieUser, StorageMovementConfirmation confirmation, Collection<StorageMovement> storageMovements) throws MissingRowInViewsException {

        String requestedArticleId = confirmation.getRequestedArticleId();

        if (requestedArticleId == null) {

            if (log.isDebugEnabled()) {
                log.debug("confirmation " + confirmation + " doesn't concerns");
            }

        } else {

            if (log.isDebugEnabled()) {
                log.debug("will confirm article delivery with confirmation " + confirmation);
            }

            // confirmation concerns a requested article, add a delivered requested article

            JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

            RequestedArticleJpaDao requestedArticleDao =
                    persistenceContext.getRequestedArticleDao();

            RequestedArticle requestedArticle =
                    requestedArticleDao.findById(requestedArticleId);

            if (requestedArticle == null) {
                throw new MissingRowInViewsException("Il n'existe pas de ligne dans la vue 'requestedArticle' avec l'identifiant " + requestedArticleId);
            }

            DeliveredRequestedArticle newDeliveredRequestedArticle = new DeliveredRequestedArticle();

            newDeliveredRequestedArticle.setRequestedArticle(requestedArticle);

            newDeliveredRequestedArticle.addAllStorageMovements(storageMovements);

            DeliveredRequestedArticleJpaDao deliveredRequestedArticleDao =
                    persistenceContext.getDeliveredRequestedArticleDao();

            deliveredRequestedArticleDao.persist(newDeliveredRequestedArticle);

            // maybe requested list is finished, let's update the status

            RequestedList requestedList = requestedArticle.getRequestedList();

            List<RequestedArticle> allRequestedArticlesForList =
                    requestedArticleDao.findAllByRequestedList(requestedList);

            List<DeliveredRequestedArticle> deliveredRequestedArticlesForList =
                    deliveredRequestedArticleDao.findAll(requestedList);

            boolean allIsConfirmed =
                    deliveredRequestedArticlesForList.size() == allRequestedArticlesForList.size()
                            && Iterables.all(
                            deliveredRequestedArticlesForList,
                            DeliveredRequestedArticles.isConfirmedPredicate());

            if (allIsConfirmed) {

                DeliveredRequestedListJpaDao deliveredRequestedListDao =
                        persistenceContext.getDeliveredRequestedListDao();

                DeliveredRequestedList deliveredRequestedList =
                        deliveredRequestedListDao.findByRequestedList(requestedList);

                Preconditions.checkNotNull(deliveredRequestedList);

                if (log.isInfoEnabled()) {
                    log.info("all requested articles have confirmed order, marking list "
                            + requestedList.getCode() + " as complete");
                }

                MagalieUserNotificationContext userNotificationContext =
                        serviceContext.getUserNotificationContext();

                userNotificationContext.notifyUnaffectedRequestedListForCompletedRequestedList(magalieUser, deliveredRequestedList);

                deliveredRequestedList.setStatus(DeliveredRequestedListStatus.COMPLETE);

            }

            persistenceContext.commit();

        }

    }

    public void removeAffectation(MagalieUser magalieUser) {

        JpaMagaliePersistenceContext persistenceContext =
                serviceContext.getPersistenceContext();

        DeliveredRequestedListJpaDao dao =
                persistenceContext.getDeliveredRequestedListDao();

        DeliveredRequestedList deliveredRequestedList =
                dao.findByAffectedTo(magalieUser);

        if (deliveredRequestedList != null && deliveredRequestedList.getStatus().isAffected()) {

            if (log.isInfoEnabled()) {
                log.info("user " + magalieUser.getLogin() +
                         " is no longer affected to list " +
                         deliveredRequestedList.getRequestedList().getCode());
            }

            deliveredRequestedList.setAffectedTo(null);

            deliveredRequestedList.setStatus(DeliveredRequestedListStatus.PENDING);

            persistenceContext.commit();

        }

    }

    public Optional<RequestedList> getAffectationForUser(MagalieUser magalieUser) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        DeliveredRequestedListJpaDao dao = persistenceContext.getDeliveredRequestedListDao();

        DeliveredRequestedList deliveredRequestedList = dao.findByAffectedTo(magalieUser);

        Optional<RequestedList> affectationForUser;

        if (deliveredRequestedList == null) {

            affectationForUser = Optional.absent();

        } else {

            affectationForUser = Optional.of(deliveredRequestedList.getRequestedList());

        }

        return affectationForUser;

    }

    public List<String> getListTypes(Building building) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        RequestedListJpaDao dao = persistenceContext.getRequestedListDao();

        List<String> allDistinctListTypes = dao.findAllDistinctListTypes(building);

        return allDistinctListTypes;

    }

    public Optional<DeliveredRequestedList> getDeliveredRequestedList(StorageMovementConfirmation confirmation) {

        String requestedArticleId = confirmation.getRequestedArticleId();

        Optional<DeliveredRequestedList> result;

        if (requestedArticleId == null) {

            result = Optional.absent();

        } else {

            JpaMagaliePersistenceContext persistenceContext =
                    serviceContext.getPersistenceContext();

            RequestedArticleJpaDao requestedArticleDao =
                    persistenceContext.getRequestedArticleDao();

            RequestedArticle requestedArticle =
                    requestedArticleDao.findById(requestedArticleId);

            RequestedList requestedList = requestedArticle.getRequestedList();

            DeliveredRequestedListJpaDao deliveredRequestedListDao =
                    persistenceContext.getDeliveredRequestedListDao();

            DeliveredRequestedList deliveredRequestedList =
                    deliveredRequestedListDao.findByRequestedList(requestedList);

            return Optional.of(deliveredRequestedList);

        }

        return result;

    }

    public RequestedArticle getRequestedArticle(String requestedArticleId) {

        JpaMagaliePersistenceContext persistenceContext = serviceContext.getPersistenceContext();

        RequestedArticleJpaDao requestedArticleDao = persistenceContext.getRequestedArticleDao();

        RequestedArticle requestedArticle = requestedArticleDao.findById(requestedArticleId);

        Preconditions.checkNotNull(requestedArticle, "unable to find requestedArticle with id = " + requestedArticleId);

        return requestedArticle;

    }
}
