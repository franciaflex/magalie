package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.persistence.entity.RequestedList;
import com.franciaflex.magalie.services.service.FindOrderToExecuteResult;
import com.franciaflex.magalie.services.service.RequestedArticleService;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.Preparable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;

@Results({
    @Result(name="success", type="redirectAction", params = { "actionName", "withdraw-item!input", "requestedArticleId", "${requestedArticleId}" }),
    @Result(name="choose-list-type", type="redirectAction", params = { "actionName", "deliver-requested-article!input" })
})
public class DeliverRequestedArticleAction extends MagalieActionSupport implements Preparable {

    private static final Log log = LogFactory.getLog(DeliverRequestedArticleAction.class);

    protected MagalieSession session;

    protected RequestedArticleService service;

    protected RequestedList requestedList;

    protected List<String> listTypes;

    protected String listType;

    protected RequestedArticle requestedArticle;

    public void setService(RequestedArticleService service) {
        this.service = service;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    @Override
    public void prepare() {

        MagalieUser magalieUser = session.getMagalieUser();

        Optional<RequestedList> affectationForUser = service.getAffectationForUser(magalieUser);

        if (affectationForUser.isPresent()) {

            requestedList = affectationForUser.get();

        }

        Building building = session.getBuilding();

        listTypes = service.getListTypes(building);

    }

    @Override
    public String input() {

        return INPUT;

    }

    public RequestedList getRequestedList() {
        return requestedList;
    }

    public List<String> getListTypes() {
        return listTypes;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    /**
     * On arrive ici quand l'utilisateur a sélectionné un type de liste. On cherche
     * quelque-chose à faire et si on trouve ou redirige vers l'interface de prélèvement
     * sinon on reste sur la liste des types de listes en affichant un message expliquant
     * pourquoi on est pas allé plus loin
     */
    @Override
    public String execute() {

        Preconditions.checkState(listType != null);

        session.setActivity(Activity.REQUESTED_LISTS);

        MagalieUser magalieUser = session.getMagalieUser();

        Building building = session.getBuilding();

        FindOrderToExecuteResult findOrderToExecuteResult =
                service.findOrderToExecute(magalieUser, building, listType);

        String result;

        if (findOrderToExecuteResult.isSuccess()) {

            requestedArticle = findOrderToExecuteResult.getRequestedArticle();

            if (log.isInfoEnabled()) {
                log.info("va traiter la demande " + requestedArticle.getId() + " de la liste " + requestedArticle.getRequestedList().getId());
            }

            result = SUCCESS;

        } else {

            if (findOrderToExecuteResult.isNothingToDo()) {

                addMessage("Il n'y a aucune demande. Vous n'avez rien à faire");

            }

            if (findOrderToExecuteResult.isDriverLicenseRequired()) {

                addMessage("Il y a des demandes en cours, mais un permis est requis");

            }

            if (findOrderToExecuteResult.isEverythingUnavailable()) {

                addMessage("Toutes les demandes en cours concernent des articles indisponibles");

            }

            result = INPUT;

        }

        return result;

    }

    /**
     * On arrive ici après avoir terminer de prélever un article. On redirige vers l'article
     * suivant s'il y en a un sinon on propose directement de changer le type de liste.
     *
     * On affiche pas de message particulier, l'utilisateur a déjà été notifié qu'il n'est
     * plus affecté à la liste
     */
    public String next() {

        Preconditions.checkState(listType != null);

        session.setActivity(Activity.REQUESTED_LISTS);

        MagalieUser magalieUser = session.getMagalieUser();

        Building building = session.getBuilding();

        FindOrderToExecuteResult findOrderToExecuteResult =
                service.findOrderToExecute(magalieUser, building, listType);

        String result;

        if (findOrderToExecuteResult.isSuccess()) {

            requestedArticle = findOrderToExecuteResult.getRequestedArticle();

            result = SUCCESS;

        } else {

            result = "choose-list-type";

        }

        return result;

    }

    public String getRequestedArticleId() {
        return requestedArticle.getId();
    }

}
