package com.franciaflex.magalie.persistence.dao;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Kanban;
import com.franciaflex.magalie.persistence.entity.Warehouse;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class KanbanJpaDao extends AbstractKanbanJpaDao {

    public KanbanJpaDao(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Kanban find(Article article, Warehouse warehouse) {
        TypedQuery<Kanban> query = entityManager.createQuery("from Kanban k where k.article = :article and k.warehouse = :warehouse", getEntityClass());
        query.setParameter("article", article);
        query.setParameter("warehouse", warehouse);
        return findAnyOrNull(query);
    }

}
