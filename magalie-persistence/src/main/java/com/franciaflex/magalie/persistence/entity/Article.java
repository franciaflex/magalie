package com.franciaflex.magalie.persistence.entity;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Objects;
import com.google.common.collect.Sets;

import javax.persistence.Entity;
import java.util.Set;

@Entity
public class Article extends AbstractJpaArticle {

    private static final long serialVersionUID = 1L;

    public boolean isFixedLocation(Location location) {
        boolean isFixedLocation = false;
        Set<Location> fixedLocations = getFixedLocations();
        if (fixedLocations != null) {
            isFixedLocation = fixedLocations.contains(location);
        }
        return isFixedLocation;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add(PROPERTY_CODE, code)
                .add(PROPERTY_DESCRIPTION, description)
                .add(PROPERTY_ID, id)
                .toString();
    }

    public Set<Location> getFixedLocationsInBuilding(Building building) {
        Set<Location> fixedLocationsInBuilding = Sets.newHashSet();
        if (getFixedLocations() != null) {
            for (Location location : getFixedLocations()) {
                if (location.getWarehouse().getBuilding().equals(building)) {
                    fixedLocationsInBuilding.add(location);
                }
            }
        }
        return fixedLocationsInBuilding;
    }

}
