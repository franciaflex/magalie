package com.franciaflex.magalie.services;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.franciaflex.magalie.MagalieTechnicalException;
import com.franciaflex.magalie.persistence.entity.Article;
import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.InventoryArticle;
import com.franciaflex.magalie.persistence.entity.Kanban;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.PreparedArticleReception;
import com.franciaflex.magalie.persistence.entity.RequestedArticle;
import com.franciaflex.magalie.persistence.entity.RequestedList;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.persistence.entity.Supplier;
import com.franciaflex.magalie.persistence.entity.Warehouse;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class MagalieFixtures {

    protected Map<String, Object> fixtures;

    public MagalieFixtures(String fixturesName) {
        String yamlPath = "/" + fixturesName + ".yaml";
        InputStream inputStream = MagalieFixtures.class.getResourceAsStream(yamlPath);
        String yaml;
        try {
            yaml = IOUtils.toString(inputStream, Charsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalArgumentException(fixturesName + " is not a valid fixtures set name", e);
        }
        YamlReader reader = new YamlReader(yaml);
        reader.getConfig().setClassTag("company", Company.class);
        reader.getConfig().setClassTag("building", Building.class);
        reader.getConfig().setClassTag("warehouse", Warehouse.class);
        reader.getConfig().setClassTag("supplier", Supplier.class);
        reader.getConfig().setClassTag("article", Article.class);
        reader.getConfig().setClassTag("kanban", Kanban.class);
        reader.getConfig().setClassTag("stored-article", StoredArticle.class);
        reader.getConfig().setClassTag("user", MagalieUser.class);
        reader.getConfig().setClassTag("requested-article", RequestedArticle.class);
        reader.getConfig().setClassTag("requested-list", RequestedList.class);
        reader.getConfig().setClassTag("location", Location.class);
        reader.getConfig().setClassTag("prepared-article-reception", PreparedArticleReception.class);
        reader.getConfig().setClassTag("inventory-article", InventoryArticle.class);
        try {
            fixtures = (Map<String, Object>) reader.read();
        } catch (YamlException e) {
            throw new MagalieTechnicalException("unable to read yaml file", e);
        }
    }

    public <E> E fixture(String id) {
        return (E) fixtures.get(id);
    }
}
