package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.exception.PreparedArticleReceptionAlreadyStoredException;
import com.franciaflex.magalie.services.service.ReceptionService;
import com.franciaflex.magalie.web.Activity;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results({
    @Result(name="success", type="redirectAction", params = { "actionName", "receive-article!input", "preparedArticleReceptionBarcode", "${barcode}" })
})
public class PreparePreparedArticleReceptionAction extends MagalieActionSupport {

    protected ReceptionService receptionService;

    protected String barcode;

    protected MagalieSession session;

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setReceptionService(ReceptionService receptionService) {
        this.receptionService = receptionService;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    @Override
    public String execute() {

        session.setActivity(Activity.PREPARED_RECEPTIONS);

        Building building = session.getBuilding();

        try {

            receptionService.getPreparedArticleReception(building, barcode);

        } catch (PreparedArticleReceptionAlreadyStoredException e) {

            addFieldError("barcode", "Article déjà rangé");

            return INPUT;

        } catch (InvalidMagalieBarcodeException e) {

            addFieldError("barcode", "Ce n'est pas un code-barre valide");

            return INPUT;

        }

        return SUCCESS;

    }

    public String getBarcode() {
        return barcode;
    }
}
