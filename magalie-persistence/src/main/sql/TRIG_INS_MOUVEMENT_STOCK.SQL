CREATE OR REPLACE TRIGGER INS_MOUVEMENT_STOCK
			AFTER INSERT OR UPDATE ON MAGALIE.STORAGE_MOVEMENT
			FOR EACH ROW

BEGIN
IF  (:new.CONFIRM_DATE IS  NOT NULL) THEN
DBMS_OUTPUT.put_line('INSERTION MOUVEMENT STOCK');
INSERT INTO BAAN.TSPSFC550340 ("T$IDSM",
                               "T$LOCO",
                               "T$LOCD",
                               "T$ITEM",
                               "T$QTEA",
                               "T$QTEM",
                               "T$DTEO",
                               "T$DTEM",
                               "T$MAGR",
                               "T$MAJS",
                               "T$ERRM",
                               "T$REFCNTD",
                               "T$REFCNTU")
                               
    VALUES (
             :new.ID,
             :new.ORIGIN_LOCATION,
             :new.DESTINATION_LOCATION,
             :new.ARTICLE,
             CASE WHEN( :new.ACTUAL_QUANTITY IS NULL) THEN 0 ELSE :new.ACTUAL_QUANTITY END,
             CASE WHEN( :new.ACTUAL_QUANTITY IS NULL) THEN 0 ELSE :new.ACTUAL_QUANTITY END,
             CASE WHEN(:new.MOVEMENT_DATE IS NULL) THEN 'AUCUNE_DATE' ELSE TO_CHAR(:new.MOVEMENT_DATE, 'DD/MM/YY HH24:MI:SS') END,
             CASE WHEN(:new.CONFIRM_DATE IS NULL) THEN 'AUCUNE_DATE' ELSE TO_CHAR(:new.CONFIRM_DATE, 'DD/MM/YY HH24:MI:SS') END,
              :new.MAGALIE_USER,     
               '2','2',0,0);  
END IF;
END;