package com.franciaflex.magalie.web;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MagalieApplicationListener implements ServletContextListener {

    private static final Log log = LogFactory.getLog(MagalieApplicationListener.class);

    protected MagalieApplicationContext applicationContext;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        String contextPath = sce.getServletContext().getContextPath().replace("/", "");

        if (log.isInfoEnabled()) {
            log.info("init MagaLiE for context path '" + contextPath + "'");
        }

        applicationContext = new MagalieApplicationContext(contextPath);

        sce.getServletContext().setAttribute(
                MagalieApplicationContext.APPLICATION_CONTEXT_PARAMETER,
                applicationContext);

        applicationContext.init();

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

        if (log.isInfoEnabled()) {
            log.info("stopping MagaLiE");
        }

        applicationContext.close();

    }
}
