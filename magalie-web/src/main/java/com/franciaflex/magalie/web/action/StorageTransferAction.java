package com.franciaflex.magalie.web.action;

/*
 * #%L
 * MagaLiE :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Building;
import com.franciaflex.magalie.persistence.entity.Company;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.franciaflex.magalie.persistence.entity.StoredArticle;
import com.franciaflex.magalie.services.exception.InaccessibleLocationException;
import com.franciaflex.magalie.services.exception.InvalidMagalieBarcodeException;
import com.franciaflex.magalie.services.service.LocationErrorsService;
import com.franciaflex.magalie.services.service.StorageTransferService;
import com.franciaflex.magalie.web.MagalieActionSupport;
import com.franciaflex.magalie.web.MagalieSession;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.nuiton.jpa.api.JpaEntities;

import java.util.List;

/**
 * @author Bavencoff
 */

@Results({
       @Result(name="success", type="redirectAction", params = { "actionName", "storage-transfer-location!input"}),
       @Result(name="error", type="redirectAction", params = { "actionName", "storage-transfer-location!input"}),
       @Result(name="choose-stored-article", type="redirectAction", params = { "actionName", "storage-transfer!input", "originId", "%{origin.id}"}),
       @Result(name="choose-location", type="redirectAction", params = { "actionName", "storage-transfer-location!input"})
})
public class StorageTransferAction extends MagalieActionSupport {

    private static final Log log = LogFactory.getLog(StorageTransferAction.class);

    protected MagalieSession session;

    protected StorageTransferService service;

    protected LocationErrorsService locationErrorsService;

    protected String originId;

    protected Location origin;

    protected List<StoredArticle> storedArticles;

    protected String storedArticleId;

    protected StoredArticle storedArticle;

    protected String destinationBarcode;

    protected double quantity;

    protected String destinationBuildingId;

    protected List<Building> destinationBuildings;

    public void setService(StorageTransferService service) {
        this.service = service;
    }

    public void setLocationErrorsService(LocationErrorsService locationErrorsService) {
        this.locationErrorsService = locationErrorsService;
    }

    public void setSession(MagalieSession session) {
        this.session = session;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    protected void prepare() {

        if (log.isDebugEnabled()) {
            log.debug("originId = " + originId +
                      ", storedArticleId = " + storedArticleId);
        }

        // dans un cas très rare on a originId qui est null/vide
        // XXX jcouteau 08/01/2020 c'est arrivé une seule fois, à voir si on conserve ce comportement.
        if (StringUtils.isBlank(originId)) {
            if (log.isErrorEnabled()) {
                log.error("originId = " + originId + ", storedArticleId = " + storedArticleId);
            }
        } else {
            origin = service.getLocationById(originId);

            storedArticles = service.getStoredArticlesInLocation(origin);

            if (storedArticleId != null) {

                // dans un cas très rare, on risque la NPE si l'article a déjà sorti de l'emplacement par un autre utilisateur
                // dans ce cas, on retourne à la liste de choix
                // XXX brendan 02/09/13 c'est arrivé une seule fois, à voir si on conserve ce comportement qui ne devrait pratiquement jamais arriver
                Optional<StoredArticle> storedArticleOptional = Iterables.tryFind(storedArticles, JpaEntities.entityHasId(storedArticleId));

                if (storedArticleOptional.isPresent()) {

                    storedArticle = storedArticleOptional.get();

                }

            }
        }

        Company company = session.getBuilding().getCompany();

        destinationBuildings = service.getDestinationBuildings(company);

    }

    @Override
    public String input() {

        prepare();

        if (origin == null) {
            if (log.isWarnEnabled()) {
                log.warn("Le code scanné correspond a aucun emplacement");
            }
            return "choose-location";
        }

        if (storedArticleId != null && storedArticle == null) {
            if (log.isWarnEnabled()) {
                log.warn("stored article " + storedArticleId + " was not available, ask user to choose another");
            }
            return "choose-stored-article";
        }

        // default value
        if (storedArticle != null) {
            quantity = storedArticle.getQuantity();
        }

        return INPUT;

    }

    public List<Building> getDestinationBuildings() {
        return destinationBuildings;
    }

    public Location getOrigin() {
        return origin;
    }

    public List<StoredArticle> getStoredArticles() {
        return storedArticles;
    }

    public String getStoredArticleId() {
        return storedArticleId;
    }

    public StoredArticle getStoredArticle() {
        return storedArticle;
    }

    public double getQuantity() {
        return quantity;
    }

    public String getDestinationBarcode(){
        return destinationBarcode;
    }

    public void setStoredArticleId(String storedArticleId) {
        this.storedArticleId = storedArticleId;
    }

    public void setDestinationBarcode(String destinationBarcode) {
        this.destinationBarcode = destinationBarcode;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getDestinationBuildingId() {
        if (destinationBuildingId == null) {
            destinationBuildingId = session.getBuilding().getId();
        }
        return destinationBuildingId;
    }

    public void setDestinationBuildingId(String destinationBuildingId) {
        this.destinationBuildingId = destinationBuildingId;
    }

    @Override
    public String execute() {

        if (log.isDebugEnabled()) {
            log.debug("originId = " + originId +
                    ", storedArticleId = " + storedArticleId +
                    ", quantity = " + quantity +
                    ", destinationBuildingId = " + destinationBuildingId +
                    ", destinationBarcode = " + destinationBarcode);
        }

        prepare();

        if ( ! (quantity > 0.)) {
            addFieldError("quantity", "la quantité ne doit pas être nulle");
            return INPUT;
        }

        if (quantity > storedArticle.getQuantity()) {
            addFieldError("quantity", "Stock insuffisant");
            return INPUT;
        }

        if (destinationBarcode.isEmpty()) {
            addFieldError("destinationBarcode", "L'emplacement de destination doit être renseigné");
            return INPUT;
        }

        MagalieUser magalieUser = session.getMagalieUser();

        Location destination;

        try {

            destination = service.getLocation(getDestinationBuildingId(), magalieUser, destinationBarcode);

        } catch (InvalidMagalieBarcodeException e) {

            addFieldError("destinationBarcode", "Le code barre n'est pas un code valide");

            return INPUT;

        } catch (InaccessibleLocationException e) {

            addFieldError("destinationBarcode", "Vous n'êtes pas autorisé à accéder à l'emplacement");

            return INPUT;

        }

        if (destination.equals(storedArticle.getLocation())) {
            addFieldError("destinationBarcode", "la destination doit être différente de l'origine");
            return INPUT;
        }

        service.confirmStorageTransfer(magalieUser, storedArticle, quantity, destination);

        return SUCCESS;

    }

    public String reportError() {

        MagalieUser magalieUser = session.getMagalieUser();

        locationErrorsService.reportError(storedArticleId, magalieUser);

        addMessage("L'emplacement a bien été reporté en erreur");

        return ERROR;

    }

}
