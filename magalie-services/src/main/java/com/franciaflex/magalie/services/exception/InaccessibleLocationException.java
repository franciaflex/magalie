package com.franciaflex.magalie.services.exception;

/*
 * #%L
 * MagaLiE :: Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.Locations;
import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;

public class InaccessibleLocationException extends MagalieException {

    protected Location location;

    protected MagalieUser magalieUser;

    public InaccessibleLocationException(String message, Location location, MagalieUser magalieUser) {
        super(message);
        this.location = location;
        this.magalieUser = magalieUser;
    }

    public static void checkLocationIsAccessible(Location location, MagalieUser magalieUser) throws InaccessibleLocationException {

        if (Locations.inaccessibleLocationPredicate(magalieUser).apply(location)) {

            InaccessibleLocationException newException =
                    new InaccessibleLocationException(
                            "user is not allowed to access given location",
                            location, magalieUser);

            throw newException;

        }

    }

    public Location getLocation() {
        return location;
    }

    public MagalieUser getMagalieUser() {
        return magalieUser;
    }

}
