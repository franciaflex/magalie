package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Supplier;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;

import java.util.Comparator;

public class Suppliers {

    public static Function<Supplier, String> getSupplierGroup() {
        return new GetSupplierGroup();
    }

    public static Comparator<Supplier> orderByNamesComparator() {
        Comparator<Supplier> orderByNamesComparator = Ordering.natural().onResultOf(new GetName());
        return orderByNamesComparator;
    }

    protected static class GetName implements Function<Supplier, String> {

        @Override
        public String apply(Supplier supplier) {
            return supplier.getName();
        }
    }

    protected static class GetSupplierGroup implements Function<Supplier, String> {

        @Override
        public String apply(Supplier supplier) {
            return supplier.getSupplierGroup();
        }
    }

}
