package com.franciaflex.magalie.persistence;

/*
 * #%L
 * MagaLiE :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.franciaflex.magalie.persistence.entity.Location;
import com.franciaflex.magalie.persistence.entity.MagalieUser;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import java.util.Collection;
import java.util.Comparator;

public class Locations {

    protected static class IsReceptionLocation implements Predicate<Location> {

        @Override
        public boolean apply(Location location) {
            return codeForReceptionLocations().equals(location.getCode());
        }
    }

    protected static class IsFullLocation implements Predicate<Location> {

        @Override
        public boolean apply(Location location) {
            return location.isFullLocation();
        }
    }

    protected static class AccessibleLocationPredicate implements Predicate<Location> {

        protected MagalieUser magalieUser;

        public AccessibleLocationPredicate(MagalieUser magalieUser) {
            this.magalieUser = magalieUser;
        }

        @Override
        public boolean apply(Location location) {
            boolean isLocationAccessible = magalieUser.getAccreditationLevel() >= location.getRequiredAccreditationLevel()
                    && (magalieUser.isCraneMan() || !location.isRequiredCraneMan())  ;
            return isLocationAccessible;
        }

    }

    protected static class LocationRequiringDriverLicenseFirstComparator implements Comparator<Location> {

        @Override
        public int compare(Location location1, Location location2) {
            return location2.getRequiredAccreditationLevel() - location1.getRequiredAccreditationLevel();
        }
    }

    protected static class GetBarcode implements Function<Location, String> {

        @Override
        public String apply(Location location) {
            return location.getBarcode();
        }
    }

    public static Predicate<Location> accessibleLocationPredicate(MagalieUser magalieUser) {
        return new AccessibleLocationPredicate(magalieUser);
    }

    public static Predicate<Location> inaccessibleLocationPredicate(MagalieUser magalieUser) {
        return Predicates.not(accessibleLocationPredicate(magalieUser));
    }

    public static Comparator<Location> locationRequiringDriverLicenseFirstComparator() {
        return new LocationRequiringDriverLicenseFirstComparator();
    }

    public static String codeForWarehouseWithoutLocations() {
        return "    SANS";
    }

    public static String codeForReceptionLocations() {
        return "     REC";
    }

    public static Predicate<Location> isNotReceptionLocation() {
        return Predicates.not(new IsReceptionLocation());
    }

    public static Predicate<Location> isNotFullLocation() {
        return Predicates.not(new IsFullLocation());
    }

    public static Predicate<Location> barcodeEquals(String originLocationBarcode) {
        return Predicates.compose(Predicates.equalTo(originLocationBarcode), getBarcodeFunction());
    }

    public static Function<Location, String> getBarcodeFunction() {
        return new GetBarcode();
    }

}
